<?php
session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"are")
{
	header("Location: " . 'index.php');	
}

include 'php/load_territories.php';

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <?php include 'are_navbar.php'; ?>

            <div class="clearfix"></div>

 <!-- ==========menu profile quick info ===== -->
           <?php include 'profile.php'; ?>
			 <br />
 <!--========== /menu profile quick info ===-->
           

  <!--==========sidebar menu  =========-->
     <?php include 'are_menu.php'; ?>
         
 <!--==============/sidebar menu======-->
  </div>
 </div>
 <!-- ==========top navigation ======-->
        <?php include 'top_nav.php'; ?>
 <!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>Add New Circuit</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
					<div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ter_code">Territory<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left has-feedback-left" required="required" name="ter_code" id="ter_code" autofocus>
                            <option value="">Select Territory</option>
							<?php echo $territories; ?> 
                           </select>
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
					 </div>
					 <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="are_code">
                         Area <span style="color:red;">*</span>
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select   class="form-control has-feedback-left" id="are_code" required="required" name="are_code">
							 <option value="">Select Area</option>
						</select>
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					<div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="cir_code" >Circuit Code <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="cir_code" required="required" class="form-control has-feedback-left col-md-7 col-xs-12" name="cir_code"  data-inputmask="'mask' : '99'" placeholder="Enter two digit Circuit code "  type="text" >
						  <span class="fa fa-building form-control-feedback left" aria-hidden="true" ></span>
                        
                        </div>
                      </div>
					  									 
					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="cir_name" >Circuit Name <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="cir_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="cir_name" placeholder="30 letters max"  type="text"  required="required" maxlength="30"  >
						  <span class="fa fa-home form-control-feedback left" aria-hidden="true" required></span>                        
                        </div>
                      </div>  
						<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"for="chu_building">Image of Church Building</label>
                      	<div class="col-md-4 col-sm-4 col-xs-12">
                         <input type="file" class="form-control has-feedback-left" id="chu_building" accept="image/*" name="chu_building" />
						  <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
						  <span class="fa fa-camera form-control-feedback left" aria-hidden="true" required></span>
                      </div>
					</div>
					 <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"for="front_pic">Front View of Interior</label>
                      	<div class="col-md-4 col-sm-4 col-xs-12">
                         <input type="file" class="form-control has-feedback-left" id="front_pic" accept="image/*" name="front_pic" />
						  <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
						  <span class="fa fa-camera form-control-feedback left" aria-hidden="true" required></span>
                      </div>
					</div> 
					
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"for="back_pic">Back View of Interior</label>
                      	<div class="col-md-4 col-sm-4 col-xs-12">
                         <input type="file" class="form-control has-feedback-left" id="back_pic" accept="image/*" name="back_pic" />
						  <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
						  <span class="fa fa-camera form-control-feedback left" aria-hidden="true" required></span>
                      </div>
					</div>
					
					
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="chu_building_status">Status of Church Building</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="chu_building_status">e.g. completed or roofing level etc.</label>
                          <textarea  id="chu_building_status" name="chu_building_status" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="1024" style="resize:none">
						  </textarea>						  
                        </div> 
                      </div> 
					  
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="building_desc">Description of Church Building</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="building_desc"> e.g. One storey building with a sitting capacity of 1000. No parking space etc. </label>
                          <textarea  id="building_desc" name="building_desc" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="1024" style="resize:none" ></textarea>						  
                        </div> 
                      </div>
					<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"for="mission_hse">Image of Mission House</label>
                      	<div class="col-md-4 col-sm-4 col-xs-12">
                         <input type="file" class="form-control has-feedback-left" id="mission_hse" accept="image/*" name="mission_hse" />
						  <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
						  <span class="fa fa-camera form-control-feedback left" aria-hidden="true" required></span>
                      </div>
					</div>
					
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mission_hse_status">Status of Mission Hse</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="mission_hse_status"> e.g. Completed, roofing level etc.</label>
                          <textarea  id="mission_hse_status" name="mission_hse_status" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="1024" style="resize:none">
						  </textarea>						  
                        </div> 
                      </div>
					  
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mission_hse_desc">Description of Mission Hse</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="mission_hse_desc"> e.g. 4 Bedroom self contain with no garage and parking space etc.</label>
                          <textarea  id="mission_hse_desc" name="mission_hse_desc" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="1024" style="resize:none">
						  </textarea>						  
                        </div> 
                      </div>
					
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="town_name">Name of Town</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="town_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="town_name" placeholder="Town where Church is Located" type="text" maxlength="30">
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required="required"></span>
                        
                        </div>
                      </div>                    

					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="area_name">Area Where Church is Located</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="area_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="area_name" placeholder="Name of area where the churc is located" type="text"  maxlength="30">
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					   	
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street_name">Street Name</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="street_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="street_name" placeholder="30 letters max" type="text"  maxlength="30">
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hse_no">House Address</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="hse_no" class="form-control has-feedback-left col-md-7 col-xs-12" name="hse_no" placeholder="15 letters max" type="text"  maxlength="15">
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="postal_address">Postal Address</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="postal_address" class="form-control has-feedback-left col-md-7 col-xs-12" name="postal_address" placeholder="60 letters max" type="text" maxlength="60">
						  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gps_code">GPS Code
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="gps_code" class="form-control has-feedback-left col-md-7 col-xs-12" name="gps_code" placeholder="" type="text" maxlength="12" />
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>				  
					   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number One</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile_one" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile_one" placeholder="Contact Number" type="text"  >
						 
                          <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number Two</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile_two" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile_two" placeholder="Contact Number " type="text">
						 
                          <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address 
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="email" class="form-control has-feedback-left col-md-7 col-xs-12" name="email" placeholder="30 letters max" type="email"  maxlength="30">
						   <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_founded">Date Founded
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_founded" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_founded" placeholder="MM/DD/YYYY" type="date">
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					   
                    
                     
                  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Circuit Details Successfully Added.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-warning" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-warning">Add New Circuit</button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Circuit details was not saved, Please contact Help Desk</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-warning" data-dismiss="modal">Cancel!</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "duplicate" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >DUPLICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Circuit details have been captured already!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="dup_cancel" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-warning">Add New Circuit</button>
                       
                        </div>

                      </div>
                    </div>
                  </div>
				   
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
						 <input id="add_Circuit" type="submit" style="width:100%" class="btn btn-primary btn-md" value="Add"/>
						 <button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
					  <button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_are.php' "> Cancel</button>
						
						
					 
					    </div>
                      </div>
					  
					  <div id= "err" > </div>
					 
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>
    <script>
$(document).ready(function (e) {
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
  
  $.ajax({
   url: "php/save_circuit.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    $("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    
     if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
                }
				if(data.match(/duplicate/gi)){
					// alert("Member is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
				
				if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    $("#err").html(e).fadeIn();
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_are.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_are.php");	
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				$("#dup_cancel").click(function(){
					 window.location.replace("index_are.php");	
											
				});
				
	
 })
 )
});
    </script>
		 <script type="text/javascript">
 $(document).ready(function(){  
 
      $('#ter_code').change(function(){  
           var ter_code = $(this).val(); 
		   console.log(ter_code);
           $.ajax({  
                url:"php/load_areas_mem_registration.php",  
                method:"POST",  
                data:{ter_code:ter_code}, 
				 dataType:"text",
                success:function(data){  
				// $('#show_product').html("");  
				console.log(data);
                     $('#are_code').html(data);  
                }  
           });  
      });  
	
	
 });  
 </script>  
	 <?php include 'timeout.php'; ?>
	 <!-- jquery.inputmask -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
    </script>
    <!-- /jquery.inputmask -->  
    
  </body>
</html>
