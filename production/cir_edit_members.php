<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"cir")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'cir_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'cir_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>EDIT MEMBERS</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div style="overflow-x: auto;">
					
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					
                      <thead>
									<tr >
										 <th></th>
										
										<th>Title</th>
										<th>First Name</th>
										<th>Middle Names</th>
										<th>Last Name</th>
										<th>Maiden Name</th>
										<th>Dob</th>
										<th>Gender</th>
										<th>Home Region</th>
										<th>Home Town</th>
										<th>Mother Tongue</th>
										<th>Secondary Lang</th>										
										<th>Marital Status</th>
										<th>Type of Marriage</th>
										<th>Mobile</th>
										<th>Mobile2</th>
										<th>Whatsapp No</th>
										<th>Email</th>
										<th>Postal Address</th>
										<th>Digital Address</th>
										<th>Recidencial Town</th>
										<th>Area Name</th>
										<th>Street Name</th>
										<th>Hse No</th>
										<th>Qualification</th>
										<th>Profession</th>
										<th>Occupation</th>
										<th>Employer</th>
										<th>Emergency Contact Person</th>
										<th>Emergency Contact No</th>
										<th>Emergency Contact Email</th>
										<th>Date Joined</th>
										<th>Baptised?</th>
										<th>Where Baptised</th>
										<th>Date Baptised</th>
										<th>Aux_code1</th>
										<th>Date Joined Aux1</th>
										<th>Aux_code2</th>
										<th>Date Joined Aux2</th>
										<th>Aux_code3</th>
										<th>Date Joined Aux3</th>
										<th>Account Status</th>
										<th>Family Group Code</th>
									</tr>
							</thead >		
							
						<tbody >
					  </tbody> 
					  <tfoot>
									<tr >
										<th></th>
										
										<th>Title</th>
										<th>First Name</th>
										<th>Middle Names</th>
										<th>Last Name</th>
										<th>Maiden Name</th>
										<th>Dob</th>
										<th>Gender</th>
										<th>Home Region</th>
										<th>Home Town</th>
										<th>Mother Tongue</th>
										<th>Secondary Lang</th>										
										<th>Marital Status</th>
										<th>Type of Marriage</th>
										<th>Mobile</th>
										<th>Mobile2</th>
										<th>Whatsapp No</th>
										<th>Email</th>
										<th>Postal Address</th>
										<th>Digital Address</th>
										<th>Recidencial Town</th>
										<th>Area Name</th>
										<th>Street Name</th>
										<th>Hse No</th>
										<th>Qualification</th>
										<th>Profession</th>
										<th>Occupation</th>
										<th>Employer</th>
										<th>Emergency Contact Person</th>
										<th>Emergency Contact No</th>
										<th>Emergency Contact Email</th>
										<th>Date Joined</th>
										<th>Baptised?</th>
										<th>Where Baptised</th>
										<th>Date Baptised</th>
										<th>Aux_code1</th>
										<th>Date Joined Aux1</th>
										<th>Aux_code2</th>
										<th>Date Joined Aux2</th>
										<th>Aux_code3</th>
										<th>Date Joined Aux3</th>
										<th>Account Status</th>
										<th>Family Group Code</th>
									</tr> 
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
					
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
		
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript" language="javascript" class="init">
	


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "../vendors/editor/examples/php/cir_update_members.php",
		table: "#results",
		fields: [ {
				label: "Title:",
				name: "title"
			}, {
				label: "First Name:",
				name: "first_name"
			}, {
				label: "Middle Names:",
				name: "middle_names"
			},  {
				label: "Last Name:",
				name: "last_name"
			}, {
				label: "Maiden Name:",
				name: "maiden_name"
			}, {
				label: "DoB:",
				name: "dob"
			},{
				label: "Gender:",
				name: "gender"
			},{
				label: "Home Region:",
				name: "home_region"
			},{
				label: "Home Town:",
				name: "home_town"
			},{
				label: "Mother Tongue:",
				name: "mother_tongue"
			}, {
				label: "Secondary Lang:",
				name: "sec_lang"
			}, {
				label: "Marital Status:",
				name: "marital_status"
			},{
				label: "Type of Marriage:",
				name: "type_of_marriage"
			},{
				label: "Mobile:",
				name: "mobile"
			},{
				label: "Mobile2:",
				name: "mobile2"
			},{
				label: "Whatsapp No:",
				name: "whatsapp"
			},{
				label: "Email:",
				name: "email"
			},{
				label: "Postal Address:",
				name: "postal_add"
			},{
				label: "Digital Address:",
				name: "digi_add"
			},{
				label: "Recidencial Town:",
				name: "town_of_res"
			},{
				label: "Area Name:",
				name: "area_name"
			},{
				label: "Street Name:",
				name: "street_name"
			},{
				label: "Hse No:",
				name: "hse_no"
			},{
				label: "Qualification:",
				name: "qualification"
			},{
				label: "Profession:",
				name: "profession"
			},{
				label: "Occupation:",
				name: "occupation"
			},{
				label: "Employer:",
				name: "employer"
			},{
				label: "Emergency Contact Person:",
				name: "emergency_name"
			},{
				label: "Emergency Contact No:",
				name: "emergency_no"
			},{
				label: "Emergency Contact Email:",
				name: "emergency_email"
			},{
				label: "Date Joined:",
				name: "date_joined"
			},{
				label: "Baptised?:",
				name: "baptised"
			},{
				label: "Where Baptised:",
				name: "where_baptised"
			},{
				label: "Date Baptised:",
				name: "date_baptised"
			},{
				label: "Aux Code 1:",
				name: "aux_code1"
			},{
				label: "Date Joined Aux 1:",
				name: "date_joined_aux1"
			},{
				label: "Aux Code 2:",
				name: "aux_code2"
			},{
				label: "Date Joined Aux 2:",
				name: "date_joined_aux2"
			},{
				label: "Aux Code 3:",
				name: "aux_code3"
			},{
				label: "Date Joined Aux 3:",
				name: "date_joined_aux3"
			},{
				label: "Account Status:",
				name: "account_status"
			},{
				label: "Family Group Code:",
				name: "fam_code"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#results').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this );
	} );

	$('#results').DataTable( {
		dom: "Bfrtip",
		ajax: "../vendors/editor/examples/php/cir_update_members.php",
		order: [[ 1, 'asc' ]],
		columns: [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
			{ data: "title" },
			{ data: "first_name" },
			{ data: "middle_names" },
			{ data: "last_name" },
			{ data: "maiden_name" },
			{ data: "dob" },
			{ data: "gender" },
			{ data: "home_region" },
			{ data: "home_town" },
			{ data: "mother_tongue" },
			{ data: "sec_lang" },
			{ data: "marital_status" },
			{ data: "type_of_marriage" },
			{ data: "mobile" },
			{ data: "mobile2" },
			{ data: "whatsapp" },
			{ data: "email" },
			{ data: "postal_add" },
			{ data: "digi_add" },
			{ data: "town_of_res" },
			{ data: "area_name" },
			{ data: "street_name" },
			{ data: "hse_no" },
			{ data: "qualification" },
			{ data: "profession" },
			{ data: "occupation" },
			{ data: "employer" },
			{ data: "emergency_name" },
			{ data: "emergency_no" },
			{ data: "emergency_email" },
			{ data: "date_joined" },
			{ data: "baptised" },
			{ data: "where_baptised" },
			{ data: "date_baptised" },
			{ data: "aux_code1" },
			{ data: "date_joined_aux1" },
			{ data: "aux_code2" },
			{ data: "date_joined_aux2" },
			{ data: "aux_code3" },
			{ data: "date_joined_aux3" },
			{ data: "account_status" },
			{ data: "fam_code" }
			//{ data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
		],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
		buttons: [
			//{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor }
			//{ extend: "remove", editor: editor }
		]
	} );
} );

</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>

