<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"aux")
{
	header("Location: " . 'index.php');
	
}
include 'php/load_territories.php';
?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'aux_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'aux_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

       <!-- page content -->
      <div class="right_col" role="main">
        <div class="">

             <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
              <!--div class="title_left"-->
			   <center><img src="images/caci-logo.png" alt="..."  width="40x" height="40px" style =" color:red; min-width:10px; min-height=10px; padding: 0px 0px 0px 0px margin:0px 50px 0px 0px" ><font color='blue'><br/><h3>The&#8239;Christ&#8239;Apostolic&#8239;Church&#8239;Int.</h3><p><h2>&#8239;Membership&#8239;Registration&#8239;Form</h2></p></font> </center>
                 
			<!--/div-->
            </div>
			 <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
			 <!-- form-start-->
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="frm" >
					
                <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ter_code">Territory<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left has-feedback-left" required="required" name="ter_code" id="ter_code" autofocus>
                            <option value="">Select Territory/Missionary area</option>
							<?php echo $territories; ?>  
                           </select>
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
					 </div>

					 <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aux_code">
                         area <span style="color:red;">*</span>
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select   class="form-control has-feedback-left" id="aux_code" required="required" name="aux_code">
							 <option value="">Select area</option>
						</select>
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  
					 <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cir_code">
                         Circuit<span style="color:red;">*</span>
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" required="required" id="cir_code" name="cir_code">
						    <option value="">Select Circuit</option>
							
						</select>
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div> 
                     
                      <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="local" >Local Assembly<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="local" class="form-control has-feedback-left col-md-7 col-xs-12" name="local" placeholder="15 letters max"  type="text"  required maxlength="15"  />
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>

                      <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fam_code">Family Group<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left has-feedback-mleft" required="required" name="fam_code" id="fam_code" autofocus>
                            <option value="">Select Family Group</option>
							<?php echo $fam_groups; ?>  
                           </select>
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
           </div>
           
					  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"for="picture">Picture<span style="color:red;">*</span>
                        </label>
                      	<div class="col-md-4 col-sm-4 col-xs-12">
                         <input type="file" class="form-control has-feedback-left" id="picture" accept="image/*" name="picture" required="required"/>
						  <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
						  <span class="fa fa-camera form-control-feedback left" aria-hidden="true" required></span>
                      </div>
					</div>
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Title<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" required="required" name="title" id="title" >
                            <option value="">Select</option>
							<option value="Master">Master</option>
                            <option value="Miss">Miss</option>
							<option value="Mr">Mr.</option>
                            <option value="Mrs">Mrs.</option>
                            <option value="Ms">Ms</option>
							<option value="Elder">Elder</option>							
							<option value="Deacon">Deacon</option>							
							<option value="Deaconess">Deaconess</option>							
							<option value="Nana">Nana</option>							
                            <option value="Prof">Prof.</option>	
							 <option value="Dr">Dr.</option>
							 <option value="Hon">Hon.</option>
                           </select>
						   <span class="fa fa-star form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
						
                      </div>
					  
					 				  
					<div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="first_name" >First Name <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="first_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="first_name" placeholder="15 letters max"  type="text"  required maxlength="15"  />
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="middle_names">Middle Name 
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="middle_names" class="form-control has-feedback-left col-md-7 col-xs-12" name="middle_names" placeholder="30 letters max" type="text" maxlength="30"/>
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last_name">Last Name<span style="color:red;">*</span> 
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="last_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="last_name" placeholder="15 letters max" type="text" required="required" maxlength="15"/>
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="maiden_name">Maiden Name
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="maiden_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="maiden_name" placeholder="30 letters max" type="text"  maxlength="30"/>
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					 
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dob">Date of Birth<span style="color:red;">*</span>
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="dob" class="form-control has-feedback-left col-md-7 col-xs-12" name="dob" placeholder="DD/MM/YYYY" type="date" required="required" />
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" required="required" name="gender" id="gender" >
                            <option value="">Select</option>
							<option value="male">Male</option>
                            <option value="female">Female</option>
                           </select>
						   <span class="fa fa-male form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
						
                      </div>
					  
					 <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Number I<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile" 
						  placeholder="Use Format 233XXXXXXXXX" type="tel" required="required" pattern="[0-9]{3}[0-9]{9}" />						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Number II</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile2" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile2" 
						  placeholder="Use Format: 233xxxxxxxxx" type="tel" pattern="[0-9]{3}[0-9]{9}" />	
						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">WhatsApp Number</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="whatsapp" class="form-control has-feedback-left col-md-7 col-xs-12" name="whatsapp" 
						 placeholder="Use Format: 233xxxxxxxxx" type="tel" pattern="[0-9]{3}[0-9]{9}" />	
						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address 
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="email" class="form-control has-feedback-left col-md-7 col-xs-12" name="email" placeholder="30 letters max" type="email"  maxlength="30"/>
						   <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="postal_add">Postal Address</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="house_location"> 60 letters max</label>
                          <textarea  id="postal_add" name="postal_add" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="60" style="resize:none"></textarea>
                        </div> 
                      </div>
					    <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="marital_status">Marital Status<span style="color:red;">*</span>
                        </span>
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="marital_status" name="marital_status" required="required" >
						    <option value="">Select</option>
							<option value="Single">Single</option>
							<option value="Married">Married</option>
							<option value="Separated">Separated</option>
							<option value="Widowed">Widowed</option>
							<option value="Divorced">Divorced</option>
                            </select>
							<span class="fa fa-magnet form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_of_marriage">Type of Marriage                       
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="type_of_marriage" name="type_of_marriage" disabled="dissabled">
						    <option value="">Select</option>
							<option value="Ordinance">Ordinance</option>
							<option value="Customary">Customary</option>
							
                          </select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div> 
                      </div>
					  
					  <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ghanaian">Ghanaian?<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="ghanaian" name="ghanaian"  required="required">
						    <option value="">Select</option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
                          </select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nationality">Nationality<span class="required"><span style="color:red;">*</span></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="nationality" class="form-control has-feedback-left col-md-7 col-xs-12" name="nationality" placeholder="" type="text"required="required" disabled="dissabled" />
						  <span class="fa fa-flag form-control-feedback left" aria-hidden="true" required></span>                        
                        </div>
                      </div>
					  
					  <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Region<span style="color:red;">*</span></label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
              <select  class="form-control has-feedback-left" name="home_region" id="home_region" required="required" disabled="dissabled" >
						                <option value="">Select</option>
                            <option value="Ahofo">Ahafo Region</option>
                            <option value="Ashanti">Ashanti Region</option>                             
                            <option value="Bono East">Bono East Region</option>
                            <option value="Bono">Bono Region</option>
                            <option value="Central">Central Region</option>
                            <option value="Eastern">Eastern Region</option>
							              <option value="Greater Accra">Greater Accra Region</option>                            
                            <option value="Northern East">Northern East Region</option>
                            <option value="Northern">Northern Region</option>                            
                            <option value="Oti">Oti Region</option>
                            <option value="Savannah">Savannah Region</option>
							              <option value="Upper East">Upper East Region</option>
                            <option value="upper_west">Upper West Region</option>
						              	<option value="Volta">Volta Region</option>                           
                            <option value="Western North">Western North Region</option>
                            <option value="Western">Western Region</option>                             
                </select>
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="home_town">Home Town <span class="required"><span style="color:red;">*</span></span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="home_town" class="form-control has-feedback-left col-md-7 col-xs-12" name="home_town" placeholder="30 letters max" type="text" maxlength="30" required="required"/>
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mother_tongue">Mother tongue <span class="required"><span style="color:red;">*</span></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mother_tongue" class="form-control has-feedback-left col-md-7 col-xs-12" name="mother_tongue" placeholder="15 letters max" type="text" maxlength="15" required="required"/>
						  <span class="fa fa-comment form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sec_lang">Secondary Languages</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="sec_lang">90 letters max</label>
                          <textarea  id="sec_lang" name="sec_lang" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="90" style="resize:none">
						  </textarea>
                        </div> 
                      </div>
					  
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qualification">Highest Educational Level<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="qualification" name="qualification" required="required">
						    <option value="">Select</option>
							<option value="na">Not Applicable</option>
							<option value="Primary">Primary</option>
							<option value="Junior High">Junior High</option>
							<option value="Senior High">Senior High</option>
							<option value="Diploma">Diploma</option>
							<option value="HND">Higher National Diploma</option>
							<option value="Bachelors">Bachelor's Degree</option>
							<option value="Masters">Master's Degree</option>
							<option value="Doctorial">Doctorial Degree</option>
							
                          </select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div> 
                      </div>
                     
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profession">Profession</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="profession" class="form-control has-feedback-left col-md-7 col-xs-12" name="profession" placeholder="30 letters max" type="text" maxlength="30"/>
						   <span class="fa fa-suitcase form-control-feedback left" aria-hidden="true" required></span>
                          </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Occupation</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="occupation" class="form-control has-feedback-left col-md-7 col-xs-12" name="occupation" placeholder="30 letters max" type="text" maxlength="30"/>
						   <span class="fa fa-usd form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employer">Employer's Name</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="employer" class="form-control has-feedback-left col-md-7 col-xs-12" name="employer" placeholder="90 letters max" type="text" maxlength="90" />
						   <span class="fa fa-institution form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="work_location">Work Location</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="house_location"> 60 letters max</label>
                          <textarea  id="work_location" name="work_location" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="60" style="resize:none">
						  </textarea>
						  
                        </div> 
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="town_of_res">Town of Residence<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="town_of_res" class="form-control has-feedback-left col-md-7 col-xs-12" name="town_of_res" placeholder="30 letters max" type="text" maxlength="30" required="required"/>
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="area_name">Name of area<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="area_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="area_name" placeholder="30 letters max" type="text" maxlength="30" required="required"/>
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street_name">Street Name<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="street_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="street_name" placeholder="30 letters max" type="text" maxlength="30" required="required"/>
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hse_no">House Number<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="hse_no" class="form-control has-feedback-left col-md-7 col-xs-12" name="hse_no" placeholder="10 letters max" type="text" maxlength="10" required="required"/>
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="digi_add">GPS Code</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="digi_add" class="form-control has-feedback-left col-md-7 col-xs-12" name="digi_add" placeholder="" type="text" maxlength="12" />
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_joined">Date Joined CACI<span style="color:red;">*</span></label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_joined" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_joined" placeholder="MM/DD/YYYY" type="date" required="required"/>
						   <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="baptised">
                          Have you been baptised?<span style="color:red;">*</span>
                        
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" id="baptised" name="baptised" required="required">
						    <option value="">Select</option>
							<option value="no">No</option>
							<option value="yes">Yes</option>
                         </select>
						  <span class="fa fa-plus form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div> 
                      </div>  
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="where_baptised">Name of the Church
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="where_baptised" class="form-control has-feedback-left col-md-7 col-xs-12" name="where_baptised" placeholder="where you baptised?" type="text" maxlength="30" disabled="dissabled"/>
						  <span class="fa fa-home form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_baptised">Date of Baptism
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_baptised" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_baptised" placeholder="MM/DD/YYYY" type="date" disabled="dissabled"/>
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
                      		  
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aux_code1">
                         1st Auxiliary Group Name
                        
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left " id="aux_code1" name="aux_code1">
						    <option value="">Select</option>							
						    <option value="201">Children's Ministry</option>							
						    <option value="202">CASA</option>							
						    <option value="203">Men's Ministry</option>							
						    <option value="204">Music Ministry</option>							
						    <option value="205">Protocol</option>							
						    <option value="206">Women's Ministry</option>							
						    <option value="207">Youth Ministry</option>							
							
                          </select>
						  <span class="fa fa-users form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div> 
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_joined_aux1">Date Joined 1st Auxiliary Group 
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_joined_aux1" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_joined_aux1" placeholder="MM/DD/YYYY" type="date" disabled="disabled"/>
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					 
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aux_code2">2nd Auxiliary Group Name
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" id="aux_code2" name="aux_code2" disabled="disabled">
						     <option value="">Select</option>							
						    <option value="201">Children's Ministry</option>							
						    <option value="202">CASA</option>							
						    <option value="203">Men's Ministry</option>							
						    <option value="204">Music Ministry</option>							
						    <option value="205">Protocol</option>							
						    <option value="206">Women's Ministry</option>							
						    <option value="207">Youth Ministry</option>			
							
                          </select>
						  <span class="fa fa-users form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div> 
                      </div>
                     
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_joined_aux2">Date Joined 2nd Auxiliary Group 
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_joined_aux2" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_joined_aux2" placeholder="MM/DD/YYYY" type="date" disabled="disabled"/>
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                     
                      <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aux_code3">
                          3rd Auxiliary Group Name
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" id="aux_code3" name="aux_code3" disabled="disabled">
						     <option value="">Select</option>							
						    <option value="201">Children's Ministry</option>							
						    <option value="202">CASA</option>							
						    <option value="203">Men's Ministry</option>							
						    <option value="204">Music Ministry</option>							
						    <option value="205">Protocol</option>							
						    <option value="206">Women's Ministry</option>							
						    <option value="207">Youth Ministry</option>			
                          </select>
						  <span class="fa fa-users form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_joined_aux3">Date Joined 3rd Auxiliary Group 
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_joined_aux3" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_joined_aux3" placeholder="MM/DD/YYYY" type="date" disabled="disabled"/>
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                                           
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="emergency_name">Emergency Contact Person<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                         <input id="emergency_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="emergency_name" placeholder="30 Letters Max"
						 type="text" maxlength="30" required="required"/> 
						  <span class="fa fa-ambulance form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
					<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Emergency Contact Number<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="emergency_no" class="form-control has-feedback-left col-md-7 col-xs-12" name="emergency_no" placeholder=""  required="required"/>
						 
                          <span class="fa fa-ambulance form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div> 
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="emergency_email">Emergency Contact Email 
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="emergency_email" class="form-control has-feedback-left col-md-7 col-xs-12" name="emergency_email" placeholder="" type="email" maxlength="30"/>
						  <span class="fa fa-ambulance form-control-feedback left" aria-hidden="true" required></span>
                       </div>
					   </div>
					
					  <br/>
					  
					  <div class="modal fade bs-example-modal-sm" id= "captcha" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Captha Verification Failed, Please try again.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-warning" data-dismiss="modal">Ok</button>
                         
                        </div>

                      </div>
                    </div>
                  </div>
				  
				    <div class="modal fade bs-example-modal-sm" id= "invalid_file" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >IMAGE UPLOAD ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Please upload a valid picture!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="invalid_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id="invalid_resume" class="btn btn-warning" data-dismiss="modal">Continue</button>
                         </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                             <p>New Member was not saved, Please try again</p>
                            <p>If no success please contact support</p>                        
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="modal fade bs-example-modal-sm" id= "offline" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >NETWORK ERROR!</h4>
                        </div>
                        <div class="modal-body">
                           	<p>Please check your internet access and try again.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                        </div>

                      </div>
                    </div>
                  </div>
				   <div class="modal fade bs-example-modal-sm" id= "duplicate" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >DUPLICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Double registration is not permited!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="dup_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-warning">Add New Member</button>
                        </div>

                      </div>
                    </div>
                  </div> 
				  <div class="modal fade bs-example-modal-sm" id= "offline" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >NETWORK ERROR!</h4>
                        </div>
                        <div class="modal-body">
                           	<p>Please check your internet access and try again.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                        </div>

                      </div>
                    </div>
                  </div>
				   <div class="modal fade bs-example-modal-sm" id= "check" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >CAPTHA ERROR!</h4>
                        </div>
                        <div class="modal-body">
                           	<p>Please check the captha box!.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                        </div>

                      </div>
                    </div>
                  </div>
				    <div class="modal fade bs-example-modal-sm" id= "invalid_no" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >INPUT ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>You will receive your User ID via SMS so please ensure that the mobile field is correctly filled.</p>
                            <p>E.g. if your mobile number is 0244123456, enter 244123456. </p>
                            <p>NB: You should enter it without the first zero.  </p>
                            <p>NB: Do not leave any lines unfilled: _  </p>
                            <p>Please correct it now.  </p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="invalid_mobile" class="btn btn-warning" data-dismiss="modal">Ok</button>
                         </div>

                      </div>
                    </div>
                  </div>
				    <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
						  <div class="alert alert-danger" id="bot" role="alert">
						</div>
						 
						 
						 <!--div class="g-recaptcha" data-sitekey="6LdC1W0UAAAAAAd1JP8lobI_M3mXdR9WucbALUU6"></div><br/-->
							 <input id="register_mem" type="submit" class="btn btn-primary btn-md" style="width:100%" value="Register "/>
							<!--button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button-->
							<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_aux.php' "> Cancel</button>
						</div>
                   </div>
					  
        	
				</form>
			 <!-- /form-end -->
	
			</div>
			</div>
			</div>
         </div>
		
      <!-- /page content -->
			<!-- footer content -->
			<?php include "footer.php";?>
			<!-- /footer content -->
       
		 </div>
      </div>
    </div>
    </div>
  

 
  <!-- script links -->
	<?php include "javascripts.php";?>
  <!-- /script links -->


  <!-- select2 -->
  <!-- /select2 -->
  <!-- input tags -->
  <!-- /input tags -->
  <!-- form validation -->
  
  <script type="text/javascript">
    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#demo-form .btn').on('click', function() {
        $('#demo-form').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#demo-form').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });

    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#frm.btn').on('click', function() {
        $('#frm').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#frm').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });
    try {
      hljs.initHighlightingOnLoad();
    } catch (err) {}
  </script>
  
  <!-- /form validation -->
  <!-- editor -->
  <!-- /editor -->
  
  <script>
$(document).ready(function (e) {	
	$('#bot').hide();
	
 $("#frm").on('submit',(function(e) {
  e.preventDefault();
  //if ($("#g-recaptcha-response").val()) {
	$.ajax({
   url: "php/aux_save_mem_registration.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
    success: function(data)
      {
    
				if(data.match(/completed/gi)){
				// alert("success");
				//$("#success").modal({backdrop: true});
				 window.location.replace("aux_generate_ID.php");
                }
				else if(data.match(/duplicate/gi)){
					// alert("Member is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
				
				else if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}else if(data.match(/internet/gi)){
							$("#internet").modal({backdrop: true});
				} 
				else if(data.match(/invalid_file/gi)){
					// alert("PHP error");
					$("#invalid_file").modal({backdrop: true});
				}
				 else if(data.match(/check/gi)){
							$("#check").modal({backdrop: true});
				} else if(data.match(/mobile/gi)){
							$("#invalid_no").modal({backdrop: true});
				} 
				else if(data.match(/robot/gi)){
					//$('#bot').hide();
								$('#bot').html("");
								$('#bot').html("<p><sytle= 'font-family:courier;'>Malicious Activity Detected!</p><p> Quitting Registration ...</font></p>");												
								$('#bot').show(); 
								setTimeout(function(){	window.location="index_aux.php";},2000);
				}
   
      },
     
    error: function(e) 
      {
	
	$("#sys_error").modal({backdrop: true});
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					 location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_aux.php");	
				});
				
				//Invalid File
				$("#invalid_cancel").click(function(){
					 window.location.replace("index_aux.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_aux.php");	
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				
				$("#dup_cancel").click(function(){
					 window.location.replace("index_aux.php");	
											
			});
 /*}else {
			$('#bot').html("");							
			$('#bot').html("<p><sytle= 'font-family:courier; color:white;'>Please respond to the captcha below by checking the box! </font></p>");
			$('#bot').show();
		*/
  
 })
 )
 
});
 
    </script>
		
        <script type="text/javascript">
 $(document).ready(function(){  
      $('#ter_code').change(function(){  
           var ter_code = $(this).val(); 
		   console.log(ter_code);
           $.ajax({  
                url:"php/load_areas_mem_registration.php",  
                method:"POST",  
                data:{ter_code:ter_code}, 
				 dataType:"text",
                success:function(data){  
				// $('#show_product').html("");  
				console.log(data);
                     $('#aux_code').html(data);  
                }  
           });  
      });  
	  
	  $('#aux_code').change(function(){  
           var aux_code = $(this).val();  
		   console.log(aux_code);
           $.ajax({  
                url:"php/load_circuits_mem_registration.php",  
                method:"POST",  
                data:{aux_code:aux_code}, 
				 dataType:"text",
                success:function(data){  
				console.log(data);
                     $('#cir_code').html(data);  
                }  
	   });  
      });  
	   $('#cir_code').change(function(){  
           var cir_code = $(this).val();  
		   console.log(cir_code);
           $.ajax({  
                url:"php/load_fam_groups.php",  
                method:"POST",  
                data:{cir_code:cir_code}, 
				 dataType:"text",
                success:function(data){  
				console.log(data);
                     $('#fam_code').html(data);  
                }  
	   });  
      });  

	  $('#cir_code').change(function(){  
           var cir_code = $(this).val();  
		   console.log(cir_code);
           $.ajax({  
                url:"php/load_aux_groups.php",  
                method:"POST",  
                data:{cir_code:cir_code}, 
				 dataType:"text",
                success:function(data){   
				console.log(data);
                     $('#aux_code1').html(data);  
                }  
	   });  
      });  

	  $('#cir_code').change(function(){  
           var cir_code = $(this).val();  
		   console.log(cir_code);
           $.ajax({  
                url:"php/load_aux_groups.php",  
                method:"POST",  
                data:{cir_code:cir_code}, 
				 dataType:"text",
                success:function(data){  
				// $('#show_product').html("");  
				console.log(data);
                     $('#aux_code2').html(data);  
                }  
	   });  
      });  
	
	$('#cir_code').change(function(){  
           var cir_code = $(this).val();  
		   console.log(cir_code);
           $.ajax({  
                url:"php/load_aux_groups.php",  
                method:"POST",  
                data:{cir_code:cir_code}, 
				 dataType:"text",
                success:function(data){  
				// $('#show_product').html("");  
				console.log(data);
                     $('#aux_code3').html(data);  
                }  
	   });  
      }); 

	  //  $("#type_of_marriage").attr("disabled", "disabled");
	  
 });  
 </script>  
 <script>
  $('#ghanaian').change(function(){  
           var ghanaian = $(this).val();  
		  
		   
		  if (ghanaian =='no'){
			   	   
				   $("#nationality").removeAttr('disabled');
		  } else{
			$("#nationality").attr("disabled", "disabled");	
			$("#home_region").removeAttr('disabled');			 
		   }
            
      });
  $('#marital_status').change(function(){  
           var married = $(this).val();  
		  
		   
		  if (married =='Married' || married =='Divorced' || married =='Separated' || married =='Widowed'){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#type_of_marriage").removeAttr('disabled');
		  } else{
			    $("#type_of_marriage").attr("disabled", "disabled");
				
		   }
            
      });  
	  
  $('#baptised').change(function(){  
           var baptised = $(this).val();  
		  
		   
		  if (baptised =='yes' ){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#where_baptised").removeAttr('disabled'); 
				  $("#date_baptised").removeAttr('disabled');
		  } else{
			     $("#where_baptised").attr("disabled", "disabled");
			    $("#date_baptised").attr("disabled", "disabled");
				
		   }
            
      }); 

	 /*	  $('#confirmed').change(function(){  
           var confirmed = $(this).val();  
		  
		   
		  if (confirmed =='yes' ){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#where_confirmed").removeAttr('disabled'); 
				  $("#date_confirmed").removeAttr('disabled');
				$('#mem_type option').eq(2).removeAttr('disabled');
				$('#mem_type option').eq(1).removeAttr('disabled');
				$('#mem_type option').eq(3).attr("disabled", "disabled");
				$('#mem_type option').eq(4).attr("disabled", "disabled");
		  } else if(confirmed =='no'){
				$('#mem_type option').eq(2).removeAttr('disabled');
				$('#mem_type option').eq(3).removeAttr('disabled');
				$('#mem_type option').eq(4).removeAttr('disabled');
				$('#mem_type option').eq(1).attr("disabled", "disabled");
				 $("#where_confirmed").attr("disabled", "disabled");
			    $("#date_confirmed").attr("disabled", "disabled");
		  }else{
			     $("#where_confirmed").attr("disabled", "disabled");
			    $("#date_confirmed").attr("disabled", "disabled");
				$('#mem_type option').eq(1).attr("disabled", "disabled");
				$('#mem_type option').eq(2).attr("disabled", "disabled");
				$('#mem_type option').eq(3).attr("disabled", "disabled");
				$('#mem_type option').eq(4).attr("disabled", "disabled");
		   }
            
      });*/ 
	   $('#aux_code1').change(function(){  
           var aux_code1 = $(this).val();  
		  
		   
		  if (aux_code1 !=='' ){
			   console.log(aux_code1);
			 // alert ("single");
			   	  
				  $("#date_joined_aux1").removeAttr('disabled'); 
				  $("#aux_code2").removeAttr('disabled');
				
		  
		  } else if (aux_code1 =='' ){
			   console.log(aux_code1);
			     $("#date_joined_aux1").attr("disabled", "disabled");
			    $("#aux_code2").attr("disabled", "disabled");
				 $("#date_joined_aux2").attr("disabled", "disabled");
			    $("#aux_code3").attr("disabled", "disabled");
				 $("#date_joined_aux3").attr("disabled", "disabled");
			 }
            
      }); 

	  $('#aux_code2').change(function(){  
           var aux_code2 = $(this).val();  
		  
		   
		  if (aux_code2 !=='' ){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#date_joined_aux2").removeAttr('disabled'); 
				  $("#aux_code3").removeAttr('disabled');
				
		  
		  }else if (aux_code2 =='' ){
			     $("#date_joined_aux2").attr("disabled", "disabled");
			    $("#aux_code3").attr("disabled", "disabled");
				 $("#date_joined_aux3").attr("disabled", "disabled");
			 }
            
      });  
	  
	  $('#aux_code3').change(function(){  
           var aux_code3 = $(this).val();  
		  
		   
		  if (aux_code3 !=='' ){
			   
				  $("#date_joined_aux3").removeAttr('disabled'); 
		  
		  }else if (aux_code3 =='' ){
			     $("#date_joined_aux3").attr("disabled", "disabled");
			 }
            
      }); 
	  
	   $('#ter_code').change(function(){  
           var ter_code = $(this).val();  
		  
		   
		  if (ter_code!=18 ){
				 $("#mobile").attr({"pattern": "[2,3]{3}[0-9]{9}"});
				 $("#mobile2").attr({"pattern": "[2,3]{3}[0-9]{9}"});				 
				 $("#emergency_no2").attr({"pattern": "[2,3]{3}[0-9]{9}"});
				 $("#mobile").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[2,3]{3}[0-9]{9}"});
				 $("#emergency_no").attr({"pattern": "[2,3]{3}[0-9]{9}"});
		  }
      }); 
	  
	   $('#aux_code').change(function(){  
           var aux_code = $(this).val(); 
		   
		  if (aux_code==1806 || aux_code==1817 ){
				 $("#mobile").attr({"pattern": "[1]{1}[0-9]{10}"});
				 $("#mobile2").attr({"pattern": "[1]{1}[0-9]{10}"});
				 $("#whatsapp").attr({"pattern": "[1]{1}[0-9]{10}"});
				 $("#mobile").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[1]{1}[0-9]{10}"});
				 $("#emergency_no").attr({"pattern": "[1]{1}[0-9]{10}"});
		  
		  }else if(aux_code==1801 || aux_code==1802 || aux_code==1803 || aux_code==1809 || aux_code==1811 || aux_code==1813 || aux_code==1814 ){
				 $("#mobile").attr({"pattern": "[0-9]{2}[0-9]{9}"});
				 $("#mobile2").attr({"pattern": "[0-9]{2}[0-9]{9}"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{9}"});
				 $("#mobile").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{9}"});
				 $("#emergency_no").attr({"pattern": "[0-9]{2}[0-9]{9}"});
		  
		  }else if(aux_code==1812 || aux_code==1816){
				 $("#mobile").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 $("#mobile2").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 $("#mobile").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 $("#emergency_no").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 
		  }else if(aux_code==1810){
				 $("#mobile").attr({"pattern": "[0-9]{2}[0-9]{11}"});
				 $("#mobile2").attr({"pattern": "[0-9]{2}[0-9]{11}"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{11}"});
				 $("#mobile").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{11}"});
				 $("#emergency_no").attr({"pattern": "[0-9]{2}[0-9]{11}"});
            
		  }else if(aux_code==1804 || aux_code==1805 || aux_code==1807 || aux_code==1808 || aux_code==18|| aux_code==1815 ){
				 $("#mobile").attr({"pattern": "[0-9]{3}[0-9]{8}"});
				 $("#mobile2").attr({"pattern": "[0-9]{3}[0-9]{8}"});
				 $("#whatsapp").attr({"pattern": "[0-9]{3}[0-9]{8}"});
				 $("#mobile").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[0-9]{3}[0-9]{8}"});
				 $("#emergency_no").attr({"pattern": "[0-9]{3}[0-9]{8}"});
		  }
      });  
	  $('#ter_code').change(function(){  
           var ter_code = $(this).val();  
		  
		   
		  if (ter_code==18 ){
				 $("#email").attr({"required": "required"});
				 $("#email").attr({"placeholder": "for login details"});
		  }else{
			   $("#email").removeAttr('required'); 
			   $("#email").attr({"placeholder": ""});
		  }
      }); 
		

	 
	  
 </script>
	
	 <!-- jquery.inputmask -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
    </script>
    <!-- /jquery.inputmask -->  
</body>

</html>
