<?php @session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"cir")
{
	header("Location: " . 'index.php');
	
}
include 'php/fill_fam_groups.php';
include 'php/fill_mem_statuses.php';
include 'php/fill_aux_groups.php';

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'cir_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'cir_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>ADD NEW APPOINTMENT</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
					  
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cir_code">Circuit <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="cir_code" class="form-control has-feedback-left col-md-7 col-xs-12" name="cir_code" placeholder="Connexional" type="text" value='<?php echo $_SESSION["cir_name"]; ?>' maxlength="30" readonly="readonly">
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_id">Member's User ID<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="user_id" class="form-control has-feedback-left col-md-7 col-xs-12" name="user_id" placeholder="Registration number" maxlength="9" autofocus type="text" required="required">
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">
                          Appointed As<span style="color:red;">*</span>
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  required="required" class="form-control has-feedback-left" id="status" name="status">
						    <option value="">Select</option>
							<?php echo $statuses; ?>  
                          </select>
						  <span class="fa fa-briefcase form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					 
					  <!--div class="item form-group" >
                      <center>  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="at">
                         At: <strong><!?php echo $_SESSION["society_name"]; ?> Society</strong>
                        </label></center>
                      </div-->
					  
					  
					 <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fam_code">
                         Family Group Name
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" id="fam_code" name="fam_code">
						    <option value="">If Applicable</option>
						<?php echo $fam_groups; ?>	
						</select>
						  <span class="fa fa-bars form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
										  
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aux_code">
                         Auxilliary Group Name
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" id="aux_code" name="aux_code">
						    <option value="">If Applicable</option>
						<?php echo $aux_codes; ?>	
						</select>
						  <span class="fa fa-bars form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="from_date">Effective Date<span style="color:red;">*</span>
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="from_date" class="form-control has-feedback-left col-md-7 col-xs-12" name="from_date" placeholder="MM/DD/YYYY" type="date" max="2020-12-31" min="1890-12-31" required='required'>
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div> 
                    
                  <!--MODALS START-->   
                  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Appointment Details Successfully Saved.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-primary" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-primary">Add New Appointment</button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The appointment details failed to save.</p>
                            <p>Please try again or contact support</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-primary" data-dismiss="modal">Cancel!</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "position" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >CONFIRMATION REQUIRED</h4>
                        </div>
                        <div class="modal-body" id="confirm">
                           
                          
                        </div>
                        <div class="modal-footer">
						  <button type="button" id="edit" class="btn btn-primary" data-dismiss="modal">Edit</button>
                          <button type="button" id="dup_cancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-primary" data-dismiss="modal">Save New Appointment</button>
                       
                        </div>

                      </div>
                    </div>
                  </div>
				   <!--MODALS END-->
				   
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
					 <input id="save_appointment" type="submit" style="width:100%" class="btn btn-primary btn-md" value="Save Appointment"/>
					  <button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_cir.php' "> Cancel</button>
						
					    </div>
                      </div>
					  
					  <div id= "err" > </div>
					 
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
 $(document).ready(function(){  
      $('#status').change(function(){  
           var status = $(this).val();  
		   console.log(status);
       });  
	  
	  $('#cir_code').change(function(){  
           var cir_code = $(this).val();  
		   console.log(cir_code);
           $.ajax({  
                url:"php/load_societies.php",  
                method:"POST",  
                data:{cir_code:cir_code}, 
				 dataType:"text",
                success:function(data){  
				// $('#show_product').html("");  
				console.log(data);
                     $('#cir_code').html(data);  
                }  
	   });  
      });  
	  $('#fam_code').change(function(){  
           var fam_code = $(this).val();  
		   console.log(fam_code);
		    if (fam_code =='' ){
			 
				  $("#aux_code").removeAttr('disabled');
		  } else{
			   
				 $("#aux_code").attr("disabled", "disabled");
		   }
      });
	   $('#aux_code').change(function(){  
           var aux_code = $(this).val();  
		   console.log(aux_code);
		    if (aux_code =='' ){
			 
				 $("#fam_code").removeAttr('disabled');
		  } else{
			    $("#fam_code").attr("disabled", "disabled");
				 
		   }
      });
	  
 });  
 </script>     
    <script>
$(document).ready(function (e) {
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
    $("#fam_code").removeAttr('disabled');
	 $("#aux_code").removeAttr('disabled');
  $.ajax({
   url: "php/cir_confirm_min_appointments.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    $("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    
     if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
                }
				if(data.match(/Position/gi)){
					 $("#confirm").html(data);
					 $("#position").modal({backdrop: true});
				}
				
				if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    $("#err").html(e).fadeIn();
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					save_appointments_data();
				});
				$("#dup_cancel").click(function(){
					 window.location.replace("index_cir.php");	
											
				});
				function save_appointments_data(){
					 $.ajax({
								   url: "php/cir_save_appointments.php",
								   type: "POST",
								   data:  new FormData(this),
								   contentType: false,
										 cache: false,
								   processData:false,
								   
								   success: function(data)
									  {
									
									 if(data.match(/success/gi)){
												// alert("success");
												$("#success").modal({backdrop: true});
												}
												if(data.match(/error/gi)){
													// alert("PHP error");
													$("#sys_error").modal({backdrop: true});
												}

								   
									  },
									 
									error: function(e) 
									  {
										  alert ("ajax error");
									$("#err").html(e).fadeIn();
									  } 
									  
									});
													
				}
				
	
 })
 )
});
    </script>
	<?php include 'timeout.php'; ?>	
    
  </body>
</html>
