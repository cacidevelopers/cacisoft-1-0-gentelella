<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"cir")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'cir_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'cir_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

       <!-- page content -->
      <div class="right_col" role="main">
        <div class="">

             <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
              <!--div class="title_left"-->
			   <center><img src="images/caci-logo.png" alt="..."  width="40x" height="40px" style =" color:red; min-width:10px; min-height=10px; padding: 0px 0px 0px 0px margin:0px 50px 0px 0px" ><font color='blue'><br/><h3>The&#8239;Christ&#8239;Apostolic&#8239;Church&#8239;Int.</h3><p><h2>&#8239;Visitors&#8239;Registration&#8239;Form</h2></p></font> </center>
                 
			<!--/div-->
            </div>
			 <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
			 <!-- form-start-->
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="frm" >
					
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Title<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" required="required" name="title" id="title" >
                            <option value="">Select</option>
							<option value="Master">Master</option>
              <option value="Miss">Miss</option>
							<option value="Mr">Mr.</option>
              <option value="Mrs">Mrs.</option>
              <option value="Ms">Ms</option>
							<option value="Elder">Elder</option>							
							<option value="Deacon">Deacon</option>							
							<option value="Deaconess">Deaconess</option>							
							<option value="Aps">Apostle</option>							
							<option value="Rev">Reverend</option>							
							<option value="Nana">Nana</option>							
              <option value="Prof">Prof.</option>	
							 <option value="Dr">Dr.</option>
							 <option value="Hon">Hon.</option>
                           </select>
						   <span class="fa fa-star form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
						
                      </div>
					  
					 				  
					<div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="first_name" >First Name <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="first_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="first_name" placeholder="15 letters max"  type="text"  required="required" maxlength="15"  />
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                     
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last_name">Last Name<span style="color:red;">*</span> 
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="last_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="last_name" placeholder="15 letters max" type="text" required="required" maxlength="15"/>
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
				
					 <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Number I<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile" 
						  placeholder="Use Format 233XXXXXXXXX" type="tel" required="required" pattern="[0-9]{3}[0-9]{9}" />						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					 
					   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">WhatsApp Number</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="whatsapp" class="form-control has-feedback-left col-md-7 col-xs-12" name="whatsapp" 
						 placeholder="Use Format: 233xxxxxxxxx" type="tel" pattern="[0-9]{3}[0-9]{9}" />	
						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					  				  					  
					  <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ghanaian">Ghanaian?<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="ghanaian" name="ghanaian"  required="required">
						    <option value="">Select</option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
                          </select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>

                      <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Region<span style="color:red;">*</span></label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" name="home_region" id="home_region" required="required" disabled="dissabled" >
						  <option value="">Select</option>
                           	<option value="Ashanti">Ashanti Region</option>
                            <option value="Brong Ahafo">Brong Ahafo Region</option>
                             <option value="Central">Central Region</option>
                            <option value="Eastern">Eastern Region</option>
							 <option value="Greater Accra">Greater Accra Region</option>
                            <option value="Northern">Northern Region</option>
							 <option value="Upper East">Upper East Region</option>
                            <option value="upper_west">Upper West Region</option>
							<option value="Volta">Volta Region</option>
							 <option value="Western">Western Region</option>                            
                          </select>
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nationality">Nationality<span class="required"><span style="color:red;">*</span></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="nationality" class="form-control has-feedback-left col-md-7 col-xs-12" name="nationality" placeholder="" type="text"required="required" disabled="dissabled" />
						  <span class="fa fa-flag form-control-feedback left" aria-hidden="true" required></span>                        
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="purpose">Purpose of Visit</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="purpose"> 512 Characters max</label>
                          <textarea  id="purpose" name="purpose" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="512" style="resize:none"></textarea>
                        </div> 
                      </div>

            
					  
					  <br/>
            <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Visitor details Successfully Saved.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-primary" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-primary">Add New </button>
                        </div>

                      </div>
                    </div>
                  </div>
					  <div class="modal fade bs-example-modal-sm" id= "captcha" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Captha Verification Failed, Please try again.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-warning" data-dismiss="modal">Ok</button>
                         
                        </div>

                      </div>
                    </div>
                  </div>
				  
				    <div class="modal fade bs-example-modal-sm" id= "invalid_file" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >IMAGE UPLOAD ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Please upload a valid picture!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="invalid_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id="invalid_resume" class="btn btn-warning" data-dismiss="modal">Continue</button>
                         </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                             <p>New Visitor was not saved, Please try again</p>
                            <p>If no success please contact support</p>                        
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "duplicate" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >DUPLICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Double registration is not permited!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="dup_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-warning">Add New Visitor</button>
                        </div>

                      </div>
                    </div>
                  </div> 
				  <div class="modal fade bs-example-modal-sm" id= "offline" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >NETWORK ERROR!</h4>
                        </div>
                        <div class="modal-body">
                           	<p>Please check your internet access and try again.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                        </div>

                      </div>
                    </div>
                  </div>
				   <div class="modal fade bs-example-modal-sm" id= "check" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >CAPTHA ERROR!</h4>
                        </div>
                        <div class="modal-body">
                           	<p>Please check the captha box!.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                        </div>

                      </div>
                    </div>
                  </div>
				    
				    <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
						  <div class="alert alert-danger" id="bot" role="alert">
						</div>
						 
						 
						 <!--div class="g-recaptcha" data-sitekey="6LdC1W0UAAAAAAd1JP8lobI_M3mXdR9WucbALUU6"></div><br/-->
							 <input id="register_mem" type="submit" class="btn btn-primary btn-md" style="width:100%" value="Register "/>
							<button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
							<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_cir.php' "> Cancel</button>
						</div>
                   </div>
					  
        	
				</form>
			 <!-- /form-end -->
	
			</div>
			</div>
			</div>
         </div>
		
      <!-- /page content -->
			<!-- footer content -->
			<?php include "footer.php";?>
			<!-- /footer content -->
       
		 </div>
      </div>
    </div>
    </div>
  

 
  <!-- script links -->
	<?php include "javascripts.php";?>
  <!-- /script links -->


  <!-- select2 -->
  <!-- /select2 -->
  <!-- input tags -->
  <!-- /input tags -->
  <!-- form validation -->
  
  <script type="text/javascript">
    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#demo-form .btn').on('click', function() {
        $('#demo-form').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#demo-form').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });

    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#frm.btn').on('click', function() {
        $('#frm').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#frm').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });
    try {
      hljs.initHighlightingOnLoad();
    } catch (err) {}
  </script>
  
  <!-- /form validation -->
  <!-- editor -->
  <!-- /editor -->
  
  <script>
$(document).ready(function (e) {	
	$('#bot').hide();
	
 $("#frm").on('submit',(function(e) {
  e.preventDefault();
  //if ($("#g-recaptcha-response").val()) {
	$.ajax({
   url: "php/cir_save_visitors.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
    success: function(data)
      {
    
				if(data.match(/completed/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
				 //window.location.replace("cir_generate_ID.php");
                }
				else if(data.match(/duplicate/gi)){
					// alert("Visitor is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
				
				else if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}
				else if(data.match(/invalid_file/gi)){
					// alert("PHP error");
					$("#invalid_file").modal({backdrop: true});
				}
				 else if(data.match(/check/gi)){
							$("#check").modal({backdrop: true});
				} else if(data.match(/mobile/gi)){
							$("#invalid_no").modal({backdrop: true});
				} 
				else if(data.match(/robot/gi)){
					//$('#bot').hide();
								$('#bot').html("");
								$('#bot').html("<p><sytle= 'font-family:courier;'>Malicious Activity Detected!</p><p> Quitting Registration ...</font></p>");												
								$('#bot').show(); 
								setTimeout(function(){	window.location="index_cir.php";},2000);
				}
   
      },
     
    error: function(e) 
      {
	
	$("#sys_error").modal({backdrop: true});
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					 location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//Invalid File
				$("#invalid_cancel").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				
				$("#dup_cancel").click(function(){
					 window.location.replace("index_cir.php");	
											
			});
 /*}else {
			$('#bot').html("");							
			$('#bot').html("<p><sytle= 'font-family:courier; color:white;'>Please respond to the captcha below by checking the box! </font></p>");
			$('#bot').show();
		*/
  
 })
 )
 
});
 
    </script>
		
	 <script type="text/javascript">
 $(document).ready(function(){  
 
      $('#ter_code').change(function(){  
           var ter_code = $(this).val(); 
		   console.log(ter_code);
           $.ajax({  
                url:"php/load_areas_mem_registration.php",  
                method:"POST",  
                data:{ter_code:ter_code}, 
				 dataType:"text",
                success:function(data){  
				// $('#show_product').html("");  
				console.log(data);
                     $('#are_code').html(data);  
                }  
           });  
      });  
	  
	  $('#are_code').change(function(){  
           var are_code = $(this).val();  
		   console.log(are_code);
           $.ajax({  
                url:"php/load_circuits_mem_registration.php",  
                method:"POST",  
                data:{are_code:are_code}, 
				 dataType:"text",
                success:function(data){  
				console.log(data);
                     $('#cir_code').html(data);  
                }  
	   });  
      });  
	 
	  //  $("#type_of_marriage").attr("disabled", "disabled");
	  
 });  
 </script>  
 <script>
  $('#ghanaian').change(function(){  
           var ghanaian = $(this).val();  
		  
		   
		  if (ghanaian =='no'){
			   	   
				   $("#nationality").removeAttr('disabled');
		  } else{
			$("#nationality").attr("disabled", "disabled");	
			$("#home_region").removeAttr('disabled');			 
		   }
            
      });
  $('#marital_status').change(function(){  
           var married = $(this).val();  
		  
		   
		  if (married =='Married' || married =='Divorced' || married =='Separated' || married =='Widowed'){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#type_of_marriage").removeAttr('disabled');
		  } else{
			    $("#type_of_marriage").attr("disabled", "disabled");
				
		   }
            
      });  
	  
  $('#baptised').change(function(){  
           var baptised = $(this).val();  
		  
		   
		  if (baptised =='yes' ){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#where_baptised").removeAttr('disabled'); 
				  $("#date_baptised").removeAttr('disabled');
		  } else{
			     $("#where_baptised").attr("disabled", "disabled");
			    $("#date_baptised").attr("disabled", "disabled");
				
		   }
            
      }); 

	 /*	  $('#confirmed').change(function(){  
           var confirmed = $(this).val();  
		  
		   
		  if (confirmed =='yes' ){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#where_confirmed").removeAttr('disabled'); 
				  $("#date_confirmed").removeAttr('disabled');
				$('#mem_type option').eq(2).removeAttr('disabled');
				$('#mem_type option').eq(1).removeAttr('disabled');
				$('#mem_type option').eq(3).attr("disabled", "disabled");
				$('#mem_type option').eq(4).attr("disabled", "disabled");
		  } else if(confirmed =='no'){
				$('#mem_type option').eq(2).removeAttr('disabled');
				$('#mem_type option').eq(3).removeAttr('disabled');
				$('#mem_type option').eq(4).removeAttr('disabled');
				$('#mem_type option').eq(1).attr("disabled", "disabled");
				 $("#where_confirmed").attr("disabled", "disabled");
			    $("#date_confirmed").attr("disabled", "disabled");
		  }else{
			     $("#where_confirmed").attr("disabled", "disabled");
			    $("#date_confirmed").attr("disabled", "disabled");
				$('#mem_type option').eq(1).attr("disabled", "disabled");
				$('#mem_type option').eq(2).attr("disabled", "disabled");
				$('#mem_type option').eq(3).attr("disabled", "disabled");
				$('#mem_type option').eq(4).attr("disabled", "disabled");
		   }
            
      });*/ 
	   $('#aux_code1').change(function(){  
           var aux_code1 = $(this).val();  
		  
		   
		  if (aux_code1 !=='' ){
			   console.log(aux_code1);
			 // alert ("single");
			   	  
				  $("#date_joined_aux1").removeAttr('disabled'); 
				  $("#aux_code2").removeAttr('disabled');
				
		  
		  } else if (aux_code1 =='' ){
			   console.log(aux_code1);
			     $("#date_joined_aux1").attr("disabled", "disabled");
			    $("#aux_code2").attr("disabled", "disabled");
				 $("#date_joined_aux2").attr("disabled", "disabled");
			    $("#aux_code3").attr("disabled", "disabled");
				 $("#date_joined_aux3").attr("disabled", "disabled");
			 }
            
      }); 

	  $('#aux_code2').change(function(){  
           var aux_code2 = $(this).val();  
		  
		   
		  if (aux_code2 !=='' ){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#date_joined_aux2").removeAttr('disabled'); 
				  $("#aux_code3").removeAttr('disabled');
				
		  
		  }else if (aux_code2 =='' ){
			     $("#date_joined_aux2").attr("disabled", "disabled");
			    $("#aux_code3").attr("disabled", "disabled");
				 $("#date_joined_aux3").attr("disabled", "disabled");
			 }
            
      });  
	  
	  $('#aux_code3').change(function(){  
           var aux_code3 = $(this).val();  
		  
		   
		  if (aux_code3 !=='' ){
			   
				  $("#date_joined_aux3").removeAttr('disabled'); 
		  
		  }else if (aux_code3 =='' ){
			     $("#date_joined_aux3").attr("disabled", "disabled");
			 }
            
      }); 
	  
	   $('#ter_code').change(function(){  
           var ter_code = $(this).val();  
		  
		   
		  if (ter_code!=18 ){
				 $("#mobile").attr({"pattern": "[2,3]{3}[0-9]{9}"});
				 $("#mobile2").attr({"pattern": "[2,3]{3}[0-9]{9}"});				 
				 $("#emergency_no2").attr({"pattern": "[2,3]{3}[0-9]{9}"});
				 $("#mobile").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: 233XXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[2,3]{3}[0-9]{9}"});
				 $("#emergency_no").attr({"pattern": "[2,3]{3}[0-9]{9}"});
		  }
      }); 
	  
	   $('#are_code').change(function(){  
           var are_code = $(this).val(); 
		   
		  if (are_code==1806 || are_code==1817 ){
				 $("#mobile").attr({"pattern": "[1]{1}[0-9]{10}"});
				 $("#mobile2").attr({"pattern": "[1]{1}[0-9]{10}"});
				 $("#whatsapp").attr({"pattern": "[1]{1}[0-9]{10}"});
				 $("#mobile").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: 1XXXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[1]{1}[0-9]{10}"});
				 $("#emergency_no").attr({"pattern": "[1]{1}[0-9]{10}"});
		  
		  }else if(are_code==1801 || are_code==1802 || are_code==1803 || are_code==1809 || are_code==1811 || are_code==1813 || are_code==1814 ){
				 $("#mobile").attr({"pattern": "[0-9]{2}[0-9]{9}"});
				 $("#mobile2").attr({"pattern": "[0-9]{2}[0-9]{9}"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{9}"});
				 $("#mobile").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: CCXXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{9}"});
				 $("#emergency_no").attr({"pattern": "[0-9]{2}[0-9]{9}"});
		  
		  }else if(are_code==1812 || are_code==1816){
				 $("#mobile").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 $("#mobile2").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 $("#mobile").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: CCXXXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 $("#emergency_no").attr({"pattern": "[0-9]{2}[0-9]{10}"});
				 
		  }else if(are_code==1810){
				 $("#mobile").attr({"pattern": "[0-9]{2}[0-9]{11}"});
				 $("#mobile2").attr({"pattern": "[0-9]{2}[0-9]{11}"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{11}"});
				 $("#mobile").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: CCXXXXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[0-9]{2}[0-9]{11}"});
				 $("#emergency_no").attr({"pattern": "[0-9]{2}[0-9]{11}"});
            
		  }else if(are_code==1804 || are_code==1805 || are_code==1807 || are_code==1808 || are_code==18|| are_code==1815 ){
				 $("#mobile").attr({"pattern": "[0-9]{3}[0-9]{8}"});
				 $("#mobile2").attr({"pattern": "[0-9]{3}[0-9]{8}"});
				 $("#whatsapp").attr({"pattern": "[0-9]{3}[0-9]{8}"});
				 $("#mobile").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#mobile2").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#whatsapp").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#emergency_no").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#emergency_no2").attr({"placeholder": "Use Format: CCCXXXXXXXX"});
				 $("#whatsapp").attr({"pattern": "[0-9]{3}[0-9]{8}"});
				 $("#emergency_no").attr({"pattern": "[0-9]{3}[0-9]{8}"});
		  }
      });  
	  $('#ter_code').change(function(){  
           var ter_code = $(this).val();  
		  
		   
		  if (ter_code==18 ){
				 $("#email").attr({"required": "required"});
				 $("#email").attr({"placeholder": "for login details"});
		  }else{
			   $("#email").removeAttr('required'); 
			   $("#email").attr({"placeholder": ""});
		  }
      }); 
		

	 
	  
 </script>
	
	 <!-- jquery.inputmask -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
    </script>
    <!-- /jquery.inputmask -->  
</body>

</html>
