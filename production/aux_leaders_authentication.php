<?php @session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"aux")
{
	header("Location: " . 'index.php');
}
include 'php/fill_aux_groups.php';
?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'aux_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'aux_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->
 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
             <div class="page-title">
              <div class="title_right">
               <div style="color:blue;"><h4>ORGANIZATION LEADERS' AUTHENTICATION 
               
             </h4> </div></div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
					  
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aux_code">
                         Auxiliary Group<span style="color:red;">*</span>
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  required class="form-control has-feedback-left" id="aux_code" name="aux_code">
						    <option value="">Please Select</option>
							<?php echo   $aux_codes; ?>  
                          </select>
						  <span class="fa fa-lock form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					 
					 
                  <!--MODALS START-->   
                 				  
				   <div class="modal fade bs-example-modal-sm" id= "auth_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >AUTHENTICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Sorry, You were DENIED ACCESS to the selected organization.</p>
                          
                        </div>
                        <div class="modal-footer">
						 <button type="button" class="btn btn-primary" data-dismiss="modal">Select Organization</button>
                          <button type="button" id="auth_cancel" class="btn btn-primary" data-dismiss="modal">Exit </button>                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				  
				   <!--MODALS END-->
				   
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
					 <input id="save_appointment" type="submit" style="width:100%" class="btn btn-primary btn-md" value="Login"/>
					  <button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index.php' "> Cancel</button>
						
					    </div>
                      </div>
					  
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
$(document).ready(function (e) {
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
  
  $.ajax({
   url: "php/aux_authenticate_leaders.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   success: function(data)
      {
    
     if(data.match(/success/gi)){
				//alert("success");
			window.location.replace("index_aux.php");	
                }
								
				if(data.match(/denied/gi)){
					// alert("PHP error");
					$("#auth_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    $("#err").html(e).fadeIn();
      } 
      
    });
	
				
								
				//select 
				$("#select").click(function(){
					location.reload();
				});
				
				//Authentication Error
				$("#auth_cancel").click(function(){
					 window.location.replace("index.php");	
				});
				
				
 })
 )
});
    </script>
<?Php include "timeout.php"; ?>	
    
  </body>
</html>
