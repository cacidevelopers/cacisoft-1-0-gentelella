<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"ict")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'ict_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'ict_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->
		
 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>TERRITORIAL SUMMARIES</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
									<tr >
										<th >Action</th>
										<th >Territory</th>
										<th >Apostle</th>
										<th >Secretary</th>
										<th >Fin Secretary</th>
										<th >Elder</th>
										<th >Office Manager</th>
										<th >Areas</th>
										<th >Circuits</th>
										<th >Aged:60+</th>
										<th >Adults:45-59</th>
										<th >Young Adults:35-44</th>
										<th >Youth: 18-34</th>
										<th >Children:0-17</th>
										<th >Total Membership</th> 
										<th >Territorial Code</th>
									</tr>
							</thead >
							
						<tbody >
					  </tbody> 
					  <tfoot>
									<tr >
										<th >Action</th>
										<th >Territory</th>
										<th >Apostle</th>
										<th >Secretary</th>
										<th >Fin Secretary</th>
										<th >Elder</th>
										<th >Office Manager</th>
										<th >Areas</th>
										<th >Circuits</th>
										<th >Aged:60+</th>
										<th >Adults:45-59</th>
										<th >Young Adults:35-44</th>
										<th >Youth: 18-34</th>
										<th >Children:0-17</th>
										<th >Total Membership</th> 
										<th >Territorial Code</th>
									</tr>  
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
 $('#results').DataTable( {
    ajax: {
        url: 'php/fetch_ter_summaries.php',
        dataSrc: '',
		 "deferRender": true
    },
    columns: [
           
      { data: "Action" },
      { data: "Territory" },
      { data: "Apostle" },
      { data: "Secretary" },
			{ data: "Fin Secretary" },
			{ data: "Elder" },
			{ data: "Office Manager" },
			{ data: "Areas" },
			{ data: "Circuits" },
			{ data: "Aged:60+" },
			{ data: "Adults:45-59" },
			{ data: "Young Adults:35-44" },
      { data: "Youth: 18-34" },
			{ data: "Children:0-17" },
			{ data: "Total Membership" },
			{ data: "Territorial Code" }
        ],	
		order: [[ 15, "asc" ]]		
											
} );
</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>
