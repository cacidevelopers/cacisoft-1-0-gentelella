<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"cir")
{
	header("Location: " . 'index.php');
	
}
include 'php/cir_fetch_top_tiles_summaries.php';
?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
  

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'cir_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'cir_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_left">
                <h2>EDIT PHOTO</h2><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				    <form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="am">

					 			  
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="picture">Picture<span style="color:red;">*</span>
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="picture" class="form-control col-md-7 col-xs-12 has-feedback-left" name="picture" accept="image/*"type="file" required="required" >
						  <span class="fa fa-camera form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  
					  					  
					
                     
                 <!--Modals-->
				 <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Photo Successfully saved!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-warning" data-dismiss="modal">Finish</button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				    <div class="modal fade bs-example-modal-sm" id= "invalid_file" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >IMAGE UPLOAD ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Please upload a valid picture!</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="invalid_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id="invalid_resume" class="btn btn-warning" data-dismiss="modal">Continue</button>
                         </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The photo was not updated!, Please try again or contact Help Desk</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>
				  
				 				 
				  <br><br><br>
				   
				   <div class="form-group">
                          <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
						 <input id="register_cir" type="submit" class="btn btn-primary btn-md" style="width:100%" value="Update Photo"/>
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_cir.php' "> Cancel</button>
					 </div>
                      </div>
					  
					  <div id= "err" > </div>
					 
                    </form>
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>
    <script>
$(document).ready(function (e) {
 $("#am").on('submit',(function(e) {
  e.preventDefault();
  
  $.ajax({
   url: "php/cir_update_profile_photo.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   /*beforeSend : function()
   {
    $("#preview").fadeOut();
    $("#err").fadeOut();
   },*/
   success: function(data)
      {
    
				
				if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
				 
				}
				
				else if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}
				else if(data.match(/invalid_file/gi)){
					// alert("PHP error");
					$("#invalid_file").modal({backdrop: true});
				}
   
      },
     
    error: function(e) 
      {
		//  alert ("ajax error");
    //$("#err").html(e).fadeIn();
	$("#sys_error").modal({backdrop: true});
      } 
      
    });
	
								
				$("#success_fin").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//Invalid File
				$("#invalid_cancel").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				
 })
 )
});
    </script>
	
	<?Php include "timeout.php"; ?>
	 <!-- jquery.inputmask -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
    </script>
    <!-- /jquery.inputmask -->  
    
  </body>
</html>
