<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"cir")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'cir_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'cir_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>ADD NEW FAMILY</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
					   
					  
					   <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="fam_code" >Family Code <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="fam_code" required="required" class="form-control has-feedback-left col-md-7 col-xs-12" name="fam_code"  placeholder="Two digit Family code: e.g. 01 or 10"  type="text" required="required"  data-inputmask="'mask' : '99'" autofocus>
						  <span class="fa fa-tags form-control-feedback left" aria-hidden="true" ></span>
                        
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="fam_name" >Family Name<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="fam_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="fam_name" placeholder="30 letters max"  type="text"  required="required" maxlength="30"  >
						  <span class="fa fa-tags form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div> 
				  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meeting_days">Meeting Days</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="meeting_days"> 60 letters max</label>
                          <textarea id="meeting_days" name="meeting_days" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="60" style="resize:none"></textarea>
                        </div> 
                      </div>  
										 
					 <!--div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="vice_id" >First Vice's ID <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="vice_id" class="form-control has-feedback-left col-md-7 col-xs-12" name="vice_id" placeholder="registration number of leader "  min="1"  type="number" required="required" >
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>

					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="sec_id" >Secretary's ID <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="sec_id" class="form-control has-feedback-left col-md-7 col-xs-12" name="sec_id" placeholder="registration number of leader "  min="1"  type="number" required="required" >
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="fin_sec_id" >Finanicial Secretary's ID <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="fin_sec_id" class="form-control has-feedback-left col-md-7 col-xs-12" name="fin_sec_id" placeholder="registration number of leader "  min="1"  type="number" required="required" >
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="org_sec_id" >Organizing Secretary's ID <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="org_sec_id" class="form-control has-feedback-left col-md-7 col-xs-12" name="org_sec_id" placeholder="registration number of leader "  min="1"  type="number" required="required" >
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div-->
						

                  <!--MODALS START-->   
                  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Family Successfully Saved.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-primary" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-primary">Add New </button>
                        </div>

                      </div>
                    </div>
                  </div>
				   <div class="modal fade bs-example-modal-sm" id= "duplicate" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >DUPLICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Sorry, the Family code has already been assigned!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="dup_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-warning">Add New</button>
                       
                        </div>

                      </div>
                    </div>
                  </div>
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Family code was not saved!, Please contact Help Desk</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   
				   <!--MODALS END-->
				   
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
					 <input id="save_appointment" type="submit" style="width:100%" class="btn btn-primary btn-md" value="Save Family"/>
					  <button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_cir.php' "> Cancel</button>
						
					    </div>
                      </div>
					  
					  <div id= "err" > </div>
					 
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
  
$(document).ready(function (e) {
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
  
  $.ajax({
   url: "php/cir_save_fam_group.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
     success: function(data)
      {
    
			if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
                }
				if(data.match(/duplicate/gi)){
					// alert("Member is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
							
				if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    $("#err").html(e).fadeIn();
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_cir.php");	
				});
		//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				$("#dup_cancel").click(function(){
					 window.location.replace("index_cir.php");	
											
			});
 })
 )
});
    </script>
	<script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
 </script>	
<?php include 'timeout.php'; ?>
    
  </body>
</html>
