<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"adm")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'adm_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'adm_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>MEMBER'S PROFILE</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
				<div class='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 col-lg-offset-3 col-xl-offset-3'> 
				 
				 <div class="form-group">
                         
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='adm_view_members.php' "> Back to members</button>
					 
                      </div>
					   <div class="form-group">
                         
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="photo" onclick="window.location.href='adm_edit_mem_photo.php' "> Edit Photo</button>
					
                      </div>
                     <br /><br />
					 
					 <style>
						tag { font-weight: bold; color:blue;}
					</style>
					
					<style>
						errortag { font-weight: bold; color:red;}
					</style>
				<?php 

						include 'php/db_con.php';
						include 'php/validate.php';
						 $mem_id=$mobile=$whatsapp=''; 
							if(isset($_GET['id'])) 
						{
						  $mem_id = empty($_GET['id']) ? '' :  validate($_GET['id']);
						  						  
						//echo $transfer_no;
						}	
						 $_SESSION["mem_id"]=$mem_id; 
					  //minister details
						
						$data = $pdo->query("SELECT * FROM members WHERE user_id='$mem_id' ")->fetchAll();
						foreach ($data as $row) 
						list($Year, $Month, $Day) = explode("-", $row["dob"]); 
						$YearDiff = date("Y") - $Year; 
						if(date("m") < $Month || (date("m") == $Month && date("d") < $DayDiff)) 
						{ 
						$YearDiff--; 
						} 
						$age= $YearDiff;
						
						$Year=$Month=$Day='';
						$YearDiff=$DayDiff=0;
						
						$name=$row["title"].' '.$row["first_name"].' '.$row["middle_names"].' '.$row["last_name"];
						
						if($row["mobile2"]==''){
							$mobile=$row["mobile"];
						}else{
							$mobile=$row["mobile"].', '.$row["mobile2"];
						}
						
				?>
													 <center><img src="pics/<?php echo $row["picture_id"]; ?>" alt="..."  width="30%" height="2%" ></center>
													 <center style='color:red;'><?php echo $name;  ?></center>
					  
					  
					 
                      <br/><br/>
						 			
							
							 <table  class='table table-striped table-bordered dt-responsive nowrap' cellspacing='0' width='100%' style='color:blue;'>
					  
						
						   <tr><td><tag>Member's ID: </td><td><?php echo $mem_id; ?></tag></td></tr>
						   <tr><td><tag>Age: </td><td><?php echo $age;?></tag></td></tr>
						   <tr><td><tag>Qualification: </td><td><?php echo $row["qualification"];?></tag></td></tr>
						   <tr><td><tag>Contact No(s): </td><td><?php echo $mobile;?></tag></td></tr>
						   <tr><td><tag>WhatsApp: </td><td><?php echo $row["whatsapp"];?></tag></td></tr>
						   <tr><td><tag>Email: </td><td><?php echo $row["email"];?></tag></td></tr>
						   <tr><td><tag>Nationality: </td><td><?php echo $row["nationality"];?></tag></td></tr>
						   <tr><td><tag>Home Region: </td><td><?php echo $row["home_region"];?></tag></td></tr>
						   <tr><td><tag>Home Town: </td><td><?php echo $row["home_town"];?></tag></td></tr>
						   <tr><td><tag>Mother Tongue: </td><td><?php echo $row["mother_tongue"];?></tag></td></tr>
						   <tr><td><tag>Secondary Languages: </td><td><?php echo $row["sec_lang"];?></tag></td></tr>
						   <tr><td><tag>Marital Status: </td><td><?php echo $row["marital_status"];?></tag></td></tr>
						   <tr><td><tag>Emergency Contact: </td><td><?php echo $row["emergency_name"];?></tag></td></tr>
						   <tr><td><tag>Emergency Number: </td><td><?php echo $row["emergency_no"];?></tag></td></tr>
						
					  </table>
					  
				   
					  <div id= "err" > </div>
					</div> 
                    </form>											  
				  <!----FORM END--->
				  		  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

  </body>
</html>
