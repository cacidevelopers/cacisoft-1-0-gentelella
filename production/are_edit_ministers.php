<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"are")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'are_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'are_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>EDIT MINISTERS</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div style="overflow-x: auto;">
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					
                      <thead>
									<tr >
										 <th></th>
										<th>Title</th>
										<th>First Name</th>
										<th>Middle Names</th>
										<th>Last Name</th>
										<th>Qualification</th>
										<th>Other Qualifications</th>
										<th>Grade</th>
										<th>Notch</th>
										<th>Current Station</th>
										<th>Previous Stations</th>
										<th>Marital Status</th>
										<th>Spouse</th>
										<th>Spouse No</th>
										<th>Children</th>
										<th>Mobile</th>
										<th>Mobile2</th>
										<th>Whatsapp</th>
										<th>Email</th>
										<th>Secondary Lang</th>
										<th>Postal Address</th>
										<th>Emergency Contact</th>
										<th>Emergency Number</th>
										<th>Relationship</th>
									</tr>
							</thead >		
							
						<tbody >
					  </tbody> 
					  <tfoot>
									<tr >
										 <th></th>
										<th>Title</th>
										<th>First Name</th>
										<th>Middle Names</th>
										<th>Last Name</th>
										<th>Qualification</th>
										<th>Other Qualifications</th>
										<th>Grade</th>
										<th>Notch</th>
										<th>Current Station</th>
										<th>Previous Stations</th>
										<th>Marital Status</th>
										<th>Spouse</th>
										<th>Spouse No</th>
										<th>Children</th>
										<th>Mobile</th>
										<th>Mobile2</th>
										<th>Whatsapp</th>
										<th>Email</th>
										<th>Secondary Lang</th>
										<th>Postal Address</th>
										<th>Emergency Contact</th>
										<th>Emergency Number</th>
										<th>Relationship</th>
									</tr> 
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
					
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
		
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript" language="javascript" class="init">
	


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "../vendors/editor/examples/php/update_ministers.php",
		table: "#results",
		fields: [  {
				label: "Title:",
				name: "title"
			}, {
				label: "First Name:",
				name: "first_name"
			}, {
				label: "Middle Names:",
				name: "middle_names"
			}, {
				label: "Last Name:",
				name: "last_name"
			}, {
				label: "Qualification:",
				name: "qualification"
			},{
				label: "Other Qualifications:",
				name: "oqualifications"
			},{
				label: "Grade:",
				name: "grade"
			},{
				label: "Notch:",
				name: "notch"
			},{
				label: "Current Station:",
				name: "current_station"
			},{
				label: "Previous Stations:",
				name: "previous_stations"
			},{
				label: "Marital Status:",
				name: "marital_status"
			},{
				label: "Spouse:",
				name: "spouse"
			},{
				label: "Spouse No:",
				name: "spouse_no"
			},{
				label: "Children:",
				name: "children"
			}, {
				label: "Mobile:",
				name: "mobile"
			},{
				label: "Mobile2:",
				name: "mobile2"
			},{
				label: "Whatsapp:",
				name: "whatsapp"
			},{
				label: "Email:",
				name: "email"
			},{
				label: "Secondary Lang:",
				name: "sec_lang"
			},{
				label: "Postal Address:",
				name: "postal_add"
			},{
				label: "Emergency Contact:",
				name: "emergency_name"
			},{
				label: "Emergency Number:",
				name: "emergency_no"
			},{
				label: "Relationship:",
				name: "relationship"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#results').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this );
	} );

	$('#results').DataTable( {
		dom: "Bfrtip",
		ajax: "../vendors/editor/examples/php/update_ministers.php",
		order: [[ 1, 'asc' ]],
		columns: [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
			
			{ data: "title" },
			{ data: "first_name" },
			{ data: "middle_names" },
			{ data: "last_name" },
			{ data: "qualification" },
			{ data: "oqualifications" },
			{ data: "grade" },
			{ data: "notch" },
			{ data: "current_station" },
			{ data: "previous_stations" },
			{ data: "marital_status" },
			{ data: "spouse" },
			{ data: "spouse_no" },
			{ data: "children" },
			{ data: "mobile" },
			{ data: "mobile2" },
			{ data: "whatsapp" },
			{ data: "email" },
			{ data: "sec_lang" },
			{ data: "postal_add" },
			{ data: "emergency_name" },
			{ data: "emergency_no" },
			{ data: "relationship" }
			//{ data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
		],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
		buttons: [
			//{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor }
			//{ extend: "remove", editor: editor }
		]
	} );
} );

</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>

