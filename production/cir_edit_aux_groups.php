<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"cir")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'cir_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'cir_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>EDIT AUXILLIARY GROUPS</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
				  
				  
                    <div style="overflow-x: auto;">
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					
                      <thead>
									<tr >
										 <th></th>
											<th>Group Code</th>
											<th>Group Name</th>
											<th>Meeting Days</th>
									</tr>
							</thead >		
							
						<tbody >
					  </tbody> 
					  <tfoot>
									<tr >
											<th></th>
											<th>Group Code</th>
											<th>Group Name</th>
											<th>Meeting Days</th>
									</tr> 
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
					
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
		
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript" language="javascript" class="init">
	


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "../vendors/editor/examples/php/update_aux_groups.php",
		table: "#results",
		fields: [ {
				label:" Group Code:",
				name: "aux_code"
			}, {
				label:" Group Name:",
				name: "aux_name"
			}, {
				label: "Meeting Days:",
				name: "meeting_days"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#results').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this );
	} );

	$('#results').DataTable( {
		dom: "Bfrtip",
		ajax: "../vendors/editor/examples/php/update_aux_groups.php",
		order: [[ 1, 'asc' ]],
		columns: [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
			{ data: "aux_code" },
			{ data: "aux_name" },
			{ data: "meeting_days" }
		],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
		buttons: [
			//{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor },
			//{ extend: "remove", editor: editor }
		]
	} );
} );

</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>

