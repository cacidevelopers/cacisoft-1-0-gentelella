<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"hrm")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'hrm_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'hrm_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>CIRCUIT MEMBERS</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div style="overflow-x: auto;">
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					
					<thead>
									<tr >
										<th >Action</th>
										<th >Photo</th>
										<th >Reg No</th>
										<th >Name</th>
										<th >Mobile</th>
										<th >Qualification</th>
										<th >Profession</th>										
										<th >Occupation</th>
										<th >Member Since</th>	
										<th >Local</th>	
										<th >Marital Status</th>
										<th >Address</th>
										<th >Age</th>									
										<th >Gender</th>
										<th >Family Group</th>
									</tr>
							</thead >
							
						<tbody >
					  </tbody> 
					  <tfoot>
									<tr >
										<th >Action</th>
										<th >Photo</th>
										<th >Reg No</th>
										<th >Name</th>
										<th >Mobile</th>
										<th >Qualification</th>
										<th >Profession</th>										
										<th >Occupation</th>	
										<th >Member Since</th>	
										<th >Local</th>		
										<th >Marital Status</th>
										<th >Address</th>
										<th >Age</th>									
										<th >Gender</th>
										<th >Family Group</th>
									</tr>
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
					</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

<script type="text/javascript">
 $(document).ready(function(){ 

	// Setup - add a text input to each footer cell
	$('#results tfoot th').each( function () {
       // var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search" />' );
		} );
		
//DataTable
 var table=$('#results').DataTable( {
    ajax: {
        url: 'php/hrm_fetch_members.php',
        dataSrc: '',
		 "deferRender": true
    },
    columns: [
			 { data: "Action" },
			 { data: "Photo" },
			 { data: "Reg No" },
			 { data: "Name" },
			 { data: "Mobile" },
      { data: "Qualification" },
			 { data: "Profession" },
			 { data: "Occupation" },
			 { data: "Member Since" },
			 { data: "Local" },
			 { data: "Marital Status" },
			{ data: "Address" },
			{ data: "Age" },
            { data: "Gender" },
			{ data: "Family Group" }
        ]					
} );


 // Apply the search
 table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
		} );

} );
</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>


