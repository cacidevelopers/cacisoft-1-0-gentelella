<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"exe")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'exe_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'exe_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>EDIT VISITORS</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div style="overflow-x: auto;">
					
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					
                      <thead>
									<tr >
										<th></th>										
                                        <!--th>Visitor No</th-->
										<th>Date</th>
										<th>Title</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Mobile</th>
										<th>Whatsapp No</th>
										<th>Nationality</th>
										<th>Purpose</th>
									</tr>
							</thead >		
							
						<tbody >
					  </tbody> 
					  <tfoot>
									<tr >
										<th></th>
										<!--th>Visitor No</th-->
										<th>Date</th>
										<th>Title</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Mobile</th>
										<th>Whatsapp No</th>
										<th>Nationality</th>
										<th>Purpose</th>
									</tr> 
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
					
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
		
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript" language="javascript" class="init">
	


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "../vendors/editor/examples/php/exe_update_visitors.php",
		table: "#results",
		fields: [ {
				label: "Date:",
				name: "reg_date"
			}, {
				label: "Title:",
				name: "title"
			}, {
				label: "First Name:",
				name: "first_name"
			},  {
				label: "Last Name:",
				name: "last_name"
			}, {
				label: "Mobile:",
				name: "mobile"
			}, {
				label: "Whatsapp No:",
				name: "whatsapp"
			},{
				label: "Nationality:",
				name: "nationality"
			},{
				label: "Purpose:",
				name: "purpose"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#results').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this );
	} );

	$('#results').DataTable( {
		dom: "Bfrtip",
		ajax: "#../vendors/editor/examples/php/exe_update_visitors.php",
		order: [[ 1, 'asc' ]],
		columns: [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
		//	{ data: "visitor_id" },
			{ data: "reg_date" },
			{ data: "title" },
			{ data: "first_name" },
			{ data: "last_name" },            
			{ data: "mobile" },
			{ data: "whatsapp" },
			{ data: "nationality" },
			{ data: "purpose" }
			//{ data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
		],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
		buttons: [
			//{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor }
			//{ extend: "remove", editor: editor }
		]
	} );
} );

</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>

