<?php @session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"cir")
{
	header("Location: " . 'index.php');
	
}
include 'php/fill_fam_groups.php';
include 'php/fill_mem_statuses.php';
include 'php/fill_aux_groups.php';

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'cir_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'cir_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
 <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>NEW CREDIT TRANSACTIONS</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
					   
					    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mem_id">Member's ID<span style="color:red;">*</span> 
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mem_id" class="form-control has-feedback-left col-md-7 col-xs-12" name="mem_id" placeholder="Enter Member's User ID" type="text" required="required"  maxlength="9" value="" autofocus>
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Type<span style="color:red;">*</span></label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  required="required" class="form-control has-feedback-left" id="type" name="type">
						    <option value="">Select</option>
							<?php echo $types; ?>  
							
						</select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>	
					  
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="week_no">Week No<span style="color:red;">*</span></label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select   class="form-control has-feedback-left" id="week_no" name="week_no" required="required">
						    <option value="">Select  </option>
							<option value="1">Week 1 </option>
							<option value="2">Week 2 </option>
							<option value="3">Week 3 </option>
							<option value="4">Week 4 </option>
							<option value="5">Week 5 </option>
							  
						</select>
						  <span class="fa fa-search form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount<span style="color:red;">*</span></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="amount" class="form-control has-feedback-left col-md-7 col-xs-12" name="amount" placeholder="GHS" 
						   type="number" step="0.01" min="0" required="required" >
						  <span class="fa fa-money form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
					 				  
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="description"> 160 letters max</label>
                          <textarea  required="required" id="description" name="description" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="320" 
						  style="resize:none"  rows="4" cols="40"  ></textarea>
                        </div> 
                      </div>
					
					
                  <!--MODALS START-->   
                  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Credit Transaction Successfully Saved.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-primary" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-primary">Add New </button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Transaction details was not saved, Please contact Help Desk</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				   <div class="modal fade bs-example-modal-sm" id= "duplicate" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >DUPLICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The amount can't be saved twice!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="dup_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-warning">Add New</button>
                       
                        </div>

                      </div>
                    </div>
                  </div>
				   <div class="modal fade bs-example-modal-sm" id= "ses_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SESSION TIMEOUT!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Sorry the data was not saved!</p>
                            <p>It is either your session has timed out or Network is too slow.</p>
							<p>Please log off, log in and try again.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="ses_cancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          
                        </div>

                      </div>
                    </div>
                  </div> 
				   <!--MODALS END-->
				   
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
					 <input id="save_appointment" type="submit" style="width:100%" class="btn btn-primary btn-md" value="Save Transaction"/>
					  <button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_cir.php' "> Cancel</button>
						
					    </div>
                      </div>
					  
					  <div id= "err" > </div>
					 
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
  
$(document).ready(function (e) {
	// $("#bc_code").attr("disabled", "disabled");
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
  
  $.ajax({
   url: "php/soc_save_ind_credits_data.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
  
   success: function(data)
      {
    
     if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
                }
				if(data.match(/duplicate/gi)){
				
					 $("#duplicate").modal({backdrop: true});
				}			
				if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}
				if(data.match(/session/gi)){
					// alert("PHP error");
					$("#ses_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    $("#err").html(e).fadeIn();
      } 
      
    });
		//action buttons
				//success transaction
				$("#success_add").click(function(){
					location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_cir.php");	
				});
				
				//Session Error
				$("#ses_cancel").click(function(){
					 window.location.replace("index.php");	
				});
				//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				$("#dup_cancel").click(function(){
					 window.location.replace("index_cir.php");	
											
			});
	
 })
 )
});
    </script>
	 	  
	  
 <?php include 'timeout.php'; ?>	
    
  </body>
</html>
