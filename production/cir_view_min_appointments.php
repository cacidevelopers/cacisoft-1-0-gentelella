<?php @session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"cir")
{
	header("Location: " . 'index.php');
	
}
?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'cir_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'cir_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Circuits</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
				  
				  
					  
                    
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
									<tr >
										<th >ID</th>
										<th >Name</th>
										<th >Status</th>
										<th >At</th>
										<th >From</th>
										<th >To</th>
									</tr>
									</thead >						
									<tbody >
									
									
									</tbody> 
									<tfoot>
									<tr >
										<th >ID</th>
										<th >Name</th>
										<th >Status</th>
										<th >At</th>
										<th >From</th>
										<th >To</th>
									</tr>
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
 $(document).ready(function(){  
     
 table1=$('#results').DataTable( {
    ajax: {
        url: 'php/fetch_min_appointments.php',
        dataSrc: '',
		 "deferRender": true
    },
    columns: [
				
				{ data: "ID" },
				{ data: "Name" },
				{ data: "Status" },
				{ data: "At" },
				{ data: "From" },
				{ data: "To" }
			]
										
} );
} ); 
</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>
