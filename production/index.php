<?php
session_start();
 //unset($_SESSION['user_session']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
 
    <meta http-equiv="Content-Type" content="text/html; charset=ANSI">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CACI-ERP</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
	<!-- Start of tecman Zendesk Widget script -->
	
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},
window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",
d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e)
{n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");
n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,
this.zendeskHost="tecmanlimited.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>
<!-- End of tecmanlimited Zendesk Widget script -->
	

  </head>

<body style="background:#b3d7ff;">
    <div class="">
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
 
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
		  
		 	 <img src="images/cacilogo.png" alt="..."  width="30%" height="25%" ><br />	
		 	
           <form class="form-horizontal form-label-left" method="post"   id="li">
               <h5><font color='red'>CACISoft v1.0.0</font></h5>
			   
			     <div class="alert alert-success" id="results" role="alert">
						
				</div>
				 <div class="alert alert-danger" id="error" role="alert">
							
				</div>
              <div>
                <input type="text" class="form-control" placeholder="User ID" id="user_id" autofocus required="required" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="password" id="password" required="required" />
              </div>
              <div class="form-group ">
                 <div  class="btn-group btn-group-justified">
				 <input id="login" type="submit" class="btn btn-primary btn-md " style="width:100%" value="login"/>
				
				<h4><a class="reset_pass fa fa-home" href="https://cac-int.org" style="width:78%; color:red;"> Back to Homepage </a>
				<a class="reset_pass fa fa-user" href="min_registration.php" style="width:78%; color:blue;"> Minister Registration </a>
				<a class="reset_pass fa fa-user" href="mem_registration.php" style="width:78%; color:blue;"> Member Registration </a>
				<a class="reset_pass fa fa-unlock" href="#recover_pass.php" style="width:78%; color:blue;">      Recover lost password?</a></h4>
              </div>
			  </div>
			 

			 <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
               		
                  <div>
                 <p> <h1><font color='red'></font><i class=""></i></h1><font color='#ffffff'>  </font></br>
				  <a href="https://www.cac-int.org"><font color='red'> <p> The Christ Apostolic Church International:</p> Enterprise Resource Planner: [CACISoft version 1.0.0]</a></font></p><p style="color:red">©<?php echo date('Y'); ?></p>
                </div>
              </div>
			  
			  
            </form>
          </section>
        </div>        
       </div>
      </div>
	  
  <?php include 'javascripts.php'; ?> 
   <!-- Flot -->
    <!--script src="../vendors/Flot/jquery.flot.js"></script-->
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
  
 <script type="text/javascript" >
$(document).ready(function (e) {
	
	$('#results').hide();
	$('#error').hide();
 $("#li").on('submit',(function(e) {
  e.preventDefault();
 var user_id = $("#user_id").val();
 var password = $("#password").val();
 
 //alert(user_id);
 //alert(password);
  $.ajax({
  
   type: "POST",
   data:  {
	   user_id:user_id,
	    password:password
		},
	url: "php/authenticate.php",
   dataType: 'html',
         cache: false,
  
   success: function(data)
      {
						console.log(data);
							if(data.match(/mem/g)){
								$('#error').hide();
								$('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful!</p><p> Loading Homepage ...</font></p>");
								$('#results').show();
								setTimeout(function(){	window.location="index_mem.php";},2000);	
							}
							else if(data.match(/aux/g)){
								$('#error').hide();
								$('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful!</p><p> Loading Homepage ...</font></p>");
								$('#results').show(); 
								setTimeout(function(){	window.location="aux_leaders_authentication.php";},2000);
								//setTimeout(function(){	window.location="org_leaders_authentication.php";},2000);
							}
							else if(data.match(/fam/g)){
								$('#error').hide();
								$('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p> Loading Homepage ...</font></p>");											
								$('#results').show(); 
								setTimeout(function(){	window.location="fam_leaders_authentication.php";},2000);
								//setTimeout(function(){	window.location="bib_leaders_authentication.php";},2000);
							}
							else if(data.match(/loc/g)){
								$('#error').hide();
								$('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful!</p><p> Loading Homepage ...</font></p>");											
								$('#results').show(); 
								setTimeout(function(){	window.location="index_loc.php";},2000);
							}
							else if(data.match(/lfs/g)){
								$('#error').hide();
								$('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful!</p><p> Loading Homepage ...</font></p>");
								$('#results').show(); 
								setTimeout(function(){	window.location="index_lfs.php";},2000);
							}
							else if(data.match(/ict/)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_ict.php";},2000);							
							 }
							else if(data.match(/cir/)){
								$('#error').hide();
								$('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");											
								$('#results').show(); 
								setTimeout(function(){	window.location="index_cir.php";},2000);
							}
							else if(data.match(/cfs/g)){
								$('#error').hide();
								$('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful!</p><p> Loading Homepage ...</font></p>");											
								$('#results').show();
								setTimeout(function(){	window.location="index_cfs.php";},2000);
							
							 }
							 else if(data.match(/are/g)){
								 $('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");													
								$('#results').show();
								setTimeout(function(){	window.location="index_are.php";},2000);
							
							 }
							else if(data.match(/afs/g)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_afs.php";},2000);							
							 }
							 else if(data.match(/ter/g)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_ter.php";},2000);							
							 }
							  else if(data.match(/tfs/g)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_tfs.php";},2000);							
							 }
							  else if(data.match(/acc/g)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_acc.php";},2000);							
							 }
							  else if(data.match(/hrm/g)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_hrm.php";},2000);							
							 }
							  else if(data.match(/adm/g)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_adm.php";},2000);							
							 }
							  else if(data.match(/exe/g)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_exe.php";},2000);							
							 }
							  
							  else if(data.match(/sup/g)){
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_sup.php";},2000);							
							 }
							  else if(data.match(/hdq/g)){
							  alert("success");
								$('#error').hide();
								 $('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Login Successful! </p><p>Loading Homepage ...</font></p>");												
								$('#results').show();
								setTimeout(function(){	window.location="index_hdq.php";},2000);							
							 }
							 
							 else if(data.match(/offline/g)){
								$('#error').html("");
								
								$('#error').html("<p><sytle= 'font-family:courier; color:white;'>NETWORK ERROR! <br />Please check your internet connection.</font></p>");								
								$('#error').show();
							}
						
							else if(data.match(/denied/g)){
								$('#error').html("");
								
								$('#error').html("<p><sytle= 'font-family:courier; color:white;'>Incorrect Username/Password! Please try again.</font></p>");
														
								$('#error').show();
							
							}
								
							 else{
								$('#error').html("");
								
								$('#error').html("<p><sytle= 'font-family:courier; color:white;'>SYSTEM ERROR! Please contact Help Desk.</font></p>");
														
								$('#error').show();
							 }
											
				},
							
		error: function(e) {
						$('#error').html("");
							
							$('#error').html("<p><sytle= 'font-family:courier; color:white;'>NETWORK ERROR! <br />Please check your internet connection.</font></p>");	
													
							$('#error').show();
						
				}			
					
			
		});
 }

		
	 
 
 )
)
});

</script>
  </body>
</html>