    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu"  >
              <div class="menu_section" >
                <br/><br/><br/><br/>
                <ul class="nav side-menu" >
                 
				 <li><a class="ajax-link" href="index_cir.php"><i class="fa fa-tachometer"></i><span> Dashboard</span></a>
                 </li>
				 
				 <li><a><i class="fa fa-sitemap"></i> Church Statistics<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       
						<li><a>National<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						  <li><a href="#cir_view_nat_summaries.php">National Summaries</a>
                            </li>							
                          </ul>
                        </li>
                        <li><a>Territory<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            
                            <li><a href="cir_view_ter_summaries.php">View Territory Summaries </a>
                            </li>
                            						
                          </ul>
                        </li>
						 <li><a>Areas<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="cir_view_are_summaries.php">View Areas Summaries</a>							
                            </li>
						  </ul>
                        </li>
						 <li><a>Circuits<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
							<li><a href="#cir_edit_circuit.php">Edit Circuit</a></li>
                            <li><a href="cir_view_cir_summaries.php">View Circuits Summaries</a>
                            </li>
                          </ul>
                        </li>
						<li><a>Family Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="cir_add_fam_groups.php">Create Family Group</a></li>
						  <li><a href="cir_edit_fam_groups.php">View/Edit Family Group</a></li>
						</ul>
                        </li>
						
						 <li><a>Auxilliary Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="cir_add_aux_groups.php">Create Aux Groups</a></li>
						  <li><a href="cir_edit_aux_groups.php">View/Edit Aux Groups</a></li>
						</ul>
                        </li>
					</ul>
                  </li>
				  				  
				   <li><a><i class="fa fa-users"></i>Members<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						   <li><a href="cir_register_members.php">Add Members</a></li>  
						   <li><a href="cir_edit_members.php">Edit  Members</a></li>
						   <li><a href="cir_view_members.php">View Members</a></li>
							</ul>
                  </li>
				  
				  <li><a><i class="fa fa-book"></i>Sermons<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="cir_auth_sermon_upload.php">Add New Sermon</a></li>
                     <li><a href="cir_view_sermons.php">View Sermons</a></li>
					  <!--li><a href="#delete_sermons.php">Delete My Sermons</a></li-->
                    </ul>
                  </li>
				   <li><a><i class="fa fa-folder-open"></i>Drop Box<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					 <li><a href="#cir_add_documents.php">Upload Documents</a>
                            </li>
							 <li><a href="#cir_view_documents.php">View Documents</a>
                            </li>
							 <!--li><a href="#cir_view_report_summaries.php">Summaries</a></li-->
                    </ul>
                  </li>
				 <li><a><i class="fa fa-money"></i> Contributions<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					
						 <li><a>Income: Individuals<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                             <li><a href="cir_add_ind_credit_transactions.php">New Transactions</a></li>
							 <!--li><a href="cir_edit_ind_credit_transactions.php">Edit Transactions</a></li> 
							 <!--li><a href="#cir_view_cong_credit_transactions.php">Reports</a>							
							 </li-->
						</ul>
                        </li>
						<li><a>Income: Congregational<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                             <li><a href="#cir_add_cong_credit_transactions.php">New Transactions</a></li> 
							 <!--li><a href="cir_edit_cong_credit_transactions.php">Edit Transactions</a>							
							 </li> 
							 <!--li><a href="#cir_view_cong_credit_transactions.php">Reports</a>							
							 </li-->
						</ul>
                        </li>
						 <li><a>Expenses<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="#cir_add_debit_transactions.php">New Transactions</a>
                            </li>
							 <li><a href="#cir_edit_debit_transactions.php">Edit Transactions</a>
                            </li>
                           
                          </ul>
                        </li>
						 <li><a>Reports<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="#cir_fin_reports.php">Reports</a></li>
						</ul>
                        </li>
												
                    </ul>
                  </li>
				  <li><a><i class="fa fa-users"></i>Visitors<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="cir_add_visitors.php">Add Visitors</a></li>
						<li><a href="cir_edit_visitors.php">View/Edit Visitors</a></li>
					   <!--li><a href="#cir_view_visitors.php">View Visitors</a></li-->
                    </ul>
                   </li>
				   	   <li><a><i class="fa fa-flag"></i> Requests<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Transfers<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <!--li><a href="cir_unprocessed_transfer_requests.php">View Pending Requests</a></li-->
							  <!--li><a href="cir_process_transfers_out_auth.php">Process Outgoing Requests</a></li-->
							  <!--li><a href="cir_process_transfers_in_auth.php">Process Incoming Requests</a></li-->
							  <li><a href="#cir_unprocessed_transfer_in_requests.php">Process Incoming Requests</a></li>
							  <li><a href="#cir_unprocessed_transfer_out_requests.php">Process Outgoing Requests</a></li>
							<li><a href="#cir_new_transfers.php">My New Requests</a></li>
							  <li><a href="#cir_edit_my_transfers.php">Edit My Requests</a></li>
							  <li><a href="#cir_view_my_transfer_requests.php">View My Requests</a></li>
							  <li><a href="#cir_view_my_approved_transfer_request.php">My Approved Requests</a></li>
							  <li><a href="#cir_view_my_transfer_history.php">View My Transfer History</a></li>
							  
                          </ul>
                        </li> 
						<li><a>Burials<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <!--li><a href="cir_unprocessed_transfer_requests.php">View Pending Requests</a></li-->
							  <li><a href="#cir_process_burial_requests.php">Process Pending Burial Requests</a></li>
							  <li><a href="#cir_new_burial_requests.php">My New Requests</a></li>
							  <li><a href="#cir_edit_my_burial_requests.php">Edit My Requests</a></li>
							  <li><a href="#cir_view_my_burial_request_history.php">View My Request History</a></li>
							  
                          </ul>
                        </li>
                        <li><a>Other Requests<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
								<li><a href="#cir_process_pending_requests.php">Process Pending Requests</a></li>
								<li><a href="#cir_new_requests.php">My New Requests</a></li>
								<li><a href="#cir_edit_my_requests.php">Edit My Requests</a></li>
								<li><a href="#cir_view_my_pending_requests.php">My Pending Requests</a></li>
								<li><a href="#cir_view_my_processed_requests.php">My Processed Requests</a></li>
                          </ul>
                        </li>
                    </ul>
                  </li> 
				  <li><a><i class="fa fa-arrows	"></i>Departed<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="#cir_add_departed.php">New Departed</a></li>
						<li><a href="#cir_edit_departed.php">Edit Departed</a></li>
					   <li><a href="#cir_view_departed.php">View Departed</a></li>
                    </ul>
                   </li>
				<!--li><a><i class="fa fa-money"></i>Declarations<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <!--li><a href="cir_view_financial_summaries.php">National Summaries</a></li>
                       <li><a href="cir_view_cir_declarations.php">View Declarations</a></li>
                       <!--li><a href="cir_redeem_cir_declarations.php">Redeem Declarations</a></li>
						
                    </ul>
                   </li-->
				   
				   <li><a><i class="fa fa-bell"></i>Meetings<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					<li><a href="#cir_add_meetings.php">Add New Schedule</a></li>
					<li><a href="#cir_edit_meetings.php">Edit Schedules</a></li>
					 <li><a href="#cir_view_scheduled_meetings.php">View Scheduled Meetings</a></li>
					 <li><a href="#cir_view_archived_meetings.php">View Archived Meetings</a></li>
                    </ul>
                  </li>
				  
				  <li><a><i class="fa fa-list-alt"></i>Phone Book<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Contact Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						    <li><a href="#cir_add_contact_groups.php">Create New Groups</a></li>
							<li><a href="#cir_edit_contact_groups.php">View/Edit Contact Groups</a></li>
                          </ul>
                        </li>
                        <li><a>Contacts<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="#cir_add_contacts.php">Add Contacts to Groups</a>
                            </li> 
							<li><a href="#cir_select_contact_groups.php">View/Edit Contacts</a>
                            </li>
                          </ul>
                        </li>
                    </ul>
                  </li>  

				  <li><a><i class="fa fa-bullhorn"></i>Announcements<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="#cir_send_announcements.php">Send Announcements</a>
                            </li>
							 <!--li><a href="#cir_view_announcements.php">View Notices</a>
                            </li-->
                         </ul>
                    </li>
					
									  
				  <li><a><i class="fa fa-certificate"></i> Official Status<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Ministerial Status<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">						
						  <li><a href="#cir_add_min_statuses.php">Add New Status</a></li>
							 <li><a href="#cir_edit_min_status.php">Edit Status</a></li>
							<li><a href="#cir_view_min_statuses.php">View Status</a></li>
                          </ul>
                        </li>
                        <li><a>Lay Status<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="#cir_add_lay_statuses.php">Add New Status</a></li>
							 <li><a href="#cir_edit_lay_status.php">Edit Status</a></li>
							<li><a href="#cir_view_lay_statuses.php">View Status</a></li>
                          </ul>
                        </li>
                    </ul>
                  </li> 
				  
					<li><a><i class="fa fa-briefcase"></i> Appointments<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Ministerial Appointments<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						
						   <!--li><a href="#cir_add_min_appointments.php">New Appointments</a>
                            </li>
						  <li><a href="#cir_edit_min_appointments.php">Edit Appointments</a>
                            </li-->
							 <li><a href="cir_view_min_appointments.php">View Appointments</a>
                            </li>
							  <!--li><a href="cir_fetch_min_appointments_summaries.php">Summaries</a></li-->
                          </ul>
                        </li>
						
                        <li><a>Lay Appointments<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                       <!--li><a href="cir_fetch_appointments_summaries.php">Summaries</a></li-->
						   <li><a href="cir_add_mem_appointments.php">New Appointments</a>
                            </li>
							<li><a href="#cir_edit_lay_appointments.php">Edit Appointments</a>
                            </li>
							 <li><a href="cir_view_mem_appointments.php">View Appointments</a>
                            </li>
						 
                          </ul>
                        </li>
                    </ul>
                  </li> 
				 <li><a><i class="fa fa-key"></i>Access Levels<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="cir_edit_access.php">Edit Access Levels</a></li>  
                       <li><a href="#cir_auth_portal_change.php">Change Portal</a></li>  
                    </ul>
                  </li>
				    <li><a><i class="fa fa-magic"></i>Retrieve Lost ID Cards<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <!--li><a href="cir_retrieve_lost_mem_ids.php">Member ID Card</a></li-->
                       <li><a href="#cir_retrieve_lost_min_ids.php">Minister ID Card</a></li>
                    </ul>
                   </li>
				    <!--li><a><i class="fa fa-usd"></i>SMS Billing<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#cir_view_current_sms_bill.php">View Current SMS Bill</a></li>
					   <li><a href="#cir_view_all_sms_bills.php">View SMS Billing History</a></li>
                    </ul>
                   </li-->
                </ul>
              </div>
            </div>