<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"exe")
{
	header("Location: " . 'index.php');
	
}
include 'php/exe_fetch_top_tiles_summaries.php';
?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'exe_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'exe_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

		<!--========== page content =======-->
		<div class="right_col" role="main">
		 <!-- top tiles -->
		   <!--div class="row tile_count">
		  
		   
           <div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Income (GHS)</span>
              <div class="count green">  <?php echo $con_income; ?></div>
             
            </div>  
			<div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Expenses (GHS)</span>
              <div class="count red">  <?php echo $con_debits; ?></div>
             
            </div> 
			<div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Account Balance (GHS)</span>
              <div class="count blue">  <?php echo $con_acc_bal ?></div>
             
            </div> <br />
			<div class="col-md-3 col-sm-6 col-xs-6 tile_stats_count ">
              <span class="count_top"><i class="fa fa-users"></i> Total Membership</span>
              <div class="count red">  <?php echo $con_membership; ?></div>
             
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Aged Males</span>
              <div class="count red">  <?php echo $all_aged_males; ?></div>
             
            </div> 
			 <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i>Aged Females</span>
              <div class="count red">  <?php echo $all_aged_females; ?></div>
             
            </div>
           <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i>Male Adults</span>
              <div class="count blue">  <?php echo $all_male_adults; ?></div>
             
            </div> 
			 <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i>Female Adults</span>
              <div class="count blue">  <?php echo $all_females_adults; ?></div>
             
            </div>
			 <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i>Young Male Adults</span>
              <div class="count blue">  <?php echo $all_y_m_adults; ?></div>
             
            </div>
			 <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i>Young Female Adults</span>
              <div class="count blue">  <?php echo $all_y_f_adults; ?></div>
             
            </div>
			 <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i>Male Youth</span>
              <div class="count green">  <?php echo $all_m_youth; ?></div>
             
            </div>
			 <div class="col-md-3 col-sm-3 col-xs-3 col-lg-3  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i>Female Youth</span>
              <div class="count green">  <?php echo $all_f_youth; ?></div>
             
            </div>
			 <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i>Boys</span>
              <div class="count green">  <?php echo $all_boys; ?></div>
             
            </div>
			 <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6  tile_stats_count" >
              <span class="count_top"><i class="fa fa-user"></i>Girls</span>
              <div class="count green">  <?php echo $all_girls; ?></div>
             
            </div>
          </div-->
			
			 <div class="row">
          
			
	<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
              <div class="x_panel tile fixed_height_320 width" >
                <div class="x_title">
                  <h2>Membership Distribution</h2>
                 
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                 
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Aged [60+]</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-red" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $aged_p;?>%;">
                          <span class="sr-only"><?php echo $aged_p;?>%</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_55">
                      <span><?php echo $aged;?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>

                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Adults [45-59]</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $adults_p;?>%;">
                          <span class="sr-only"><?php echo $adult_p;?>% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_55">
                      <span><?php echo $adults;?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Young Adults [35-44]</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $yadults_p;?>%;">
                          <span class="sr-only"><?php echo $yadult_p;?>% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_55">
                      <span><?php echo $yadults;?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Youth [18-34]</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $youth_p;?>%;">
                          <span class="sr-only"><?php echo $youth_p;?>% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_55">
                      <span><?php echo $youth;?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>Junior [0-17)</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-black" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $children_p;?>%;">
                          <span class="sr-only"><?php echo $children_p;?>% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_55">
                      <span><?php echo $children;?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>

				  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span>TOTAL MEMBERSHIP</span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $cent;?>%;">
                          <span class="sr-only"><?php echo $cent;?>% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_55">
                      <span><?php echo $cac_membership;?></span>
                    </div>
                    <div class="clearfix"></div>
                  </div>

                </div>
              </div>
            </div>	
 <!--end of membership graph-->
 
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                   <h2>Revenue for Week <?php echo $week.", ".$mon." ".$year."." ; ?></h2>
                 
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <th style="width:37%;">
                        <p>Distribution</p>
                      </th>
                      <th>
                        <div class="col-lg-9 col-md- col-sm-9 col-xs-9">
                          <p class="">40% Connexional Levy (GHS)</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                          <p class="">Percentage</p>
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas id="canvas1" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                          <tr>
                            <td>
                              <p><i class="fa fa-square red"></i>Tithe:        <?php echo $tithe_sum;?></p>
                            </td>
                            <td><?php echo $tithe_p;?>%</td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square purple"></i>Offering: <?php echo $offering_sum;?></p>
                            </td>
                            <td><?php echo $offering_p;?>%</td>
                          </tr>
                          <!--tr>
                            <td>
                              <p><i class="fa fa-square aero"></i>Kofi & Ama:  <?php echo $kofi_ama_sum;?></p>
                            </td>
                            <td><?php echo $kofi_ama_p;?>%</td>
                          </tr-->
                          <tr>
                            <td>
                              <p><i class="fa fa-square green"></i>Thanks Offering:  <?php echo $thanks_sum;?></p>
                            </td>
                            <td><?php echo $thanks_p;?>%</td>
                          </tr>
                          <!--tr>
                            <td>
                              <p><i class="fa fa-square blue"></i>So mu bi:  <?php echo $som_sum;?></p>
                            </td>
                            <td><?php echo $som_p;?>%</td>
                          </tr-->
						  
						  <tr>
                            <td>
                             <h3> <p><i class="fa fa-square white"></i>TOTAL:  <?php echo $total_income;?></p></h3>
                            </td>
                            <td>100%</td>
                          </tr> 
						 
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
          </div>
<!-- end of revenue sources--> 
<!--div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Account Balance</h2>
					
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">
                   
                    <div class="sidebar-widget">
                      <h4>Expenditure/Balance (%)</h4>
                      <canvas width="150" height="80" id="foo" class="" style="width: 160px; height: 100px;"></canvas>
                      <div class="goal-wrapper">
                        <span class="gauge-value pull-left"></span>
                        <span id="gauge-text" class="gauge-value pull-left"><?php echo $con_debits_p;?></span>
                        <span id="goal-text" class="goal-value pull-right"> <?php echo $con_acc_bal_p;?></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div-->
<!---endo of graphical account balance-->	 
			
          </div>
          </div>
		</div>
		
		
		
         
<!--======== /page content ==========-->

        <!-- footer content -->
			<?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    

    <!-- jQuery -->
	  <?php include 'javascripts.php'; ?>
	
	<!-- /jQuery -->
	  <script>
      $(document).ready(function() {
        var data1 = [
          [gd(2012, 1, 1), 17],
          [gd(2012, 1, 2), 74],
          [gd(2012, 1, 3), 6],
          [gd(2012, 1, 4), 39],
          [gd(2012, 1, 5), 20],
          [gd(2012, 1, 6), 85],
          [gd(2012, 1, 7), 7]
        ];

        var data2 = [
          [gd(2012, 1, 1), 82],
          [gd(2012, 1, 2), 23],
          [gd(2012, 1, 3), 66],
          [gd(2012, 1, 4), 9],
          [gd(2012, 1, 5), 119],
          [gd(2012, 1, 6), 6],
          [gd(2012, 1, 7), 9]
        ];  
		
		
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
          data1, data2
        ], {
          series: {
            lines: {
              show: false,
              fill: true
            },
            splines: {
              show: true,
              tension: 0.4,
              lineWidth: 1,
              fill: 0.4
            },
            points: {
              radius: 0,
              show: true
            },
            shadowSize: 2
          },
          grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
          },
          colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
          xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "day"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
          },
          yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
          },
          tooltip: false
        });

        function gd(year, month, day) {
          return new Date(year, month - 1, day).getTime();
        }
      });
    </script>
    <!-- /Flot -->
	  <!-- Doughnut Chart -->
    <script>
      $(document).ready(function(){
        var options = {
          legend: false,
          responsive: false,
        };
	
		//var aged='<?php echo $aged; ?>';
		var tithe='<?php echo $tithe_p; ?>';
		var offering='<?php echo $offering_p; ?>';
		var kofi_ama='<?php echo $kofi_ama_p; ?>';
		var thanks='<?php echo $thanks_p; ?>';
		var som='<?php echo $som_p; ?>';
		
			
	
        new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: [
              "Tithe",
              "Offering",
			   "Kofi & Ama",
              "Thanks Offering",
			  "So mu bi" 
            
              
            ],
            datasets: [{
              data: [tithe, offering, kofi_ama, thanks, som],
              backgroundColor: [
			  
                "#E74C3C",
                "#9B59B6",
                "#BDC3C7",
                "#26B99A",
                "#3498DB"
				
                
              ],
              hoverBackgroundColor: [
               
                "#9B59B6",
                "#E74C3C",
                "#26B99A",
                "#3498DB",
				"#BDC3C7"
                
              ]
            }]
          },
          options: options
        });
      });
    </script>
    <!-- /Doughnut Chart -->
	 <!-- gauge.js -->
    <script>
      var opts = {
          lines: 12,
          angle: 0,
          lineWidth: 0.4,
          pointer: {
              length: 0.75,
              strokeWidth: 0.042,
              color: '#1D212A'
          },
          limitMax: 'false',
          colorStart: '#E74C3C',
          colorStop: '#1ABC9C',
          strokeColor: '#26B99A',
          generateGradient: true
      };
      var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue =100;
      gauge.animationSpeed = 50;
    //  gauge.set(<?php echo $con_debits_p;?>);
      gauge.setTextField(document.getElementById("gauge-text"));
    </script>
    <!-- /gauge.js -->
	
     <!-- jQuery AJAX -->
   
    <?php include 'timeout.php'; ?>
    
	
    <!-- /gauge.js -->
  </body>
</html>
