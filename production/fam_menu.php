    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu"  >
              <div class="menu_section" >
                <br/><br/><br/><br/>
                <ul class="nav side-menu" >
                 
				 <li><a class="ajax-link" href="index_fam.php"><i class="fa fa-tachometer"></i><span> Dashboard</span></a>
                 </li>
				 <li><a><i class="fa fa-edit"></i>MGCM Registration<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						    <li><a href="#fam_view_gpc_registrants.php">View Registrants</a></li>
							</ul>
                  </li>
				 <li><a><i class="fa fa-sitemap"></i> Church Statistics<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <!--li><a>National<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						  <li><a href="#fam_view_nat_summaries.php">National Summaries</a>
                            </li>
                           <li><a href="fam_view_National.php">View National</a>
                            </li>
                            <li><a href="#fam_edit_National.php">Edit National</a>
                            </li>
							
                          </ul>
                        </li-->
                        <li><a>Territory<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <!--li class="sub_menu"><a href="fam_view_ter_summaries.php">Territory Summaries</a>
                            </li-->
							 <li><a href="fam_create_territory.php">Create Territory </a></li>
							<li><a href="#fam_edit_territory.php">Edit Territory</a></li>
                            <li><a href="fam_view_ter_summaries.php">View Territory Summaries </a>
                            </li>
                            						
                          </ul>
                        </li>
						 <li><a>Areas<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="fam_create_area.php">Create New Area </a></li>
							<li><a href="#fam_edit_area.php">Edit Areas</a></li>
                            <li><a href="fam_view_are_summaries.php">View Areas Summaries</a>							
                            </li>
						  </ul>
                        </li>
						 <li><a>Circuits<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="fam_create_circuit.php">Create New Circuit </a></li>
							<li><a href="#fam_edit_circuit.php">Edit Circuits</a></li>
                            <li><a href="fam_view_cir_summaries.php">View Circuits Summaries</a>
                            </li>
                          </ul>
                        </li>
						<!--li><a>Family Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="#fam_setup_family_groups.php">Create Family Group</a></li>
						  <li><a href="#fam_edit_family_groups.php">View/Edit Family Group</a></li>
						</ul>
                        </li-->
						
						 <!--li><a>Auxilliary Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="#fam_setup_aux_groups.php">Create Aux Groups</a></li>
						  <li><a href="#fam_edit_aux_groups.php">View/Edit Aux Groups</a></li>
						</ul>
                        </li-->
					</ul>
                  </li>
				  <li><a><i class="fa fa-user"></i>Ministers<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						    <li><a href="fam_register_ministers.php">Register New Minister</a>  </li>
                            <li><a href="fam_edit_ministers.php">Edit Ministers</a></li>
							<li><a href="fam_view_ministers.php">View Ministers</a></li>
							</ul>
                  </li>
				  
				   <li><a><i class="fa fa-users"></i>Members<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						   <li><a href="fam_register_members.php">Add Members</a></li>  
						   <li><a href="#fam_edit_members.php">Edit  Members</a></li>
						   <li><a href="fam_view_members.php">View Members</a></li>
							</ul>
                  </li>
				  
				  <li><a><i class="fa fa-book"></i>Sermons<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="#fam_auth_sermon_upload.php">Add New Sermon</a></li>
                     <li><a href="#fam_view_sermons.php">View Sermons</a></li>
					  <!--li><a href="#delete_sermons.php">Delete My Sermons</a></li-->
                    </ul>
                  </li>
				   <li><a><i class="fa fa-bar-chart"></i>Reports<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					 <!--li><a href="#fam_add_reports.php">Add New Circular</a>
                            </li-->
							 <li><a href="#fam_view_reports.php">View Reports</a>
                            </li>
							 <!--li><a href="#fam_view_report_summaries.php">Summaries</a></li-->
                    </ul>
                  </li>
				 <li><a class="ajax-link" href="#fam_fin_reports.php"><i class="fa fa-money"></i><span>My Contributions</span></a>
                 </li>
				
				   <li><a><i class="fa fa-flag"></i>My Requests<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Transfers<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
							<li><a href="fam_new_transfers.php">New Requests</a></li>
							  <li><a href="#fam_edit_my_transfers.php">Edit Requests</a></li>
							  <li><a href="#fam_view_my_transfer_requests.php">View Requests</a></li>
							  <li><a href="#fam_view_my_approved_transfer_request.php">Approved Requests</a></li>
							  <li><a href="#fam_view_my_transfer_history.php">View Transfer History</a></li>
                          </ul>
                        </li>
						<li><a>Burials<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <!--li><a href="fam_unprocessed_transfer_requests.php">View Pending Requests</a></li-->
							  <li><a href="#fam_new_burial_requests.php">New Requests</a></li>
							  <li><a href="#fam_edit_my_burial_requests.php">Edit Requests</a></li>
							  <li><a href="#fam_view_my_burial_request_history.php">View Requests History</a></li>
							  
                          </ul>
                        </li>
                        <li><a>Other Requests<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
								<li><a href="#fam_new_requests.php">New Requests</a></li>
								<li><a href="#fam_edit_my_requests.php">Edit Requests</a></li>
								<li><a href="#fam_view_pending_requests.php">Pending Requests</a></li>
								<li><a href="#fam_view_processed_requests.php">Processed Requests</a></li>
								
                          </ul>
                        </li>
                    </ul>
                  </li> 
				   
				   <li><a><i class="fa fa-bell"></i>Meetings<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					<li><a href="#fam_add_meetings.php">Add New Schedule</a></li>
					<li><a href="#fam_edit_meetings.php">Edit Schedules</a></li>
					 <li><a href="#fam_view_scheduled_meetings.php">View Scheduled Meetings</a></li>
					 <li><a href="#fam_view_archived_meetings.php">View Archived Meetings</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-folder-open"></i>Drop Box<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					 <li><a href="#cir_add_documents.php">Upload Documents</a>
                            </li>
							 <li><a href="#cir_view_documents.php">View Documents</a>
                            </li>
							 <!--li><a href="#cir_view_report_summaries.php">Summaries</a></li-->
                    </ul>
                  </li>
				  <li><a><i class="fa fa-list-alt"></i>Phone Book<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Contact Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						    <li><a href="#fam_add_contact_groups.php">Create New Groups</a></li>
							<li><a href="#fam_edit_contact_groups.php">View/Edit Contact Groups</a></li>
                          </ul>
                        </li>
                        <li><a>Contacts<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="#fam_add_contacts.php">Add Contacts to Groups</a>
                            </li> 
							<li><a href="#fam_select_contact_groups.php">View/Edit Contacts</a>
                            </li>
                          </ul>
                        </li>
                    </ul>
                  </li>  

				  <li><a><i class="fa fa-bullhorn"></i>Notifications<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="#fam_send_announcements.php">Send Notices</a>
                            </li>
							 <!--li><a href="#fam_view_announcements.php">View Notices</a>
                            </li-->
                         </ul>
                    </li>
					
									  
				  <li><a><i class="fa fa-certificate"></i> Official Status<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Ministerial Status<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">						
						  <li><a href="#fam_add_min_statuses.php">Add New Status</a></li>
							 <li><a href="#fam_edit_min_status.php">Edit Status</a></li>
							<li><a href="#fam_view_min_statuses.php">View Status</a></li>
                          </ul>
                        </li>
                        <li><a>Lay Status<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="#fam_add_lay_statuses.php">Add New Status</a></li>
							 <li><a href="#fam_edit_lay_status.php">Edit Status</a></li>
							<li><a href="#fam_view_lay_statuses.php">View Status</a></li>
                          </ul>
                        </li>
                    </ul>
                  </li> 
				  
					<li><a><i class="fa fa-briefcase"></i> Appointments<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Ministerial Appointments<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						
						   <li><a href="fam_add_min_appointments.php">New Appointments</a>
                            </li>
						  <li><a href="#fam_edit_min_appointments.php">Edit Appointments</a>
                            </li>
							 <li><a href="fam_view_min_appointments.php">View Appointments</a>
                            </li>
							  <!--li><a href="fam_fetch_min_appointments_summaries.php">Summaries</a></li-->
                          </ul>
                        </li>
						
                        <li><a>Lay Appointments<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                             <!--li><a href="fam_fetch_appointments_summaries.php">Summaries</a></li-->
						   <li><a href="#fam_add_mem_appointments.php">New Appointments</a>
                            </li>
							<li><a href="#fam_edit_lay_appointments.php">Edit Appointments</a>
                            </li>
							 <li><a href="#fam_view_appointments.php">View Appointments</a>
                            </li>
						 
                          </ul>
                        </li>
                    </ul>
                  </li> 
				 <li><a><i class="fa fa-key"></i>Access Levels<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="#fam_edit_access.php">Edit Access Levels</a></li>  
                       <li><a href="#fam_auth_portal_change.php">Change Portal</a></li>  
                    </ul>
                  </li>
				    <li><a><i class="fa fa-magic"></i>Retrieve Lost ID Cards<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <!--li><a href="fam_retrieve_lost_fam_ids.php">famber ID Card</a></li-->
                       <li><a href="#fam_retrieve_lost_min_ids.php">Minister ID Card</a></li>
                    </ul>
                   </li>
				    <li><a><i class="fa fa-usd"></i>SMS Billing<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#fam_view_current_sms_bill.php">View Current SMS Bill</a></li>
					   <li><a href="#fam_view_all_sms_bills.php">View SMS Billing History</a></li>
                    </ul>
                   </li>
                </ul>
              </div>
            </div>