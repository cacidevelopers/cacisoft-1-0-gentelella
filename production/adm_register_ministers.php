<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"adm")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'adm_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'adm_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

      <!-- page content -->
       <div class="right_col" role="main">
        <div class="">

             <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
              <!--div class="title_left"-->
			   <center><img src="images/caci-logo.png" alt="..."  width="40x" height="40px" style =" color:red; min-width:10px; min-height=10px; padding: 0px 0px 0px 0px margin:0px 50px 0px 0px" ><font color='blue'><br/><h3>The&#8239;Christ&#8239;Apostolic&#8239;Church&#8239;Int.</h3><p><h2>&#8239;Ministerial&#8239;Registration&#8239;Form</h2></p></font> </center>
                 
			<!--/div-->
            </div>
			 <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
			<!-- form-start-->
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="frm" >
					
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Title<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" required="required" name="title" id="title" >
                            <option value="">Select</option>
							<option value="pastor">Pastor</option>
							<option value="prophet">Prophet</option>
							<option value="rev">Rev.</option>
							<option value="rev dr">Rev. Dr.</option>
							<option value="rev prof">Rev. Prof.</option>
                            <option value=" apostle">Apostle</option>
                            <option value="aps dr">Aps. Dr.</option>
                            <option value="aps prof">Aps. Prof.</option>
							 <!--option value="other">Other</option-->
                           </select>
						   <span class="fa fa-star form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  
					 				  
					<div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="first_name" >First Name <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="first_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="first_name" placeholder="15 letters max"  type="text"  required maxlength="15"  />
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="middle_names">Middle Name 
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="middle_names" class="form-control has-feedback-left col-md-7 col-xs-12" name="middle_names" placeholder="30 letters max" type="text" maxlength="30"/>
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last_name">Last Name<span style="color:red;">*</span> 
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="last_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="last_name" placeholder="15 letters max" type="text" required="required" maxlength="15"/>
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					 <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12"for="picture">Picture<span style="color:red;">*</span>
                        </label>
                      	<div class="col-md-4 col-sm-4 col-xs-12">
                         <input type="file" class="form-control has-feedback-left" id="picture" accept="image/*" name="picture" required="required"/>
						  <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
						  <span class="fa fa-camera form-control-feedback left" aria-hidden="true" required></span>
                      </div>
					</div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dob">Date of Birth<span style="color:red;">*</span>
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="dob" class="form-control has-feedback-left col-md-7 col-xs-12" name="dob" placeholder="DD/MM/YYYY" type="date" />
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile Number I<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile" 
						  placeholder="Use Format 233XXXXXXXXX" type="tel" required="required" />						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Number II</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile2" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile2" 
						  placeholder="Use Format: 233xxxxxxxxx" type="tel" />							 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					 <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">WhatsApp Number</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="whatsapp" class="form-control has-feedback-left col-md-7 col-xs-12" name="whatsapp" 
						 placeholder="Use Format: 233xxxxxxxxx" type="tel" />							 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address 
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="email" class="form-control has-feedback-left col-md-7 col-xs-12" name="email" placeholder="30 letters max" type="email"  maxlength="30"/>
						   <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="postal_add">Postal Address</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="postal_add"> 60 letters max</label>
                          <textarea  id="postal_add" name="postal_add" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="60" style="resize:none"></textarea>
                        </div> 
                      </div>
					 
					  <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ghanaian">Ghanaian?<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="ghanaian" name="ghanaian" >
						    <option value="">Select</option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
                          </select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nationality">Nationality<span class="required"><span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="nationality" class="form-control has-feedback-left col-md-7 col-xs-12" name="nationality" placeholder="" type="text" disabled="dissabled" />
						  <span class="fa fa-flag form-control-feedback left" aria-hidden="true" required></span>                        
                        </div>
                      </div>
					  
					  <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Region<span style="color:red;">*</span></label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" name="home_region" id="home_region" >
                          <option value="">Select</option>
                            <option value="Ahofo">Ahafo Region</option>
                            <option value="Ashanti">Ashanti Region</option>                             
                            <option value="Bono East">Bono East Region</option>
                            <option value="Bono">Bono Region</option>
                            <option value="Central">Central Region</option>
                            <option value="Eastern">Eastern Region</option>
							              <option value="Greater Accra">Greater Accra Region</option>                            
                            <option value="Northern East">Northern East Region</option>
                            <option value="Northern">Northern Region</option>                            
                            <option value="Oti">Oti Region</option>
                            <option value="Savannah">Savannah Region</option>
							              <option value="Upper East">Upper East Region</option>
                            <option value="upper_west">Upper West Region</option>
						              	<option value="Volta">Volta Region</option>                           
                            <option value="Western North">Western North Region</option>
                            <option value="Western">Western Region</option>                               
                          </select>
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="home_town">Home Town <span class="required"><span style="color:red;">*</span></span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="home_town" class="form-control has-feedback-left col-md-7 col-xs-12" name="home_town" placeholder="30 letters max" type="text" maxlength="30" />
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>                        
                        </div>
                      </div> 
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="permanent_add">Permanent Address</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="permanent_add"> 60 letters max</label>
                          <textarea  id="permanent_add" name="permanent_add" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="60" style="resize:none"></textarea>
                        </div> 
                      </div>					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mother_tongue">Mother tongue <span class="required"><span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mother_tongue" class="form-control has-feedback-left col-md-7 col-xs-12" name="mother_tongue" placeholder="15 letters max" type="text" maxlength="15" />
						  <span class="fa fa-comment form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sec_lang">Secondary Languages</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="sec_lang">90 letters max</label>
                          <textarea  id="sec_lang" name="sec_lang" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="90" style="resize:none">
						  </textarea>
                        </div> 
                      </div>
					  
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qualification">Highest Educational Level<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="qualification" name="qualification" >
						    <option value="">Select</option>
							<option value="na">Not Applicable</option>
							<option value="Primary">Primary</option>
							<option value="Junior High">Junior High</option>
							<option value="Senior High">Senior High</option>
							<option value="O Level">O Level</option>
							<option value="A Level">A Level</option>
							<option value="Diploma">Diploma</option>
							<option value="HND">Higher National Diploma</option>
							<option value="Bachelors">Bachelor's Degree</option>
							<option value="Masters">Master's Degree</option>
							<option value="Doctoral">Doctoral Degree</option>
							
                          </select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div> 
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="oqualifications">Other Qualifications & Year of Award</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="oqualifications"> </label>
                          <textarea  id="oqualifications" name="oqualifications" class="form-control has-feedback-left col-md-7 col-xs-12"  style="resize:none"></textarea>
                        </div> 
                      </div>
                     <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="marital_status">Marital Status<span style="color:red;">*</span>
                        </span>
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="marital_status" name="marital_status" >
						    <option value="">Select</option>
							<option value="Single">Single</option>
							<option value="Married">Married</option>
							<option value="Separated">Separated</option>
							<option value="Widowed">Widowed</option>
							<option value="Divorced">Divorced</option>
                            </select>
							<span class="fa fa-magnet form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_of_marriage">Type of Marriage                       
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="type_of_marriage" name="type_of_marriage">
						    <option value="">Select</option>
							<option value="Ordinance">Ordinance</option>
							<option value="Customary">Customary</option>
							
                          </select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div> 
						
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="spouse">Spouse</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="spouse" class="form-control has-feedback-left col-md-7 col-xs-12" name="spouse" placeholder="Wife's name" type="text" maxlength="30"/>
						   <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                          </div>
                      </div>
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Spouse's Number</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="spouse_no" class="form-control has-feedback-left col-md-7 col-xs-12" name="spouse_no" 
						  placeholder="Use Format: 233xxxxxxxxx" type="tel" />							 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="children">List of Children</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="children"> 6400 characters max</label>
                          <textarea  id="children" name="children" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="6400" style="resize:none"></textarea>
                        </div> 
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="former_deno">Former Denomination</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="former_deno" class="form-control has-feedback-left col-md-7 col-xs-12" name="former_deno" placeholder="previous church" type="text" maxlength="30"/>
						   <span class="fa fa-plus form-control-feedback left" aria-hidden="true" required></span>
                          </div>
                      </div>
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_joined">Date Joined CACI
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_joined" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_joined" placeholder="When you joined the church" type="date"/>
						   <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_baptised">Date of Baptism
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_baptised" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_baptised" placeholder="MM/DD/YYYY" type="date" />
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="holyspirit">Holy Spirit Baptised?</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="holyspirit" name="holyspirit" >
						    <option value="">Select</option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
                          </select>
						  <span class="fa fa-question form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_baptised_HS">Date of Holy Spirit Baptism</label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_baptised_HS" class="form-control has-feedback-left col-md-7 col-xs-12"  disabled="dissabled" name="date_baptised_HS" placeholder="Date of Holy Spirit Baptism" type="date"/>
						   <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="appointed_date">Appointment Date<span style="color:red;">*</span></label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="appointment_date" class="form-control has-feedback-left col-md-7 col-xs-12" name="appointment_date" placeholder="MM/DD/YYYY" type="date"/>
						   <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ordination_date">Date Ordained
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="ordination_date" class="form-control has-feedback-left col-md-7 col-xs-12" name="ordination_date" placeholder="MM/DD/YYYY" type="date"/>
						   <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="current_station">Current Station<span style="color:red;">*</span></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                         <input id="current_station" class="form-control has-feedback-left col-md-7 col-xs-12" name="current_station" placeholder="30 Letters Max"
						 type="text" maxlength="30" /> 
						  <span class="fa fa-bank form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="previous_stations">Previous Stations</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
						 <label for="previous_stations"> 6400 characters max</label>
                          <textarea  id="previous_stations" name="previous_stations" class="form-control has-feedback-left col-md-7 col-xs-12" maxlength="6400" style="resize:none"></textarea>
                        </div> 
                      </div>
					  
					  
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="grade">Grade<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="grade" name="grade">
						    <option value="">Select</option>
							<option value="Probation">Probation</option>
							<option value="Junior">Junior</option>
							<option value="Senior B">Senior B</option>
							<option value="Senior A">Senior A</option>
							<option value="Principal C">Principal D</option>
							<option value="Principal C">Principal C</option>
							<option value="Principal B">Principal B</option>
							<option value="Principal A">Principal A</option>
                          </select>
						  <span class="fa fa-star form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notch">Notch</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left" id="notch" name="notch" >
						    <option value="">Select</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
                          </select>
						  <span class="fa fa-level-up form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="emergency_name">Emergency Contact Person<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                         <input id="emergency_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="emergency_name" placeholder="30 Letters Max"
						 type="text" maxlength="30"/> 
						  <span class="fa fa-ambulance form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
					<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Emergency Contact Number<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="emergency_no" class="form-control has-feedback-left col-md-7 col-xs-12" name="emergency_no" placeholder="Use Format 233XXXXXXXXX" type="tel" />	
						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div> 
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="relationship">Relationship<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                         <input id="relationship" class="form-control has-feedback-left col-md-7 col-xs-12" name="relationship" placeholder="with contact person"
						 type="text" maxlength="30"/> 
						  <span class="fa fa-magnet form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					 
					  <br/>
				  
					  <div class="modal fade bs-example-modal-sm" id= "captcha" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Captha Verification Failed, Please try again.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-warning" data-dismiss="modal">Ok</button>
                         
                        </div>

                      </div>
                    </div>
                  </div>
				  
				    <div class="modal fade bs-example-modal-sm" id= "invalid_file" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >IMAGE UPLOAD ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Please upload a valid picture!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="invalid_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id="invalid_resume" class="btn btn-warning" data-dismiss="modal">Continue</button>
                         </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                             <p>New Minister was not saved, Please try again</p>
                            <p>If no success please contact support</p>                        
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "duplicate" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >DUPLICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Double registration is not permited!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="dup_cancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-warning">Add New Minister</button>
                        </div>

                      </div>
                    </div>
                  </div> 
				  <div class="modal fade bs-example-modal-sm" id= "offline" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >NETWORK ERROR!</h4>
                        </div>
                        <div class="modal-body">
                           	<p>Please check your internet access and try again.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                        </div>

                      </div>
                    </div>
                  </div>
				   <div class="modal fade bs-example-modal-sm" id= "check" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >CAPTHA ERROR!</h4>
                        </div>
                        <div class="modal-body">
                           	<p>Please check the captha box!.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                        </div>

                      </div>
                    </div>
                  </div>
				    <div class="modal fade bs-example-modal-sm" id= "invalid_no" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >INPUT ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>You will receive your User ID via SMS so please ensure that the mobile field is correctly filled.</p>
                            <p>E.g. if your mobile number is 0244123456, enter 244123456. </p>
                            <p>NB: You should enter it without the first zero.  </p>
                            <p>NB: Do not leave any lines unfilled: _  </p>
                            <p>Please correct it now.  </p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="invalid_mobile" class="btn btn-warning" data-dismiss="modal">Ok</button>
                         </div>

                      </div>
                    </div>
                  </div>
				    <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
						  <div class="alert alert-danger" id="bot" role="alert">
						</div>
						<!--div class="g-recaptcha" data-sitekey="6LdC1W0UAAAAAAd1JP8lobI_M3mXdR9WucbALUU6"></div><br/-->
							 <input id="register_mem" type="submit" class="btn btn-primary btn-md" style="width:100%" value="Register "/>
							<!--button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button-->
							<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_adm.php' "> Cancel</button>
						</div>
                   </div>
					  
        	
				</form>
			<!-- footer content -->
			<?php include "footer.php";?>
			<!-- /footer content -->
       
		 </div>
      </div>
    </div>
    </div>
  

 
  <!-- script links -->
	<?php include "javascripts.php";?>
  <!-- /script links -->


  <!-- select2 -->
  <!-- /select2 -->
  <!-- input tags -->
  <!-- /input tags -->
  <!-- form validation -->
  
  <script type="text/javascript">
    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#demo-form .btn').on('click', function() {
        $('#demo-form').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#demo-form').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });

    $(document).ready(function() {
      $.listen('parsley:field:validate', function() {
        validateFront();
      });
      $('#frm.btn').on('click', function() {
        $('#frm').parsley().validate();
        validateFront();
      });
      var validateFront = function() {
        if (true === $('#frm').parsley().isValid()) {
          $('.bs-callout-info').removeClass('hidden');
          $('.bs-callout-warning').addClass('hidden');
        } else {
          $('.bs-callout-info').addClass('hidden');
          $('.bs-callout-warning').removeClass('hidden');
        }
      };
    });
    try {
      hljs.initHighlightingOnLoad();
    } catch (err) {}
  </script>
  
  <!-- /form validation -->
  <!-- editor -->
  <!-- /editor -->
  
  <script>
$(document).ready(function (e) {	
	$('#bot').hide();
 $("#frm").on('submit',(function(e) {
  e.preventDefault();
 //if ($("#g-recaptcha-response").val()) {
	$.ajax({
   url: "php/save_min_registration.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
    success: function(data)
      {
    
				if(data.match(/completed/gi)){
				// alert("success");
				//$("#success").modal({backdrop: true});
				 window.location.replace("adm_min_generate_ID.php");
                }
				else if(data.match(/duplicate/gi)){
					// alert("Minister is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
				
				else if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}
				else if(data.match(/invalid_file/gi)){
					// alert("PHP error");
					$("#invalid_file").modal({backdrop: true});
				}
				 else if(data.match(/check/gi)){
							$("#check").modal({backdrop: true});
				} else if(data.match(/mobile/gi)){
							$("#invalid_no").modal({backdrop: true});
				} 
				else if(data.match(/robot/gi)){
					//$('#bot').hide();
								$('#bot').html("");
								$('#bot').html("<p><sytle= 'font-family:courier;'>Malicious Activity Detected!</p><p> Quitting Registration ...</font></p>");												
								$('#bot').show(); 
								setTimeout(function(){	window.location="index.php";},2000);
				}
   
      },
     
    error: function(e) 
      {
	
	$("#sys_error").modal({backdrop: true});
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					 location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//Invalid File
				$("#invalid_cancel").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				
				$("#dup_cancel").click(function(){
					 window.location.replace("index_adm.php");	
											
			});
/* }else  {
			$('#bot').html("");							
			$('#bot').html("<p><sytle= 'font-family:courier; color:white;'>Please respond to the captcha below by checking the box! </font></p>");
			$('#bot').show();
		}*/
 })
 )
 
});
 
 </script>
	
 <script>
  $('#ghanaian').change(function(){  
           var ghanaian = $(this).val();  
		  if (ghanaian =='no'){
			   	    $("#home_region").attr("disabled", "disabled");
				   $("#nationality").removeAttr('disabled');
		  } else{
			 $("#nationality").attr("disabled", "disabled");	
			$("#home_region").removeAttr('disabled');			 
		   }
            
      });
   $('#marital_status').change(function(){  
           var married = $(this).val();  
		  
		   
		  if (married =='Married' || married =='Divorced' || married =='Separated' || married =='Widowed'){
			  // console.log(married);
			 // alert ("single");
			   	  
				  $("#type_of_marriage").removeAttr('disabled');
				  $("#spouse").removeAttr('disabled');
				  $("#spouse_no").removeAttr('disabled');
		  } else{
			    $("#type_of_marriage").attr("disabled", "disabled");
				$("#spouse").attr("disabled", "disabled");
				  $("#spouse_no").attr("disabled", "disabled");
				
		   }
            
      });  
	  
	  $('#holyspirit').change(function(){  
           var holyspirit = $(this).val();  		  
		   
		  if (holyspirit =='yes'){
			  // console.log(married);
			 // alert ("single");			   	  
				  $("#date_baptised_HS").removeAttr('disabled');
		  } else{
			    $("#date_baptised_HS").attr("disabled", "disabled");				
		   }
       }); 
	  
  
 </script>
	
	 <!-- jquery.inputmask -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
    </script>
    <!-- /jquery.inputmask -->  
</body>

</html>
