    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu"  >
              <div class="menu_section" >
                <br/><br/><br/><br/>
                <ul class="nav side-menu" >
                 
				 <li><a class="ajax-link" href="index_exe.php"><i class="fa fa-tachometer"></i><span> Dashboard</span></a>
                 </li>
				 
				 <li><a><i class="fa fa-sitemap"></i> Church Statistics<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       
						<!--li><a>National<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						  <li><a href="exe_view_nat_summaries.php">National Summaries</a>
                            </li>							
                          </ul>
                        </li-->
                        <li><a>Territory<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            
                            <li><a href="exe_view_ter_summaries.php">View Territory Summaries </a>
                            </li>
                            						
                          </ul>
                        </li>
						 <li><a>Areas<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="exe_view_are_summaries.php">View Areas Summaries</a>							
                            </li>
						  </ul>
                        </li>
						 <li><a>Circuits<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="exe_view_cir_summaries.php">View Circuits Summaries</a>
                            </li>
                          </ul>
                        </li>
												
						 <li><a>Auxilliary Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						  <li><a href="#exe_view_aux_groups.php">View Aux Groups</a></li>
						</ul>
                        </li>
					</ul>
                  </li>
				  				  
                  <li><a><i class="fa fa-user"></i>Ministers<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						   				<li><a href="exe_view_ministers.php">View Ministers</a></li>
							  </ul>
                  </li>
				   <li><a><i class="fa fa-users"></i>Members<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						      <li><a href="exe_view_members.php">View Members</a></li>
							</ul>
                  </li>
				  
				  <li><a><i class="fa fa-book"></i>Sermons<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="exe_add_sermons.php">Add New Sermon</a></li>
                     <li><a href="exe_view_sermons.php">View Sermons</a></li>
					  <!--li><a href="#delete_sermons.php">Delete My Sermons</a></li-->
                    </ul>
                  </li>
				   <li><a><i class="fa fa-folder-open"></i>Drop Box<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					 <li><a href="#exe_add_documents.php">Upload Documents</a>
                            </li>
							 <li><a href="#exe_view_documents.php">View Documents</a>
                            </li>
							 <!--li><a href="#exe_view_report_summaries.php">Summaries</a></li-->
                    </ul>
                  </li>
				 <li><a><i class="fa fa-money"></i> Contributions<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					
						 <!--li><a>Income: Individuals<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#exe_view_cong_credit_transactions.php">Reports</a>							
							 </li>
						</ul>
                        </li-->
						<!--li><a>Income: Congregational<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                             	 <li><a href="#exe_view_cong_credit_transactions.php">Reports</a>							
							 </li>
						</ul>
                        </li-->
						 <!--li><a>Expenses<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="#exe_add_debit_transactions.php">New Transactions</a>
                            </li>
							 <li><a href="#exe_edit_debit_transactions.php">Edit Transactions</a>
                            </li>
                           
                          </ul>
                        </li-->
						 <li><a>Reports<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="#exe_fin_reports.php">Reports</a></li>
						</ul>
                        </li>
												
                    </ul>
                  </li>
				  <li><a><i class="fa fa-users"></i>Visitors<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <!--li><a href="exe_add_visitors.php">Add Visitors</a></li-->
						<li><a href="#exe_view_visitors.php">View Visitors</a></li>
					   <!--li><a href="#exe_view_visitors.php">View Visitors</a></li-->
                    </ul>
                   </li>
				   	   <li><a><i class="fa fa-flag"></i> Requests<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      
						<li><a>Report on Requests<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
							  <li><a href="#exe_process_burial_requests.php">View Report</a></li>							  
                          </ul>
                        </li>
                       
                    </ul>
                  </li> 
				 
				   
				   <li><a><i class="fa fa-bell"></i>Meetings<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					<li><a href="#exe_add_meetings.php">Add New Schedule</a></li>
					<li><a href="#exe_edit_meetings.php">Edit Schedules</a></li>
					 <li><a href="#exe_view_scheduled_meetings.php">View Scheduled Meetings</a></li>
					 <li><a href="#exe_view_archived_meetings.php">View Archived Meetings</a></li>
                    </ul>
                  </li>
				  
				  <li><a><i class="fa fa-list-alt"></i>Phone Book<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Contact Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						    <li><a href="#exe_add_contact_groups.php">Create New Groups</a></li>
							<li><a href="#exe_edit_contact_groups.php">View/Edit Contact Groups</a></li>
                          </ul>
                        </li>
                        <li><a>Contacts<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="#exe_add_contacts.php">Add Contacts to Groups</a>
                            </li> 
							<li><a href="#exe_select_contact_groups.php">View/Edit Contacts</a>
                            </li>
                          </ul>
                        </li>
                    </ul>
                  </li>  

				  <li><a><i class="fa fa-bullhorn"></i>Announcements<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="#exe_send_announcements.php">Send Announcements</a>
                            </li>
							 <li><a href="#exe_view_announcements.php">View Announcements</a>
                            </li>
                         </ul>
                    </li>
					
									  
				  <li><a><i class="fa fa-certificate"></i> Official Status<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Ministerial Status<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">						
						  <li><a href="#exe_add_min_statuses.php">Add New Status</a></li>
							 <li><a href="#exe_edit_min_status.php">Edit Status</a></li>
							<li><a href="#exe_view_min_statuses.php">View Status</a></li>
                          </ul>
                        </li>
                        <li><a>Lay Status<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            	<li><a href="#exe_view_lay_statuses.php">View Status</a></li>
                          </ul>
                        </li>
                    </ul>
                  </li> 
				  
					<li><a><i class="fa fa-briefcase"></i> Appointments<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Ministerial Appointments<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						
						   	 <li><a href="#exe_view_min_appointments.php">View Appointments</a>
                            </li>
						</ul>
                        </li>
						
                        <li><a>Lay Appointments<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   	 <li><a href="exe_view_appointments.php">View Appointments</a>
                            </li>
						 
                          </ul>
                        </li>
                    </ul>
                  </li>
                
				  </ul>
              </div>
            </div>