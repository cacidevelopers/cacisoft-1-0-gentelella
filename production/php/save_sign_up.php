<?php @session_start();

include 'db_con.php'; 
include 'validate.php'; 
include 'generate_random_number.php'; 
include 'eagle_eye.php';
include 'send_mail.php'; 

$captcha='';
$secret='6Le_eCgUAAAAADhnK-7cAC2QUzFwP8iFGp-dbWsW';
/*if(isset($_POST['g-recaptcha-response'])){
            $captcha=$_POST['g-recaptcha-response'];
        }
		if(!$captcha){
            echo 'check';
            exit;
        }
		 $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
        if($response.success==false)
        {	
			//echo $response;
            echo 'robot';
			exit;
        }else
        {*/		
	
	 $last_name = $first_name = $contact_no = $email = $address =$company =$position=$picture_id="";
	  $counter=0;
	 
		  //retriving data
		//'';//
		 
          $last_name =  ucWords(strtolower(empty($_POST['last_name']) ? '' : validate($_POST['last_name'])));
		  $first_name =  ucWords(strtolower(empty($_POST['first_name']) ? '' : validate($_POST['first_name'])));
		  $address = ucWords(strtolower(empty($_POST['address']) ? '' : validate($_POST['address'])));
		  $email = strtolower(empty($_POST['email']) ? '' : validate($_POST['email']));
		  $company =addslashes(ucWords(strtolower(empty($_POST['company']) ? '' : validate($_POST['company']))));
		  $position = ucWords(strtolower(empty($_POST['position']) ? '' : validate($_POST['position'])));
		  $date=date('d M Y');	
		  $reg_date = date("Y-m-d", strtotime($date));
		  $reg_time=date("H:i:s"); 
		  $auth_level='client';
		  $pass=generateRandomNumber(6);
		  $id= generateRandomNumber(6);
		  $contact_no = empty($_POST['contact_no']) ? '' : validate($_POST['contact_no']);
		  $counter=substr_count($contact_no,'_');
		  if($counter>0){
			   echo "contact_no";
			  exit;
		  }
		  
	$password= password_hash($pass, PASSWORD_DEFAULT);	
		//pic upload start		  
$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
$path = '../pics/'; // upload directory


if(isset($_FILES['picture']))
{
 $img = $_FILES['picture']['name'];
 $tmp = $_FILES['picture']['tmp_name'];      

 // get uploaded file's extension
// $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
 $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
 
 // can upload same picture using rand function
 $picture_id = rand(1000,1000000)."-".date('Ymdhms').'.'.$ext;
 if(in_array($ext, $valid_extensions)) 
 {     
  $path = $path.strtolower($picture_id); 
    $picture_id = strtolower($picture_id);
  if(move_uploaded_file($tmp,$path)) 
  {
   echo "<$img src='$path' />";
   $picture_loaded=true;
  }
 } 
 else 
 {
  echo 'invalid_file';
 }
}
//upload end
		
		//Saving Client details
		$sql = "SELECT COUNT(*) FROM accounts WHERE email = '$email' OR contact_no = '$contact_no'";
			$res = $pdo->query($sql);

			/* Check the number of rows that match the SELECT statement */
	
	  if ($res->fetchColumn() > 0) {
					
					  echo "duplicate";
					  exit;
          } 
		  
	
			
		 // sign up if account not found
		  $sql = "INSERT INTO `accounts`(`user_id`, `auth_level`, `first_name`, `last_name`, `contact_no`, `email`, `company`, `address`, `position`, `date`, `time`, `password`, `picture_id`) VALUES	(?,?,?,?,?,?,?,?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([$id, $auth_level,$first_name,$last_name,$contact_no,$email,$company,$address,$position,$reg_date,$reg_time,$password, $picture_id]);
									
			/* log successful sign up */
	if( $stmt->rowCount()>0)  {
		
						//audit logs 
						 $sql = "INSERT INTO `audit_logs`( `date`, `time`, `user_id`, `contact_no`, `auth_level`, `action`) VALUES	(?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([$reg_date,$reg_time,$id,$contact_no,$auth_level, 'Signed Up']);
										
						//email clients' login details
						
						//user_id login details to new user
					sendMail($email,$first_name,$user_id,$password,$subject,$body,$altbody);					
										
					// notice of registration
					eagle_wings($name,$ee_body);
								
					 echo "completed";
										exit();		 
						} 
						else{
			  echo "error";
			  exit();
		  }
		
	$pdo=null;	
?>	