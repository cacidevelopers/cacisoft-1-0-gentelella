<?php @session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}

$sender=$_SESSION["user_id"];
//$level=3;

// DataTables PHP library

include( "../js/editor/php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST

Editor::inst( $db, 'jobs','job_no' )
	->fields(
		Field::inst( 'veh_no' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'make' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'clean_type' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'exp_date' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'exp_time' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'location' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'special_req' )->validator( 'Validate::notEmpty'),
		Field::inst( 'department' )->validator( 'Validate::notEmpty' )
		
		)
		
	
	->where( $key = "sender", $value = $sender, $op = "=" )
	->where( $key = "status", $value = "queued", $op = "=" )
	//->where($key = "soc_code", $value = " ", $op = "=" )
	
	->process( $_POST )
	->json();


