<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'PHPMailer/vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
	// $mail->Host = 'smtp1.example.com;smtp2.example.com';  
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'omniit.dev@gmail.com';                 // SMTP username
    $mail->Password = 'checkP01nt';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to
	
	//smtp options
	$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
		)
	);
    //Recipients
    $mail->setFrom('omniit.dev@gmail.com', 'SmartCleanze');
    $mail->addAddress('kwaku.mike@gmail.com', 'Mike');     // Add a recipient
  //  $mail->addAddress('ellen@example.com');               // Name is optional
    $mail->addReplyTo('info@omni-it-consulting.com', 'Information');
    $mail->addCC('madjei@omni-it-consulting.com');
   // $mail->addBCC('bcc@example.com');

    //Attachments
  //  $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
  //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'SmartCleanze Login';
    $mail->Body    = 'This is the HTML message body: Your password is: <b> "Password" in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients: Your password is "pass"';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}