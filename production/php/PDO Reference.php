<?php

/*Getting only one datum, e.g. getting the last registered user
		$stmt = $pdo->query("SELECT * FROM users ORDER BY id DESC LIMIT 1");
		$user = $stmt->fetch();
	*/
	
	
	/*Getting multiple data . THIS METHOD IS USED TO WORK ON EACH DATUM AS IT IS RETURNED. E.G:
		
		$stmt = $pdo->query("SELECT * FROM users");
		while ($row = $stmt->fetch()) {
		echo $row['name']."<br />\n";
		}
	*/
	
	/*Getting multiple data . THIS METHOD IS USED TO SAVE MULTIPLE DATA INTO AN ARRAY TO BE OPEREATED ON LATER. E.G:
	$data = $pdo->query("SELECT * FROM users")->fetchAll();
		// somewhere later:
		foreach ($data as $row) {
			echo $row['name']."<br />\n";
	}

*/
?>