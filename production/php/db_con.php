<?php

define('DB_USER', "root"); // db user
define('DB_PASSWORD', ""); // db password
define('DB_DATABASE', "cacisoft"); // database name
define('DB_SERVER', "localhost"); // db server
class Database extends pdo {

    private $dbtype;
    private $host;
    private $user;
    private $pass;
    private $database;

    public function __construct(){
        $this->dbtype = 'mysql';
        $this->host = 'localhost';
        $this->user = 'root';
        $this->pass = '';
        $this->database = 'cacisoft';
        $dns = $this->dbtype.':dbname='.$this->database.";host=".$this->host;
        parent::__construct( $dns, $this->user, $this->pass );
    }
}

$database = new Database();
$pdo =& $database;
date_default_timezone_set('utc');

?>


