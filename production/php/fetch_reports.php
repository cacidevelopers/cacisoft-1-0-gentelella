<?php @session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}
//include 'db_con.php';
include 'validate.php';


$bc_codes=ARRAY();
$bc_names=ARRAY();
$bc_sum=ARRAY();
$bc_summaries=ARRAY();
$from_date='';
$to_date='';


$sort = empty($_POST['sort']) ? '' : validate($_POST['sort']);
$from_date = empty($_POST['from_date']) ? '' : validate($_POST['from_date']); //echo $from.'<br />';
  
$to_date = empty($_POST['to_date']) ? '' : validate($_POST['to_date']);//echo $to.'<br />';
//$to = str_replace('/','-',$to);
//$to = date("Y-m-d", strtotime($to));
/*	 if ($from<>''){
	
		$from_date = $from;
	}
	else {
		$from_date = '1900-01-01';
	}

	 if ($to<>''){
		$to_date=$to;
	}else{
		$to_date= '3030-12-31';
	}
*/

 if(isset($_POST["sort"]))  
 {  
      if($_POST["sort"] != '') { 
		   if($sort == 'all')  {
			all_vehicles_cleaned($from_date, $to_date);
		   }elseif($sort == 'per_valeter')  {
			vehicles_with_multiple_cleans($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   } elseif($sort == 'assigned')  {
			vehicles_cleaned_per_operator($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   } elseif($sort == 'progress')  {
			vehicles_cleaned($from_date, $to_date);
		   } elseif($sort == 'completed')  {
			cleaning_cost_per_operator($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   }  elseif($sort == 'validated')  {
			cleaning_cost_per_operator($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   }  elseif($sort == 'invoiced')  {
			cleaning_cost_per_operator($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   }  elseif($sort == 'closed')  {
			cleaning_cost_per_operator($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   } 
	}
 }

function 	all_vehicles_cleaned($from_date, $to_date){
 include 'db_con.php';
$json = array();
//$sql = $pdo->query("SELECT * FROM jobs ")->fetchAll();
$sql = $pdo->prepare("SELECT * FROM jobs ");
		$sql->execute();
		$data=$sql->fetchAll();
		
foreach ($data as $row) {
	$invoiced_by=$row['invoiced_by'];
	$sender=$row['sender'];
	$valeter=$row['valeter'];
	$val_by=$row['val_by'];
	
					
				/*$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM clients WHERE user_id = ? ");
				$stmt->execute([$sender]);
				$sender=$stmt->fetchColumn();*/
				
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$valeter]);
				$valeter=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$invoiced_by]);
				$invoiced_by=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$val_by]);
				$val_by=$stmt->fetchColumn();

	
   		
		echo ' <table>
                      <thead>
									<tr >
										<th >Docket No</th>										
										<th >Date Posted</th>
										<th >Time</th>
										<th >Posted By</th>
										<th >Vehicle/Chasis No</th>
										<th >Make</th>
										<th >Clean Type</th>
										<th >Expected Date</th>
										<th >Expected Time</th>
										<th >Location</th>
										<th >Special Request</th>
										<th >Valeter</th>
										<th >Started Date</th>
										<th >Started Time</th>
										<th >Completed Date</th>
										<th >Completed Time</th>
										<th >Validated By</th>
										<th >Validation Date</th>
										<th >Validation Time</th>
										<th >Invoiced By</th>
										<th >Invoice Date</th>
										<th >Invoice Time</th>
									</tr>
						</thead >
						<body>';

								echo '<tr>';
									echo '<td>'.$row['job_no'].'</td>';
									echo '<td>'.$row['date'].'</td>';									
									echo '<td>'.$row['time'].'</td>';							
									echo '<td>'.$sender.'</td>';										
									echo '<td>'.$row['veh_no'].'</td>';									
									echo '<td>'.$row['make'].'</td>';									
									echo '<td>'.$row['clean_type'].'</td>';									
									echo '<td>'.$row['exp_date'].'</td>';									
									echo '<td>'.$row['exp_time'].'</td>';									
									echo '<td>'.$row['location'].'</td>';									
									echo '<td>'.$row['special_req'].'</td>';							
									echo '<td>'.$valeter.'</td>';									
									echo '<td>'.$row['start_date'].'</td>';									
									echo '<td>'.$row['start_time'].'</td>';		
									echo '<td>'.$row['comp_date'].'</td>';									
									echo '<td>'.$row['comp_time'].'</td>';								
									echo '<td>'.$val_by.'</td>';								
									echo '<td>'.$row['val_date'].'</td>';									
									echo '<td>'.$row['val_time'].'</td>';								
									echo '<td>'.$invoiced_by.'</td>';
									echo '<td>'.$row['invoice_date'].'</td>';											
									echo '<td>'.$row['invoice_time'].'</td>';											
								echo '</tr>';
							echo '			
						</tbody> 
                      </table>
                    ';		
 }
}


function 	all_vehicles_cleaned1($from_date, $to_date){
 include 'db_con.php';
$json = array();

$sql = $pdo->prepare("SELECT * FROM jobs WHERE  val_date>='?' AND val_date<=  '?'");
		$sql->execute([$from_date,$to_date]);
		$data=$sql->fetchAll();
		
foreach ($data as $row) {
	
	$sender=$row['sender'];
	$valeter=$row['valeter'];

					
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM clients WHERE user_id = ? ");
				$stmt->execute([$sender]);
				$sender=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$valeter]);
				$valeter=$stmt->fetchColumn();
				
	
    $bus = array(
		
		'Job No' => $row['job_no'],
		'Sent Date' => $row['date'],
        'Sent Time' => $row['time'],
		'Posted By' => $sender,
		'Vehicle/Chasis No' => $row['veh_no'],
		'Make' => $row['make'],
		'Clean Type' => $row['clean_type'],
		'Expected Date' => $row['exp_date'],
		'Expected Time' => $row['exp_time'],
		'Location' => $row['location'],
		'Special Request' => $row['special_req'],
		'Valeter' => $valeter
    );
    array_push($json, $bus);
}

$jsonstring = json_encode($json);
echo $jsonstring;

die();
				
		echo ' <table>
                      <thead>
									<tr >
										<th >Docket No</th>										
										<th >Date Posted</th>
										<th >Time</th>
										<th >Sender</th>
										<th >Vehicle/Chasis No</th>
										<th >Make</th>
										<th >Clean Type</th>
										<th >Expected Date</th>
										<th >Expected Time</th>
										<th >Location</th>
										<th >Special Request</th>
										<th >Assigned By</th>
										<th >Valeter</th>
									</tr>
						</thead >
						<body>';

								echo '<tr>';
									echo '<td>'.number_format((float)$tot_credit, 2, '.', ',').'</td>';
									echo '<td>'.number_format((float)$tot_debit, 2, '.', ',').'</td>';
									echo'<td>'.number_format((float)$acc_bal, 2, '.', ',').'</td>';
								echo '</tr>';
							echo '			
						</tbody> 
                      </table>
                    ';		
 }
 
 
 function vehicles_with_multiple_cleans($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code){
	 $con=$con;
	 $cent=100;
	 $grand_tot=0;
	 
	
	
	 $tithe_sum=0.00;
	 $offering_sum=0.00;
	 $kofi_ama_sum=0.00;
	 $other_ind_sum=0.00;
	 $other_cong_sum=0.00;
	 $others_sum=0.00;
	 $thanks_sum=0.00;
	 $som_sum=0.00;
	
				
	// Total contributions by Tithe
				$sql="SELECT SUM(amount) s FROM credits_ind WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code' AND 
				type_code='tit' AND date BETWEEN '$from_date' AND '$to_date'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0.00;
					}
				$tithe_sum = number_format((float)$res["s"], 2, '.', '');
				
	// Total contributions by other (Individual)
				$sql="SELECT SUM(amount) s FROM credits_ind WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code' AND 
				type_code='oth' AND date BETWEEN '$from_date' AND '$to_date'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0.00;
					}
				$other_ind_sum = number_format((float)$res["s"], 2, '.', '');
				
	// Total contributions by other (Congregational)
				$sql="SELECT SUM(amount) s FROM credits_cong WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code' AND 
				type_code='oth' AND date BETWEEN '$from_date' AND '$to_date'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0.00;
					}
				$other_cong_sum = number_format((float)$res["s"], 2, '.', '');
				$others_sum=number_format((float)$other_ind_sum+$other_cong_sum, 2, '.', '');
				
				
	// Total contributions by Kofi & Ama
				
				$sql="SELECT SUM(amount) s FROM credits_cong WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code' AND 
				type_code='kof' AND date BETWEEN '$from_date' AND '$to_date'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0.00;
					}
				$kofi_ama_sum = number_format((float)$res["s"], 2, '.', '');
				
	// Total contributions by offering
				
				$sql="SELECT SUM(amount) s FROM credits_cong WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code' AND 
				type_code='off' AND date BETWEEN '$from_date' AND '$to_date'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0.00;
					}
				$offering_sum = number_format((float)$res["s"], 2, '.', '');
				
	// Total contributions by thanks offering
				
				$sql="SELECT SUM(amount) s FROM credits_cong WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code' AND 
				type_code='tha' AND date BETWEEN '$from_date' AND '$to_date'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0.00;
					}
				$thanks_sum = number_format((float)$res["s"], 2, '.', '');
				
	// Total contributions by so mu bi
				
				$sql="SELECT SUM(amount) s FROM credits_cong WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code' AND 
				type_code='som' AND date BETWEEN '$from_date' AND '$to_date'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0.00;
					}
				$som_sum = number_format((float)$res["s"], 2, '.', '');
				
	//percentages
	
	 $total_income=$tithe_sum+$offering_sum+$kofi_ama_sum+$others_sum+$thanks_sum+$som_sum;
	 $total_income=number_format((float)$total_income, 2, '.', '');
	 
	 if($total_income==0){
		 $tithe_p=0; 
		 $offering_p=0;
		 $kofi_ama_p=0;
		 $others_p=0;
		 $thanks_p=0;
		 $som_p=0;
	 }else{
		
		 $tithe_p= number_format((float)$tithe_sum/$total_income*$cent, 2, '.', '');
		 $offering_p= number_format((float)$offering_sum/$total_income*$cent, 2, '.', '');
		 $kofi_ama_p= number_format((float)$kofi_ama_sum/$total_income*$cent, 2, '.', '');
		 $others_p= number_format((float)$others_sum/$total_income*$cent, 2, '.', '');
		 $thanks_p= number_format((float)$thanks_sum/$total_income*$cent, 2, '.', '');
		 $som_p= number_format((float)$som_sum/$total_income*$cent, 2, '.', '');
	 }			
				
		echo ' <table>
                      <thead>
									<tr >
																			
										<th >Contribution Type</th>										
										<th >Total Contributions (GHS)</th>
										<th >Percentage(%)</th>
									</tr>
						</thead >
						<body>';
						
								echo '<tr>';
											echo '<td>Tithes</td>';
											echo '<td>'.number_format((float)$tithe_sum, 2, '.', ',').'</td>';
											echo '<td>'.$tithe_p.'</td>';
									
								echo '</tr>';
								echo '<tr>';
											echo '<td>Offerings</td>';
											echo '<td>'.number_format((float)$offering_sum, 2, '.', ',').'</td>';
											echo '<td>'.$offering_p.'</td>';
									
								echo '</tr>';
								echo '<tr>';
											echo '<td>Kofi & Ama</td>';
											echo '<td>'.number_format((float)$kofi_ama_sum, 2, '.', ',').'</td>';
											echo '<td>'.$kofi_ama_p.'</td>';
									
								echo '</tr>';
								echo '<tr>';
											echo '<td>Thanks Offering</td>';
											echo '<td>'.number_format((float)$thanks_sum, 2, '.', ',').'</td>';
											echo '<td>'.$thanks_p.'</td>';
									
								echo '</tr>';
								echo '<tr>';
											echo '<td>So Mu Bi</td>';
											echo '<td>'.number_format((float)$som_sum, 2, '.', ',').'</td>';
											echo '<td>'.$som_p.'</td>';
									
								echo '</tr>';
								echo '<tr>';
											echo '<td>Others</td>';
											echo '<td>'.number_format((float)$others_sum, 2, '.', ',').'</td>';
											echo '<td>'.$others_p.'</td>';
									
								echo '</tr>';
						
							echo '		
									<tr>
										<th >GRAND TOTALS (GHS)</th>
										<th >'.number_format((float)$total_income, 2, '.', ',').'</th>
										<th >'.$cent.'</th>
									</tr>
						</tbody> 
                      </table>
                    ';		
 }
 
 function vehicles_cleaned_per_operator($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code){
	/* $con=$con;
	 $grand_tot=0.00;  
	 $sql="SELECT bc_id, bc_name FROM bible_classes WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code'";
				$result = mysqli_query ($con, $sql);
				while($res = mysqli_fetch_array($result)) {
				$bc_codes[] = $res["bc_id"];
				$bc_names[] = $res["bc_name"];
				
				
				}
				$bc_length=count($bc_codes)-1;
				
				for($x=0; $x<=$bc_length; $x++){
				$sql="SELECT SUM(amount) s FROM credits_ind WHERE bc_code='$bc_codes[$x]'AND date BETWEEN '$from_date' AND '$to_date'
				AND dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0;
				}
				
				$bc_sum[$x] = number_format((float)$res["s"], 2, '.', '');
				$grand_tot+=$res["s"];
				}*/
				
	include 'db_con.php';			
				$data = $pdo->query("SELECT COUNT(*) c FROM jobs WHERE status='validated'")->fetchAll();
		
	foreach ($data as $row) {
	$user_id=$row['assigned_by'];	
	$sender=$row['sender'];
	$valeter=$row['valeter'];
	$count=$row['c'];

	$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$user_id]);
				$assigned_by=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM clients WHERE user_id = ? ");
				$stmt->execute([$sender]);
				$rec=$stmt->fetch();
				if ($stmt->rowCount() > 0){
							$sender=$rec["name"];
				}
				
				$stmt1 = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt1->execute([$sender]);
				$rec=$stmt1->fetch();
				if ($stmt1->rowCount() > 0){
							$sender=$rec["name"];
				}
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$valeter]);
				$valeter=$stmt->fetchColumn();
				
		echo ' 
		<table>
                      <thead>
									<tr >TOTAL NUMBER OF CLEANS: 20
										<th >Bible Class Name </th>										
										<th >Total Contributions in (GHS) per Bible Class</th>
									</tr>
						</thead >
						<body>';
						
						//for($x=0; $x<=$bc_length; $x++){
								echo '<tr>';
									echo '<td>'.$row['job_no'].'</td>';
									echo '<td>'.$valeter.'</td>';
								echo '</tr>';
						}
							echo '			<th >Grand Total</th>										
										<th >'.$count.'</th>
						</tbody> 
                      </table>
                    ';		
 } 
 
 
 
 function cleaning_cost_per_salesman($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code){
	 $con=$con;
	 $grand_tot=0.00;  
	 $sql="SELECT bc_id, bc_name FROM bible_classes WHERE dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code'";
				$result = mysqli_query ($con, $sql);
				while($res = mysqli_fetch_array($result)) {
				$bc_codes[] = $res["bc_id"];
				$bc_names[] = $res["bc_name"];
				
				
				}
				$bc_length=count($bc_codes)-1;
				
				for($x=0; $x<=$bc_length; $x++){
				$sql="SELECT SUM(amount) s FROM credits_ind WHERE bc_code='$bc_codes[$x]'AND date BETWEEN '$from_date' AND '$to_date'
				AND dio_code='$dio_code' AND cir_code='$cir_code' AND soc_code='$soc_code'";
				$result = mysqli_query ($con, $sql);
				$res = mysqli_fetch_array($result) ;
				if($res['s']==""){
					$res['s']=0;
				}
				
				$bc_sum[$x] = number_format((float)$res["s"], 2, '.', '');
				$grand_tot+=$res["s"];
				}
		echo ' <table>
                      <thead>
									<tr >
										<th >Bible Class Name </th>										
										<th >Total Contributions in (GHS) per Bible Class</th>
									</tr>
						</thead >
						<body>';
						
						for($x=0; $x<=$bc_length; $x++){
								echo '<tr>';
									echo '<td>'.$bc_names[$x].'</td>';
									echo '<td>'.$bc_sum[$x].'</td>';
								echo '</tr>';
						}
							echo '			<th >Grand Total</th>										
										<th >'.number_format((float)$grand_tot, 2, '.', '').'</th>
						</tbody> 
                      </table>
                    ';		
 }
?>

