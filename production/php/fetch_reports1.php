<?php @session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}
include 'db_con.php';
include 'validate.php';
$json = array();

$bc_codes=ARRAY();
$bc_names=ARRAY();
$bc_sum=ARRAY();
$bc_summaries=ARRAY();
$from_date='';
$to_date='';
$dio_code='';
$cir_code='';
$soc_code='';


$sort = empty($_POST['sort']) ? '' : validate($_POST['sort']);
$from = empty($_POST['from_date']) ? '' : validate($_POST['from_date']);//echo $from;
$to = empty($_POST['to_date']) ? '' : validate($_POST['to_date']);

	 if ($from==''){
	
		$from_date = '1900-01-01';
	}
	else {
		$from_date=$from;
	}

	 if ($to==''){
		$to_date= '3030-12-31';
	}else{
		$to_date=$to;
	}


 if(isset($_POST["sort"]))  
 {  
      if($_POST["sort"] != '') { 
		   if($sort == 'cleaning_cost')  {
			cleaning_cost($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   }elseif($sort == 'multiple_cleans')  {
			vehicles_with_multiple_cleans($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   } elseif($sort == 'per_operator')  {
			vehicles_cleaned_per_operator($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   } elseif($sort == 'vehicles_cleaned')  {
			no_of_vehicles_cleaned();	
		   } elseif($sort == 'per_salesman')  {
			cleaning_cost_per_operator($con,$from_date,$to_date,$dio_code,$cir_code,$soc_code);	
		   } 
	}
 }
 
 
 //function no_of_vehicles_cleaned(){
 $data = $pdo->query("SELECT * FROM jobs")->fetchAll();
		
	foreach ($data as $row) {
	$user_id=$row['assigned_by'];	
	$sender=$row['sender'];
	$valeter=$row['valeter'];
	$count='';

	$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$user_id]);
				$assigned_by=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM clients WHERE user_id = ? ");
				$stmt->execute([$sender]);
				$rec=$stmt->fetch();
				if ($stmt->rowCount() > 0){
							$sender=$rec["name"];
				}
				
				$stmt1 = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt1->execute([$sender]);
				$rec=$stmt1->fetch();
				if ($stmt1->rowCount() > 0){
							$sender=$rec["name"];
				}
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$valeter]);
				$valeter=$stmt->fetchColumn();
	
    $bus = array(
		 
		 
		// 'Close' => '<a href="adm_close_validated_jobs.php?id='. $row['job_no'] . '"class="btn btn-primary btn-xs"><i class="fa fa-close"></i> Close </a>',
		'Job No' => $row['job_no'],
		'Sent Date' => $row['date'],
        'Sent Time' => $row['time'],
		'Sender' => $sender,
		//'Vehicle/Chasis No' => $row['veh_no'],
		'Make' => $row['make'],
		'Clean Type' => $row['clean_type'],
		'Expected Date' => $row['exp_date'],
		'Expected Time' => $row['exp_time'],
		'Accepted Date' => $row['acc_date'],
		'Accepted Time' => $row['acc_time'],
		'Completed Date' => $row['comp_date'],
		'Completed Time' => $row['comp_time'],
		'Location' => $row['location'],
		'Special Request' => $row['special_req'],
		'Assigned By' => $assigned_by,
		'Valeter' => $valeter,
		'Department' =>  $row['department']
    );
    array_push($json, $bus);
	
}

$jsonstring = json_encode($json);
echo $jsonstring;
//}
die();
?>

