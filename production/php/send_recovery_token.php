<?php @session_start();

		use PHPMailer\PHPMailer\PHPMailer;
		use PHPMailer\PHPMailer\Exception;
		require_once('PHPMailer/vendor/autoload.php');
		include 'db_con.php'; 
		include 'validate.php'; 
		include 'generate_random_number.php';

	
	  $token=generateRandomNumber(6);
	
	  
	  $user_email = $client_email ="";
	  
	    
	   $date=date('d M Y');	
		  $date = date("Y-m-d", strtotime($date));
		  $time=date("h:i:sa");
		 
	  
		  //retriving data
		 // $email = 'micky.adjei@gmail.com';//empty($_POST['email']) ? '' : validate($_POST['email']);
		  $email = empty($_POST['email']) ? '' : validate($_POST['email']);
		
		
		  $options = [
			'cost' => 12,
			];
		 
		$pass_hash= password_hash($token, PASSWORD_BCRYPT, $options);
		//$expire=date('i')+15;
		$expire=time()+600;
		$_SESSION['pass_hash']=$pass_hash;
		$_SESSION['email_id']=$email;
		
		
		$stmt = $pdo->prepare("SELECT * FROM users WHERE email = ? ");
				$stmt->execute([$email]);
				$user=$stmt->fetchColumn();
				
		$sql = $pdo->prepare("SELECT * FROM clients WHERE email = ? ");
				$sql->execute([$email]);
				$client=$sql->fetchColumn();
				
				if ($user > 0) {
					//$user_email=$email;
					
					 $sql = "INSERT INTO `pass_reset`(`hash`, `status`, `time`) 
					VALUES	(?,?,?)";
					$stmt= $pdo->prepare($sql);
					$stmt->execute([$pass_hash,'live',$expire]);
			
							//audit logs 
							 $sql = "INSERT INTO `audit_logs`( `date`, `time`, `user_id`, `contact_no`, `auth_level`, `action`) VALUES	(?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([$date,$time,$email,'','',' Staff Requested Password Reset ']); 
									
									//email login details to new user
									
								//Load Composer's autoloader
								//	require 'PHPMailer/vendor/autoload.php';
									$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
									try {
										//Server settings
										$mail->SMTPDebug = 2;                                 // Enable verbose debug output
										$mail->isSMTP();                                      // Set mailer to use SMTP
										// $mail->Host = 'smtp1.example.com;smtp2.example.com';  
										$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
										$mail->SMTPAuth = true;                               // Enable SMTP authentication
										 $mail->Username = 'omniit.dev@gmail.com';                 // SMTP username
										$mail->Password = 'checkP01nt';                           // SMTP password
										$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
										$mail->Port = 587;                                    // TCP port to connect to
										
										//smtp options
										$mail->SMTPOptions = array(
										'ssl' => array(
											'verify_peer' => false,
											'verify_peer_name' => false,
											'allow_self_signed' => true
											)
										);
										//Recipients
										$mail->setFrom('omniit.dev@gmail.com', 'SmartCleanze');
										$mail->addAddress($email, 'Staff');     // Add a recipient
									  //  $mail->addAddress('ellen@example.com');               // Name is optional
										//$mail->addReplyTo('info@omni-it-consulting.com', 'Information');
										//$mail->addCC('madjei@omni-it-consulting.com');
									   // $mail->addBCC('bcc@example.com');

										//Attachments
									  //  $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
									  //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

										//Content
										$mail->isHTML(true);                                  // Set email format to HTML
										$mail->Subject = 'Login Details';
										$mail->Body    = 'Please enter this Reset Token to reset your password: <b>'.$token.'</b>';
										
										$mail->AltBody =  'Please enter this Reset Token to reset your password: '.$token;

										$mail->send();
										//echo 'Message has been sent';
										 echo "success";
									} catch (Exception $e) {
										echo 'Message could not be sent. Mailer : ', $mail->ErrorInfo;
									}
							
														 
						 }
				elseif ($client > 0) {
					//$client_email=$email;
					
						
					 $sql = "INSERT INTO `pass_reset`(`hash`, `status`, `time`) 
					VALUES	(?,?,?)";
					$stmt= $pdo->prepare($sql);
					$stmt->execute([$pass_hash,'live',$expire]);
			
							//audit logs 
							 $sql = "INSERT INTO `audit_logs`( `date`, `time`, `user_id`, `contact_no`, `auth_level`, `action`) VALUES	(?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([$date,$time,$email,'','',' Client Requested Password Reset ']); 
									
									//email login details to new user
									
								//Load Composer's autoloader
								//	require 'PHPMailer/vendor/autoload.php';
									$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
									try {
										//Server settings
										$mail->SMTPDebug = 2;                                 // Enable verbose debug output
										$mail->isSMTP();                                      // Set mailer to use SMTP
										// $mail->Host = 'smtp1.example.com;smtp2.example.com';  
										$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
										$mail->SMTPAuth = true;                               // Enable SMTP authentication
										 $mail->Username = 'omniit.dev@gmail.com';                 // SMTP username
										$mail->Password = 'checkP01nt';                           // SMTP password
										$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
										$mail->Port = 587;                                    // TCP port to connect to
										
										//smtp options
										$mail->SMTPOptions = array(
										'ssl' => array(
											'verify_peer' => false,
											'verify_peer_name' => false,
											'allow_self_signed' => true
											)
										);
										//Recipients
										$mail->setFrom('omniit.dev@gmail.com', 'SmartCleanze');
										$mail->addAddress($email, 'User');     // Add a recipient
									  //  $mail->addAddress('ellen@example.com');               // Name is optional
										//$mail->addReplyTo('info@omni-it-consulting.com', 'Information');
										//$mail->addCC('madjei@omni-it-consulting.com');
									   // $mail->addBCC('bcc@example.com');

										//Attachments
									  //  $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
									  //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

										//Content
										$mail->isHTML(true);                                  // Set email format to HTML
										$mail->Subject = 'Login Details';
										$mail->Body    = 'A Reset Token has been sent to your inbox. Please enter it to reset your password';
										
										$mail->AltBody =  'A Reset Token has been sent to your inbox. Please enter it to reset your password';

										$mail->send();
										//echo 'Message has been sent';
										 echo "success";
									} catch (Exception $e) {
										echo 'Message could not be sent. Mailer : ', $mail->ErrorInfo;
									}
							
														 
						 }
		
		 
		 else{
			 
			  //audit logs 
							 $sql = "INSERT INTO `audit_logs`( `date`, `time`, `user_id`, `contact_no`, `auth_level`, `action`) VALUES	(?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([$date,$time,$email,'','','Password Recovery from home page-User ID not found']); 
			 echo 'missing';
		 } 
		
		
	 
		
?>