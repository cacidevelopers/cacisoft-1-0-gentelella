<?php 
@session_start();
include 'db_con.php';
include 'get_week_no_in_month.php';
$cent=100;

$query='';

	$all_cir=0;
	$all_are=0;
	$all_aged_males=0;
	$all_aged_females=0;	
	$all_male_adults=0;			
	$all_females_adults=0;								
	$all_y_m_adults=0;
	$all_y_f_adults=0;
	$all_m_youth=0;
	$all_f_youth=0;							
	$all_boys=0;					
	$all_girls=0;
	$ter_sum=0;
	$cac_membership=0;

	
	$tot_membership=array();
	$summaries = array();


			
	//membership		
	
		   //get no. of territories
	$stmt = $pdo->query("SELECT COUNT(*) c FROM territories");
	$row = $stmt->fetch();
	$ter_sum=$row["c"];
																
									
	for($ter_code=1; $ter_code<=$ter_sum; $ter_code++){
		$children=0;
		$m_youth=0;
		$f_youth=0;
		$ym_adult=0;
		$yf_adult=0;
		$m_adult=0;
		$f_adult=0;
		$m_aged=0;
		$f_aged=0;
		$boys=$girls=$boys1=$boys2=$girls1=$girls2=0;
		
			$x=1;
			//get territorial ministers' detials
			$query = $pdo->query("SELECT CONCAT(title,' ',first_name, ' ',last_name) AS name, min_appointments.user_id, min_appointments.status, min_appointments.from_date, min_appointments.to_date
			FROM ministers
			RIGHT JOIN min_appointments
			ON ministers.user_id=min_appointments.user_id WHERE min_appointments.status='Territorial Apostle' AND min_appointments.to_date='To Date' AND min_appointments.ter_code='$ter_code'");
			$row = $query->fetch();
			$summaries[$ter_code][$x+1]=$row["name"];
			
							
			$query = $pdo->query("SELECT CONCAT(title,' ',first_name, ' ',last_name) AS name, min_appointments.user_id, min_appointments.status, min_appointments.from_date, min_appointments.to_date
			FROM ministers
			RIGHT JOIN min_appointments
			ON ministers.user_id=min_appointments.user_id WHERE min_appointments.status='Territorial Secretary' AND min_appointments.to_date='To Date' AND min_appointments.ter_code='$ter_code'");
			$row = $query->fetch();
			$summaries[$ter_code][$x+2]=$row["name"];
			
		
			$query = $pdo->query("SELECT CONCAT(title,' ',first_name, ' ',last_name) AS name, min_appointments.user_id, min_appointments.status, min_appointments.from_date, min_appointments.to_date
			FROM ministers
			RIGHT JOIN min_appointments
			ON ministers.user_id=min_appointments.user_id WHERE min_appointments.status='Territorial Financial Secretary' AND min_appointments.to_date='To Date' AND min_appointments.ter_code='$ter_code'");
			$row = $query->fetch();
			$summaries[$ter_code][$x+3]=$row["name"];
			
			//areas
			$stmt = $pdo->query("SELECT COUNT(*) c FROM areas WHERE ter_code=$ter_code");
			$row = $stmt->fetch();			
			$summaries[$ter_code][$x+4]=$row["c"];
			$all_are+=$row["c"];
			
			//circuits
			$stmt = $pdo->query("SELECT COUNT(*) c FROM circuits WHERE ter_code=$ter_code");
			$row = $stmt->fetch();	
			$summaries[$ter_code][$x+5]=$row["c"];
			$all_cir+=$row["c"];
						
					
		$query = $pdo->query("SELECT dob, gender FROM members WHERE ter_code ='$ter_code'");
			while($res = $query->fetch())
			{
					if((date('Y') - date('Y',strtotime($res["dob"])))<=17 && $res["gender"]=='male' ){
				$boys+=1;
				}elseif((date('Y') - date('Y',strtotime($res["dob"])))<=17 && $res["gender"]=='female' ){
				$girls+=1;
				}if((date('Y') - date('Y',strtotime($res["dob"])))>17 && (date('Y') - date('Y',strtotime($res["dob"])))<=35 && $res["gender"]=='male' ){
					$m_youth+=1;
				}elseif((date('Y') - date('Y',strtotime($res["dob"])))>17 && (date('Y') - date('Y',strtotime($res["dob"])))<=35 && $res["gender"]=='female' ){
					$f_youth+=1;
				}elseif((date('Y') - date('Y',strtotime($res["dob"])))>35 && (date('Y') - date('Y',strtotime($res["dob"])))<=49 && $res["gender"]=='male' ){
					$ym_adult+=1;
				}elseif((date('Y') - date('Y',strtotime($res["dob"])))>35 && (date('Y') - date('Y',strtotime($res["dob"])))<=49 && $res["gender"]=='female'){
					$yf_adult+=1;
				}elseif((date('Y') - date('Y',strtotime($res["dob"])))>49 && (date('Y') - date('Y',strtotime($res["dob"])))<=60 && $res["gender"]=='male' ){
					$m_adult +=1;
				}elseif((date('Y') - date('Y',strtotime($res["dob"])))>49 && (date('Y') - date('Y',strtotime($res["dob"])))<=60 && $res["gender"]=='female'){
					$f_adult+=1;
				}elseif((date('Y') - date('Y',strtotime($res["dob"])))>60 && $res["gender"]=='male'){
					$m_aged+=1;
					
				}elseif((date('Y') - date('Y',strtotime($res["dob"])))>60 && $res["gender"]=='female' ){
					$f_aged+=1;
				}
			}
		
			$summaries[$ter_code][$x+6]=$m_aged;
			$all_aged_males+=$m_aged;
			$summaries[$ter_code][$x+7]=$f_aged;
			$all_aged_females+=$f_aged;
			$summaries[$ter_code][$x+8]=$m_adult;
			$all_male_adults+=$m_adult;
			$summaries[$ter_code][$x+9]=$f_adult;
			$all_females_adults+=$f_adult;
			$summaries[$ter_code][$x+10]=$ym_adult;
			$all_y_m_adults+=$ym_adult;
			$summaries[$ter_code][$x+11]=$yf_adult;
			$all_y_f_adults+=$yf_adult;
			$summaries[$ter_code][$x+12]=$m_youth;
			$all_m_youth+=$m_youth;
			$summaries[$ter_code][$x+13]=$f_youth;
			$all_f_youth+=$f_youth;
			$summaries[$ter_code][$x+14]=$boys;
			$all_boys+=$boys;
			$summaries[$ter_code][$x+15]=$girls;
			$all_girls+=$girls;
			$summaries[$ter_code][$x+16]=$boys+$girls+$m_youth+$f_youth+$ym_adult+$yf_adult+$m_adult+$f_adult+$m_aged+$f_aged;
			
			$cac_membership+=$summaries[$ter_code][$x+16];
			
			//membership graphical data
			$aged= $all_aged_males+$all_aged_females;
			$adults= $all_male_adults+$all_females_adults;
			$yadults= $all_y_m_adults+$all_y_f_adults;
			$youth= $all_m_youth+$all_f_youth;
			$children= $all_boys+$all_girls;
			
			 if($cac_membership==0){
			$aged_p=0;
			$adults_p=0;
			$yadults_p=0;
			$youth_p=0;
			$children_p=0;
	
			}else{
			$aged_p=$aged/$cac_membership*$cent;
			$adults_p=$adults/$cac_membership*$cent;
			$yadults_p=$yadults/$cac_membership*$cent;
			$youth_p=$youth/$cac_membership*$cent;
			$children_p=$children/$cac_membership*$cent;
			}
			
			
			
		}

//financials
// income by sources
// Total contributions by Tithe
	
	
	$roll='sunday';	
	$week=getWeeks(date("Y-m-d"), $roll);
	
	//$date='';
	$year= (new DateTime(date("Y-m-d")))->format('Y');
	$month= (new DateTime(date("Y-m-d")))->format('m');
	$mon= (new DateTime(date("Y-m-d")))->format('F');
	
	//$week=$week-1;
	
	
	$tithe_sum=$tithe_sum1=$tithe_sum2=0;
				$sql = $pdo->query("SELECT SUM(amount) s FROM credits_ind WHERE type_code='tit' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'");
				while($res = $sql->fetch())
				{
					if($res['s']==""){
						$res['s']=0.00;
						}
					$tithe_sum1 = $res["s"];
				}

				
				$sql = $pdo->query("SELECT SUM(amount) s FROM credits_fam_groups WHERE type_code='tit' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'");
				while($res = $sql->fetch())
				{
					if($res['s']==""){
						$res['s']=0.00;
					}
					$tithe_sum2 = $res["s"];
				}		

				$tithe_sum=$tithe_sum1+$tithe_sum2;
				
				
	// Total contributions by Kofi & Ama
				
			/*	$sql="SELECT SUM(amount) s FROM credits_ind WHERE type_code='kof' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'";
				$result = mysqli_query ($con, $sql);
				while($res = mysqli_fetch_array($result)){
				if($res['s']==""){
					$res['s']=0.00;
					}
				$kofi_ama1 = $res["s"];
				}
				
				$sql="SELECT SUM(amount) s FROM credits_cong WHERE type_code='kof' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'";
				$result = mysqli_query ($con, $sql);
				while($res = mysqli_fetch_array($result)){
				if($res['s']==""){
					$res['s']=0.00;
					}
				$kofi_ama2 = $res["s"];
				}
				$kofi_ama_sum=$kofi_ama1 +$kofi_ama2 ;*/
				
	// Total contributions by offering
				
				$sql=$pdo->query("SELECT SUM(amount) s FROM credits_ind WHERE type_code='off' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'");
				while($res = $sql->fetch()){
				if($res['s']==""){
					$res['s']=0.00;
					}
				$offering1 = $res["s"];
				}
				
				$sql=$pdo->query("SELECT SUM(amount) s FROM credits_cong WHERE type_code='off' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'");
				while($res = $sql->fetch()){
				if($res['s']==""){
					$res['s']=0.00;
					}
				$offering2 = $res["s"];
				}
				$offering_sum=$offering1 +$offering2 ;
				
	// Total contributions by thanks offering
				
				$sql=$pdo->query("SELECT SUM(amount) s FROM credits_ind WHERE type_code='tha' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'");
				while($res = $sql->fetch()){
				if($res['s']==""){
					$res['s']=0.00;
					}
				$thanks1 = $res["s"];
				}
				
				$sql=$pdo->query("SELECT SUM(amount) s FROM credits_cong WHERE type_code='tha' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'");
				while($res = $sql->fetch()){
				if($res['s']==""){
					$res['s']=0.00;
					}
				$thanks2 = $res["s"];
				}
				$thanks_sum=$thanks1 +$thanks2 ;
				
				
	// Total contributions by so mu bi
				
			/*	$sql="SELECT SUM(amount) s FROM credits_ind WHERE type_code='som' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'";
				$result = mysqli_query ($con, $sql);
				while($res = mysqli_fetch_array($result)){
				if($res['s']==""){
					$res['s']=0.00;
					}
				$som1 = $res["s"];
				}
				
				$sql="SELECT SUM(amount) s FROM credits_cong WHERE type_code='som' AND YEAR(date)='$year' AND MONTH(date)='$month' AND wk_no='$week'";
				$result = mysqli_query ($con, $sql);
				while($res = mysqli_fetch_array($result)){
				if($res['s']==""){
					$res['s']=0.00;
					}
				$som2 = $res["s"];
				}
				$som_sum=$som1 +$som2 ;*/
				

//percentages
//	 $total_income=$tithe_sum+$offering_sum+$kofi_ama_sum+$thanks_sum+$som_sum;
	 $total_income=$tithe_sum+$offering_sum+$thanks_sum;
		/* $tithe_sum= $tithe_sum*0.4;
		 $offering_sum= $offering_sum*0.4;
		// $kofi_ama_sum= $kofi_ama_sum*0.4;
		 $thanks_sum= $thanks_sum*0.4;
		// $som_sum= $som_sum*0.4;
		 $total_income= $total_income*0.4;*/
		
	 
	 if($total_income==0){
		$tithe_p='0.00'; 
		 $offering_p='0.00';
		// $kofi_ama_p='0.00';
		 $thanks_p='0.00';	
		 //$som_p=0.00;
		$total_income='0.00';
		
		$tithe_sum='0.00'; 
		 $offering_sum='0.00';
		// $kofi_ama_sum='0.00';
		 $thanks_sum='0.00';	
		 //$som_sum=0.00;
		$total_income='0.00';
		
	 }else{
		
		

		 $tithe_p= number_format((float)$tithe_sum/$total_income*$cent, 2, '.', '');
		 $offering_p= number_format((float)$offering_sum/$total_income*$cent, 2, '.', '');
	//	 $kofi_ama_p= number_format((float)$kofi_ama_sum/$total_income*$cent, 2, '.', '');
		 $thanks_p= number_format((float)$thanks_sum/$total_income*$cent, 2, '.', '');
	//	 $som_p= number_format((float)$som_sum/$total_income*$cent, 2, '.', '');
		// $other_p= number_format((float)$total_other/$total_income*$cent, 2, '.', '');
		 
		 $tithe_sum= number_format((float)$tithe_sum, 2, '.', ',');
		 $offering_sum= number_format((float)$offering_sum, 2, '.', ',');
	//	 $kofi_ama_sum= number_format((float)$kofi_ama_sum, 2, '.', ',');
		 $thanks_sum= number_format((float)$thanks_sum, 2, '.', ',');
	//	 $som_sum= number_format((float)$som_sum, 2, '.', ','); 
		 $total_income= number_format((float)$total_income, 2, '.', ',');
		 
		
		
	 }			
	$pdo=null;	
 
 
?>
