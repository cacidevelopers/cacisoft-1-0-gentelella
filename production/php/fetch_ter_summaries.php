<?php 
@session_start();
include 'db_con.php';
include 'validate.php';
$json = array();

	
$tot_membership=array();
$summaries = array();

//get no. of dioceses
			
			
	
		 $stmt = $pdo->query("SELECT * FROM territories");
		 		
		 while( $row = $stmt->fetch())  
		  { 
																		
		  
			$children=0;
			$m_youth=0;
			$f_youth=0;
			$ym_adult=0;
			$yf_adult=0;
			$m_adult=0;
			$f_adult=0;
			$m_aged=0;
			$f_aged=0;
			$boys=$girls=$boys1=$boys2=$girls1=$girls2=$total=0;
			
			//$ter_name= $row["ter_name"].'<br/>';
			$ter_code=$row["ter_code"];
			$ter_name=$row["ter_name"];

			
			
			$sql = $pdo->prepare("SELECT CONCAT(title,' ',first_name, ' ',middle_names,' ',last_name) AS name FROM ministers
			 RIGHT JOIN min_appointments ON ministers.user_id=min_appointments.user_id WHERE status='Territorial Apostle' AND min_appointments.ter_code='$ter_code' AND to_date='To Date'");
			$sql->execute([$row["ter_code"]]);
			$apostle=$sql->fetchColumn();
			if($apostle==''){
				$apostle="Not Assigned";
			}	
			
			$sql = $pdo->prepare("SELECT CONCAT(title,' ',first_name, ' ',middle_names,' ',last_name) AS name FROM ministers
			 RIGHT JOIN min_appointments ON ministers.user_id=min_appointments.user_id WHERE status='Territorial Secretary' AND min_appointments.ter_code='$ter_code' AND to_date='To Date'");
			$sql->execute([$row["ter_code"]]);
			$secretary=$sql->fetchColumn();
			if($secretary==''){
				$secretary="Not Assigned";
			}	
			
			$sql = $pdo->prepare("SELECT CONCAT(title,' ',first_name, ' ',middle_names,' ',last_name) AS name FROM ministers
			 RIGHT JOIN min_appointments ON ministers.user_id=min_appointments.user_id WHERE status='Territorial Fin Sec' AND min_appointments.ter_code='$ter_code' AND to_date='To Date'");
			$sql->execute([$row["ter_code"]]);
			$fin_sec=$sql->fetchColumn();
			if($fin_sec==''){
				$fin_sec="Not Assigned";
			}	
			
			$sql = $pdo->prepare("SELECT CONCAT(title,' ',first_name, ' ',middle_names,' ',last_name) AS name FROM members
			 RIGHT JOIN mem_appointments ON members.user_id=men_appointments.user_id WHERE status='Territorial Elder' AND men_appointments.ter_code='$ter_code' AND to_date='To Date'");
			$sql->execute([$row["ter_code"]]);
			$elder=$sql->fetchColumn();
			if($elder==''){
				$elder="Not Assigned";
			}	
			
			$sql = $pdo->prepare("SELECT CONCAT(title,' ',first_name, ' ',middle_names,' ',last_name) AS name FROM members
			 RIGHT JOIN mem_appointments ON members.user_id=men_appointments.user_id WHERE status='Office Manager' AND men_appointments.ter_code='$ter_code' AND to_date='To Date'");
			$sql->execute([$row["ter_code"]]);
			$manager=$sql->fetchColumn();
			if($manager==''){
				$manager="Not Assigned";
			}	
			
			//areas
			$sql = $pdo->query("SELECT COUNT(*) c FROM areas WHERE ter_code=$ter_code");
			$rec = $sql->fetch();
			$areas=$rec["c"];
			
			//circuits
			$sql = $pdo->query("SELECT COUNT(*) c FROM circuits WHERE ter_code=$ter_code");
			$rec = $sql->fetch();
			$circuits=$rec["c"];
			
			$sql = $pdo->query("SELECT dob, gender FROM members WHERE ter_code ='$ter_code'");
			 while( $rec = $sql->fetch())  
		  {
					if((date('Y') - date('Y',strtotime($rec["dob"])))<=17 && $rec["gender"]=='male' ){
				$boys+=1;
				}elseif((date('Y') - date('Y',strtotime($rec["dob"])))<=17 && $rec["gender"]=='female' ){
				$girls+=1;
				}if((date('Y') - date('Y',strtotime($rec["dob"])))>17 && (date('Y') - date('Y',strtotime($rec["dob"])))<=35 && $rec["gender"]=='male' ){
					$m_youth+=1;
				}elseif((date('Y') - date('Y',strtotime($rec["dob"])))>17 && (date('Y') - date('Y',strtotime($rec["dob"])))<=35 && $rec["gender"]=='female' ){
					$f_youth+=1;
				}elseif((date('Y') - date('Y',strtotime($rec["dob"])))>35 && (date('Y') - date('Y',strtotime($rec["dob"])))<=49 && $rec["gender"]=='male' ){
					$ym_adult+=1;
				}elseif((date('Y') - date('Y',strtotime($rec["dob"])))>35 && (date('Y') - date('Y',strtotime($rec["dob"])))<=49 && $rec["gender"]=='female'){
					$yf_adult+=1;
				}elseif((date('Y') - date('Y',strtotime($rec["dob"])))>49 && (date('Y') - date('Y',strtotime($rec["dob"])))<=60 && $rec["gender"]=='male' ){
					$m_adult +=1;
				}elseif((date('Y') - date('Y',strtotime($rec["dob"])))>49 && (date('Y') - date('Y',strtotime($rec["dob"])))<=60 && $rec["gender"]=='female'){
					$f_adult+=1;
				}elseif((date('Y') - date('Y',strtotime($rec["dob"])))>60 && $rec["gender"]=='male'){
					$m_aged+=1;
					
				}elseif((date('Y') - date('Y',strtotime($rec["dob"])))>60 && $rec["gender"]=='female' ){
					$f_aged+=1;
				}
		  }
			
		$total=	$m_aged+$f_aged+$m_adult+$f_adult+$ym_adult+$yf_adult+$m_youth+$f_youth+$boys+$girls;

    $bus = array(
	
		'Action' => '<a href="#dio_fetch_diocese_min_staff_details.php?id='.  $ter_code . '"class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Ministers </a>',
		'Territory' => $row["ter_name"],
		'Apostle' => $apostle,
		'Secretary' => $secretary,
		'Fin Secretary' => $fin_sec,
        'Elder' => $elder,
        'Office Manager' => $manager,
        'Areas' => $areas,
        'Circuits' => $circuits,
        'Aged:60+' => $m_aged+$f_aged,
        'Adults:45-59' => $m_adult+$f_adult,
        'Young Adults:35-44' => $ym_adult+$yf_adult,
        'Youth: 18-34' => $m_youth+$f_youth,
        'Children:0-17' => $boys+$girls,
        'Total Membership' => $total,
		'Territorial Code' => $ter_code
        
    ); 
    array_push($json, $bus);
}

$jsonstring = json_encode($json);
echo $jsonstring;

die();
					   
?>