<?php
session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"valeter")
{
	header("Location: " . 'index.php');

}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'head.php'; ?>
 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <?php include 'navbar_val.php'; ?>

            <div class="clearfix"></div>

 <!-- ==========menu profile quick info ===== -->
           <?php include 'profile.php'; ?>
			 <br />
 <!--========== /menu profile quick info ===-->
           

  <!--==========sidebar menu  =========-->
     <?php include 'menu_vals.php'; ?>
         
 <!--==============/sidebar menu======-->
  </div>
 </div>
 <!-- ==========top navigation ======-->
        <?php include 'top_nav.php'; ?>
 <!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4> My Invoices</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss" >
				
				 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="from_date">From
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="from_date" class="form-control has-feedback-left col-md-7 col-xs-12" name="from_date" placeholder="DD/MM/YYYY" type="date" >
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
						
                      </div>
					  
					    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="to_date">To
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="to_date" class="form-control has-feedback-left col-md-7 col-xs-12" name="to_date" placeholder="DD/MM/YYYY" type="date" >
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" ></span>
                        
                        </div>
                      </div>
					  
					 <br/><br/>
					 <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
									<tr >
											<th>Details</th>
											<th>Valeter ID</th>
											<!--th>Valeter Name</th-->
											<th>Jobs Completed</th>
											<th>Group Name</th>
									</tr>
							</thead >
							
						<tbody >
						<!--.dataTables_processing {
							left: 50%;
							position: absolute;
							top: 50%;
							z-index: 100;
						}-->
					  </tbody> 
					  <tfoot>
									<tr >									
											<th>Details</th>
											<th>Valeter ID</th>
											<!--th>Valeter Name</th-->
											<th>Jobs Completed</th>
											<th>Group Name</th>
									</tr>
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
                    </form>
				  
				  <!----FORM END--->
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

  
<script type="text/javascript">
 $(document).ready(function(){ 
var table= $('#results').DataTable( {
	 
		processing: true,
      //  serverSide: true,
	
    ajax: {
		type: 'POST',
 url: 'php/val_fetch_my_payslips.php',
		// data   : '4'; 
		 data: function ( d ) {
                d.from_date= $('#from_date').val();
                d.to_date= $('#to_date').val();
                d.sort= $('#sort').val();
		},
		dataType : 'json',
        dataSrc: '',
		dom: 'rtip', // the "r" is for the "processing" message
		language: {
		processing: "<span class='fa fa-refresh fa-refresh-animate'></span>"
		}, // you can put text or html here in the language.processing setting.
		processing: true, // you have to set this to true as well
				//deferRender: true
    },
    columns: [
			
            { data: "Details" },
            { data: "Valeter ID" },
           // { data: "Valeter Name" },
            { data: "Jobs Completed" },           
            { data: "Group Name" } 		
        ],
	 order: [[ 0, "asc" ]]	
											
} );


$('#from_date').change(function() {
       table.ajax.reload();
 } );
 
 $('#to_date').change(function() {
       table.ajax.reload();
 } );
 
$('#sort').change(function() {
        table.ajax.reload();
    } );

 $table.find('tbody').addClass('loading');
 
 

 
   $('#from_date').change(function(){  
           var year = $(this).val();  
		   console.log(from_date);
      });
	  
	  $('#to_date').change(function(){  
           var month = $(this).val();  
		   console.log(to_date);
      });
	  
	  $('#sort').change(function(){  
           var week_no = $(this).val();  
		   console.log(sort);
      });
 
 });
</script>

  <?php include 'timeout.php'; ?>  
    
  </body>
</html>
