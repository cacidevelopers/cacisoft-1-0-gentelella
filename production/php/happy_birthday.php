<?php 
@session_start();		
include 'db_con.php'; 
include 'eagle_eye.php'; 
include 'send_sms.php'; 
include 'send_mail.php';
	 
	  $m = date('n');
	  $d = date('j');
	
	//echo $m.' '.$d;
	$from = 'CACISoft birthday service';
	$name='';
	date_default_timezone_set("Africa/accra");
	$date= date("Y-m-d");
	$time=date("h:i:sa");
	$auth_level=$count=0;
	$assembly=$fname=$lname=$ter_code=$are_code=$cir_code=$mobile=$mobileNumber=$ter_name=$are_name=$cir_name=$user_id=$resp="";
	$email='michael.adjei@cac-int.org';
	$subject="BIRTHDAY NOTICE";
	
	
	$sql = $pdo->prepare("SELECT * FROM members WHERE MONTH(dob)=? AND DAY(dob)=? ");
	$sql->execute([$m,$d]);
	$data=$sql->fetchAll();
		
		foreach ($data as $row) {
		$fname=$row['first_name'];
		$lname=$row['last_name'];
		$ter_code=$row['ter_code'];
		$are_code=$row['are_code'];
		$cir_code=$row['cir_code'];
		$mobile=$row['mobile'];
		$mobileNumber = '"'.substr_replace($mobile,"",3,1).'"';	
					
				$stmt = $pdo->prepare("SELECT ter_name FROM territories WHERE ter_code = ? ");
				$stmt->execute([$ter_code]);
				$ter_name=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT are_name FROM areas WHERE are_code = ? ");
				$stmt->execute([$are_code]);
				$are_name=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT cir_name FROM circuits WHERE cir_code = ? ");
				$stmt->execute([$cir_code]);
				$cir_name=$stmt->fetchColumn();

				$name= $fname.' '.$lname;
				$assembly= $ter_name.'->'.$are_name.'->'.$cir_name;
				$altbody="HAPPY BIRTHDAY $name, from CAC, $cir_name Assembly";
				$body="HAPPY BIRTHDAY $name, from CAC, $cir_name Assembly";
				
				// Get cURL resource to send SMS
				sendSMS($mobile,$altbody);	
				sendMail($email,'',$user_id,'',$subject,$body,'');													
				
				//log sms 				
				$sql_insert ="INSERT INTO `sms_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `sent_by`, `sent_to`, `sms_logs`, `level`) VALUES (?,?,?,?,?,?,?,?,?,?)";
				$stmt= $pdo->prepare($sql_insert);
				$stmt->execute(['0',$ter_code,$are_code,$cir_code,$date,$time,$user_id,$assembly,$resp,$auth_level]);
		}
												   
												   //echo $assembly.': '.$name;
			$pdo=null;		
			
?>