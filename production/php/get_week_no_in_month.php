 <?php

 function getWeeks($date, $roll)
    {	
		
		$rollover='sunday';
		
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++)
        {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if($day == strtolower($rollover))	  $weeks ++;
        }

        return $weeks-1;
		
    }
/*
$rollover='';
$week=getWeeks(date("Y-m-d"), $rollover);
echo $week;
echo '<br>';*/
  // echo getWeeks("2018-04-13", "sunday"); //outputs 2, for the second week of the month
  
  
?>