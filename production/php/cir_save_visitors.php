<?php @session_start();

 
include 'db_con.php'; 
include 'validate.php';
	
		
	 // $picture_loaded =false;
	  $find = $message= "";
	  
	
	 	
	  $ter_code = $are_code = $cir_code = $last_name = $first_name = $purpose = $home_region = $home_town =$nationality= $ghanaian=$resp="";
	  
		  //retriving data
		  $hq_code='00';
		  $ter_code=$_SESSION["ter_code"];
		  $are_code=$_SESSION["are_code"];
          $cir_code=$_SESSION["cir_code"];
          $user_id=$_SESSION["user_id"];
			$purpose = addslashes(empty($_POST['purpose']) ? '' : validate($_POST['purpose']));   
		  $last_name =  ucWords(strtolower(empty($_POST['last_name']) ? '' : validate($_POST['last_name'])));
		 // $visitor_id =  ucWords(strtolower(empty($_POST['visitor_id']) ? '' : validate($_POST['visitor_id'])));
		  $first_name =  ucWords(strtolower(empty($_POST['first_name']) ? '' : validate($_POST['first_name'])));
          $title = ucWords(strtolower(empty($_POST['title']) ? '' : validate($_POST['title'])));
		  $mobile = empty($_POST['mobile']) ? '' : validate($_POST['mobile']);
		  $whatsapp = empty($_POST['whatsapp']) ? '' : validate($_POST['whatsapp']);
		  $ghanaian = empty($_POST['ghanaian']) ? '' : validate($_POST['ghanaian']);
		  $nationality = empty($_POST['nationality']) ? '' : validate($_POST['nationality']);
		  $date=date('d M Y');	
		  $date = date("Y-m-d", strtotime($date));
		  $time=date("h:i:sa");
          $counter=0;
          $auth_level='cir';
		  
		//format phone number "GH"
		    if($ghanaian=='yes'){
			  $nationality='Ghanaian';
		  }
		  //validating mobile number
		  //format +233xxxxxxxxx (replace + with "")
		   if(strpos($mobile, '+') === 0){
			$mobile = substr_replace($mobile,"",0,1);
		  }
		   		  
		  //format 2330xxxxxxxx (replace 0 with "")
		  if($ghanaian=='yes' and strlen($mobile)==13 and strpos($mobile, '0') === 3){
			$mobile = substr_replace($mobile,"",3,1);
			
		  }
		  
		  //format 0xxxxxxxxx (replace 0 with 233)
		  if($ghanaian=='yes' and strlen($mobile)==10 and strpos($mobile, '0') === 0){
			$mobile = substr_replace($mobile,"233",0,1);
			
		  }		  
		  
		  //format 00233xxxxxxxxx
		  if($ghanaian=='yes' and strlen($mobile)==14 and substr_compare($mobile,"00233",0)){
			$mobile = str_replace("00233","233",$mobile);
			exit;
		  }
		  
		  //format 002330xxxxxxxxx
		  if($ghanaian=='yes' and strlen($mobile)==15 and substr_compare($mobile,"002330",0)){
			$mobile = str_replace("002330","233",$mobile);
			
		  }



	
	  
	  //checking double registration
		$stmt ="SELECT * FROM visitors WHERE title = ? AND first_name= ? AND last_name= ? AND mobile= ? ";
		$stmt = $pdo->prepare($stmt);
		$stmt ->execute([$title,$first_name,$last_name, $mobile]);
		 if ($stmt->rowCount() > 0) {
              echo "duplicate";
				exit;
			   }else{
		  
		
		//saving visitor details	 
          $sql_insert ="  INSERT INTO `visitors`(`auth_level`, `ter_code`, `are_code`, `cir_code`, `last_name`, `first_name`, `nationality`, `title`, `mobile`, `whatsapp`, `reg_date`, `reg_time`,`purpose`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
          
		if(!empty($first_name) AND !empty($last_name) AND !empty($title) AND !empty($mobile) ){

		$stmt= $pdo->prepare($sql_insert);
		 $stmt->execute([$auth_level,$ter_code,$are_code,$cir_code,$last_name,$first_name,$nationality,$title,$mobile,$whatsapp,$date,$time,$purpose]);
		
		if( $stmt->rowCount()>0)  {	
		
		$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
						$stmt= $pdo->prepare($sql);
						$stmt->execute(['00',$ter_code,$are_code,$cir_code,$date,$time,$user_id,$auth_level, 'Registered visitor']);
						
					
						$sql_insert ="INSERT INTO `sms_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `sent_by`, `sent_to`, `sms_logs`, `level`) VALUES (?,?,?,?,?,?,?,?,?,?)";
						$stmt= $pdo->prepare($sql_insert);
						$stmt->execute(['0',$ter_code,$are_code,$cir_code,$date,$time,$user_id,$cir_code,$resp,$auth_level]);
                }
                
                echo 'completed';
						
		}else {
			  echo "error, record was not inserted!";
			  exit();
		  }
		
}
$pdo=null;	
 
?>