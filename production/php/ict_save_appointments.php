<?php
@session_start();
if(!isset($_SESSION['auth_level']) and !isset($_SESSION['user_id']))
{
	session_destroy();
	header("Location: ../index.php");
	
}
include 'db_con.php'; 
include 'validate.php';
	
	 $count = $find = $message= "";
     $user_id=$status=$con_code=$ter_code=$are_code=$cir_code=$aux_code=$from_date=$to_date=$old_app_no=''; 
		 $auth_level=4;
		 $new_user_id= $_SESSION["new_user_id"];
		 $old_app_no=$_SESSION["old_app_no"];
		 $status= $_SESSION["status"];
		 $hq_code='0';
		 $ter_code= $_SESSION["ter_code"];
		 $are_code=$_SESSION["are_code"];
		 $cir_code=$_SESSION["cir_code"];
		 $fam_code=$_SESSION["new_fam_code"];
		 $aux_code=$_SESSION["new_aux_code"];
		 $from_date=$_SESSION["new_from_date"];
		 $old_user_id=$_SESSION["old_user_id"];
		 $user_id=$_SESSION["user_id"];
		 $to_date="To Date";
		 
        	 
		//Saving appointmens details	
		$date = date_create($from_date);
		date_sub($date, date_interval_create_from_date_string('1 days'));
		$end_date= date_format($date, 'Y-m-d');
		//$user_id=$_SESSION["member_user_id"];
		//echo $to_date;
		
		 $log_date=date('d M Y');	
		  $date = date("Y-m-d", strtotime($log_date));
		  $time=date("H:i:s");
		
		//set end date for existing appointment
		$sql = "UPDATE `mem_appointments` SET `to_date`= ?  WHERE `app_no`= ? ";
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$end_date,$old_app_no]);

		//save new appointment	
		 $sql_insert ="INSERT INTO `mem_appointments`(`user_id`, `status`, `hq_code`, `ter_code`, `are_code`, `cir_code`, `fam_code`, `aux_code`, `level`, `from_date`, `to_date`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		 $stmt1 = $pdo->prepare($sql_insert);
		 $stmt1->execute([$new_user_id,$status,$hq_code,$ter_code,$are_code,$cir_code,$fam_code,$aux_code,$auth_level,$from_date,$to_date]);
		
			if ($stmt->rowCount() > 0 and $stmt1->rowCount() > 0) {
			//audit logs 
				$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
						$stmt= $pdo->prepare($sql);
						$stmt->execute([$hq_code,$ter_code,$are_code,$cir_code,$date,$time,$user_id,$auth_level, 'Appointed '.$new_user_id.' as '.$status]);
				echo "success";
			}
		
		else 
			{
				//echo $count;
				echo "error";
			}
			unset($_SESSION["old_user_id"]);
		$pdo=null;	
		
		
?>