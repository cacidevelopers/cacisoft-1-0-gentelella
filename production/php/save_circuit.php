<?php @session_start();


		include 'db_con.php'; 
		include 'validate.php'; 
	 
	  $count = $find = $message= "";
      $ter_code = $town_name = $area_name = $street_name = $hse_no = $postal_address = $mobile_one = $mobile_two = $email = $date_founded =
	  $front_pic=$back_pic=$chu_building=$chu_building_status=$building_desc=$mission_hse=$mission_hse_status=$mission_hse_desc="";
	  
		  //retriving data
		  //$qid = empty($_POST['qid']) ? '' : $_POST['qid'];
		  $ter_code = empty($_POST['ter_code']) ? '' : validate($_POST['ter_code']);	
		  $are_code = empty($_POST['are_code']) ? '' : validate($_POST['are_code']);	
		  $cir_code = empty($_POST['cir_code']) ? '' : validate($_POST['cir_code']);
		  $cir_code=$are_code.$cir_code;
		  $cir_name =  ucWords(strtolower(empty($_POST['cir_name']) ? '' : validate($_POST['cir_name'])));
		  $building_desc= empty($_POST['building_desc']) ? '' : validate($_POST['building_desc']);
		  $mission_hse_desc= empty($_POST['mission_hse_desc']) ? '' : validate($_POST['mission_hse_desc']);
		  $chu_building_status =  ucWords(strtolower(empty($_POST['chu_building_status']) ? '' : validate($_POST['chu_building_status'])));
		  $mission_hse_status =  ucWords(strtolower(empty($_POST['mission_hse_status']) ? '' : validate($_POST['mission_hse_status'])));
          $town_name = ucWords(strtolower(empty($_POST['town_name']) ? '' : validate($_POST['town_name'])));
		  $area_name = ucWords(strtolower(empty($_POST['area_name']) ? '' : validate($_POST['area_name'])));
          $street_name = ucWords(strtolower(empty($_POST['street_name']) ? '' : validate($_POST['street_name'])));
          $hse_no = empty($_POST['hse_no']) ? '' : validate($_POST['hse_no']);
          $postal_address = ucWords(strtolower(empty($_POST['postal_address']) ? '' : validate($_POST['postal_address'])));
		  $gps_code = ucWords(strtolower(empty($_POST['gps_code']) ? '' : validate($_POST['gps_code'])));
          $mobile_one =empty($_POST['mobile_one']) ? '' :  validate($_POST['mobile_one']);
          $mobile_two =empty($_POST['mobile_two']) ? '' :  validate($_POST['mobile_two']);
          $email =strtolower(empty($_POST['email']) ? '' :  validate($_POST['email']));
		  $date_founded =empty($_POST['date_founded']) ? '' :  validate($_POST['date_founded']);
		  $date=date('d M Y');	
		  $date = date("Y-m-d", strtotime($date));
		  $time=date("h:i:sa");
		  
       	 //front_pic upload start		  
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
			$path = '../pics/'; // upload directory
			if(isset($_FILES['front_pic']))
			{
			 $img = $_FILES['front_pic']['name'];
			 $tmp = $_FILES['front_pic']['tmp_name'];

			 // get uploaded file's extension
			// $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			 $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			 
			 // can upload same front_pic using rand function
			 $front_pic = rand(1000,1000000)."-".date('Ymdhms').'.'.$ext;
			 if(in_array($ext, $valid_extensions)) 
			 {     
			  $path = $path.strtolower($front_pic); 
				$front_pic = strtolower($front_pic);
			  if(move_uploaded_file($tmp,$path)) 
			  {
			   echo "<$img src='$path' />";
			   $picture_loaded=true;
			  }
			 } 
			 else 
			 {
			  echo 'invalid_file';
			 }
			}
			//upload end
			
			
			
			//back_pic upload start		  
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
			$path = '../pics/'; // upload directory
			if(isset($_FILES['back_pic']))
			{
			 $img = $_FILES['back_pic']['name'];
			 $tmp = $_FILES['back_pic']['tmp_name'];

			 // get uploaded file's extension
			// $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			 $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			 
			 // can upload same back_pic using rand function
			 $back_pic = rand(1000,1000000)."-".date('Ymdhms').'.'.$ext;
			 if(in_array($ext, $valid_extensions)) 
			 {     
			  $path = $path.strtolower($back_pic); 
				$back_pic = strtolower($back_pic);
			  if(move_uploaded_file($tmp,$path)) 
			  {
			   echo "<$img src='$path' />";
			   $picture_loaded=true;
			  }
			 } 
			 else 
			 {
			  echo 'invalid_file';
			 }
			}
			//upload end
			
			//chu building upload start		  
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
			$path = '../pics/'; // upload directory
			if(isset($_FILES['chu_building']))
			{
			 $img = $_FILES['chu_building']['name'];
			 $tmp = $_FILES['chu_building']['tmp_name'];

			 // get uploaded file's extension
			// $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			 $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			 
			 // can upload same chu_building using rand function
			 $chu_building = rand(1000,1000000)."-".date('Ymdhms').'.'.$ext;
			 if(in_array($ext, $valid_extensions)) 
			 {     
			  $path = $path.strtolower($chu_building); 
				$chu_building = strtolower($chu_building);
			  if(move_uploaded_file($tmp,$path)) 
			  {
			   echo "<$img src='$path' />";
			   $picture_loaded=true;
			  }
			 } 
			 else 
			 {
			  echo 'invalid_file';
			 }
			}
			//upload end
			
			//mission upload start		  
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
			$path = '../pics/'; // upload directory
			if(isset($_FILES['mission_hse']))
			{
			 $img = $_FILES['mission_hse']['name'];
			 $tmp = $_FILES['mission_hse']['tmp_name'];

			 // get uploaded file's extension
			// $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			 $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			 
			 // can upload same mission_hse using rand function
			 $mission_hse = rand(1000,1000000)."-".date('Ymdhms').'.'.$ext;
			 if(in_array($ext, $valid_extensions)) 
			 {     
			  $path = $path.strtolower($mission_hse); 
				$mission_hse = strtolower($mission_hse);
			  if(move_uploaded_file($tmp,$path)) 
			  {
			   echo "<$img src='$path' />";
			   $picture_loaded=true;
			  }
			 } 
			 else 
			 {
			  echo 'invalid_file';
			 }
			}
			//upload end
			
			 
		 $stmt = "SELECT * FROM circuits WHERE cir_code='$cir_code' AND are_code ='$are_code' ";
		$res = $pdo->query($stmt);
		 if ($res->rowCount() > 0) {
              echo "duplicate";
			  exit;
          } 
		 
		 
		  $sql = " INSERT INTO `circuits`(`ter_code`, `are_code`, `cir_code`, `cir_name`, `front_pic`, `back_pic`, `chu_building`, `chu_building_status`, `building_desc`, `mission_hse`, `mission_hse_status`, `mission_hse_desc`, `town_name`, `area_name`, `street_name`, `hse_no`, `postal_address`, `gps_code`, `mobile_one`, `mobile_two`, `email`, `date_founded`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 $stmt= $pdo->prepare($sql);
		 $stmt->execute([$ter_code,$are_code,$cir_code,$cir_name,$front_pic,$back_pic,$chu_building,$chu_building_status,$building_desc,$mission_hse, $mission_hse_status,$mission_hse_desc,$town_name,$area_name,$street_name,$hse_no,$postal_address,$gps_code,$mobile_one,$mobile_two,$email,$date_founded]);
		if( $stmt ) {
				//audit logs 
				$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
						$stmt= $pdo->prepare($sql);
						$stmt->execute(['00',$ter_code,$_SESSION['are_code'],$_SESSION['cir_code'],$date,$time,$_SESSION['user_id'],$_SESSION['auth_level'], 'Created Circuit '.$cir_name.'Under'.$are_code.' Area']);
				echo "success";
			}
		
		else 
			{
				//echo $count;
				echo "error";
			}
		
		$pdo=null;
		
?>