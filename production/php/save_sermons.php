<?php @session_start();
if(!isset($_SESSION['auth_level']) and !isset($_SESSION['user_id']))
{
	session_destroy();
	header("Location: ../index.php");
	
}

		include 'db_con.php'; 
		include 'validate.php'; 
	
	  $file_loaded =false;
	  $file_id = $count = $find = $message=  $checksum="";
	  
	
				
	  $ter_code = $are_code = $cir_code = $con_code = $user_id = $date = $title = $author = "";
	  
		  //retriving data
		  
		//  $con_code = "con";
		  $ter_code = $_SESSION["ter_code"];
		  $are_code = $_SESSION["are_code"];
		  $cir_code = $_SESSION["cir_code"];
		  $user_id = $_SESSION["user_id"];
		//  $sender = $_SESSION["first_name"].' '.$_SESSION["last_name"];
		  $date=date('d M Y');	
		  $date = date("Y-m-d", strtotime($date));
		  $level= 0;
		  $time=date("H:i:s");
		  $auth_level='cir';
		  $title =  addslashes(ucWords(strtolower(empty($_POST['title']) ? '' : validate($_POST['title']))));
		  $author =  addslashes(empty($_POST['author']) ? '' : validate($_POST['author']));
		  		  
//file upload start	
	  
$valid_extensions = array( 'pdf', 'doc', 'docx', 'pptx', 'xls'); // valid extensions .pdf, .docx, .doc, .xls, .pptx
$path = '../sermons/'; // upload directory

if(isset($_FILES['file']))
{
 $sermon = $_FILES['file']['name'];
 //$checksum=sha1($report.$ter_code.$are_code.$cir_code);
 $tmp = $_FILES['file']['tmp_name'];

 // get uploaded file's extension
 $ext = pathinfo($sermon, PATHINFO_EXTENSION);
 
 // can upload same picture using rand function
 $file_id = rand(10000,1000000).$sermon;
 
 // check's valid format
 if(in_array($ext, $valid_extensions)) 
 {  

  $path = $path.strtolower($file_id); 
  $file_id = strtolower($file_id); 
   
  if(move_uploaded_file($tmp,$path)) 
  {
   echo "<$sermon src='$path' />";
   $file_loaded=true;
  }
 } 
 else 
 {
  echo 'invalid_file';
  exit;
 }
}
//upload end
				
		//Saving Sermon details

		$stmt ="SELECT * FROM sermons WHERE author = ? AND title= ? ";
		$stmt = $pdo->prepare($stmt);
		$stmt ->execute([$author,$title]);
		 if ($stmt->rowCount() > 0) {
							echo "duplicate";
		    } else 
		 {
			// $_SESSION["temp_id"]=$picture_id; 
			 
		$sql ="INSERT INTO `sermons`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `title`, `sermon`, `author`, `sender_id`) VALUES	(?,?,?,?,?,?,?,?,?)"; 
		$stmt= $pdo->prepare($sql);
		$stmt->execute(['00',$ter_code,$are_code,$cir_code,$date,$title,$file_id,$author,$user_id]);
		
		if( $stmt->rowCount()>0)  {	
							
				//audit logs 
				$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
				$stmt= $pdo->prepare($sql);
				$stmt->execute(['00',$ter_code,$are_code,$cir_code,$date,$time,$user_id,$auth_level, 'Posted a sermon:'.$file_id]);		 
					
				echo "success";
					
					}else {
							echo "error, record was not inserted!";
						}
						$pdo=null;
					}
?>

