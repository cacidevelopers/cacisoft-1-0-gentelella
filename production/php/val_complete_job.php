<?php
session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"valeter")
{
	header("Location: " . 'index.php');

}


?>

<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'head.php'; ?>
 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <?php include 'navbar_val.php'; ?>

            <div class="clearfix"></div>

 <!-- ==========menu profile quick info ===== -->
           <?php include 'profile.php'; ?>
			 <br />
 <!--========== /menu profile quick info ===-->
           

  <!--==========sidebar menu  =========-->
     <?php include 'menu_vals.php'; ?>
         
 <!--==============/sidebar menu======-->
  </div>
 </div>
 <!-- ==========top navigation ======-->
        <?php include 'top_nav.php'; ?>
 <!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>COMPLETED JOB DETAILS</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
<?php 
@session_start();
//include 'php/db_con.php';
include 'php/validate.php';
  
  
if(isset($_GET['id'])) 
{
  $job_no = empty($_GET['id']) ? '' :  validate($_GET['id']);
  
}
 unset( $_SESSION['job_no']);
  $_SESSION['job_no']=$job_no;
  $req_by='';
	
  $data = $pdo->query("SELECT * FROM jobs WHERE job_no='$job_no'");
  $row = $data->fetch();
	

						$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM clients WHERE email = ? ");
				$stmt->execute([$row["sender"]]);
				$rec=$stmt->fetch();
				if ($stmt->rowCount() > 0){
							$req_by=$rec["name"];
				}
				
				$stmt1 = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE email = ? ");
				$stmt1->execute([$row["sender"]]);
				$rec=$stmt1->fetch();
				if ($stmt1->rowCount() > 0){
							$req_by=$rec["name"];
				}
						
						/*$dept=$row['department'];
						$datum2 = $pdo->query("SELECT name FROM departments WHERE code='$dept'");
					    $rec2 = $datum2->fetch();
						$dep_name=$rec2["name"];*/
?>
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">JOB NO:<?php echo ' '.$job_no;?></label>
                        </div>
					  
										  
					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="reg_no" ><strong>Date Requested :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="reg_no" ><?php echo $row['date'];?></label>
                          
                      </div>  
                      </div>  
					 
					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="gender" ><strong>Time Requested :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="gender" ><?php echo $row['time'];?></label>
                         
                      </div>   
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="age" ><strong>Requested By :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="age" ><?php echo $req_by;?></label>
                         
                      </div>   
                      </div> 
						 <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="married" ><strong>Vehicle Make :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="married" ><?php echo $row['make'];?></label>
                          
                      </div>   
                      </div> 
					   <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="qualification" ><strong>Vehicle No :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="qualification" ><?php echo $row['veh_no'];?></label>
                         
                      </div>   
                      </div> 
					   <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="profession" ><strong>Cleaning Type :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="profession" ><?php echo $row['clean_type'];?></label>
                         
                      </div>   
                      </div> 
					   <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="occupation" ><strong>Expected Date :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="occupation" ><?php echo $row['exp_date'];?></label>
                          
                      </div>   
                      </div> 
					   <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="mobile" ><strong>Expected Time :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="mobile" ><?php echo $row['exp_time'];?></label>
                          
                      </div>   
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="occupation" ><strong>Accepted Date :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="occupation" ><?php echo $row['start_date'];?></label>
                          
                      </div>   
                      </div> 
					   <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="mobile" ><strong>Accepted Time :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="mobile" ><?php echo $row['start_time'];?></label>
                          
                      </div>   
                      </div> 
					    
					   <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="add" ><strong>Location :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="add" ><?php echo $row['location'];?></label>
                         
                      </div>   
                      </div> 
					    <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="cir" ><strong>Special Request :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="cir" ><?php echo $row['special_req'];?></label>
                          
                      </div>   
                      </div> 
					   <!--div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="dio" ><strong>Department :</strong></label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
						  <label class="control-label col-md- col-sm-3 col-xs-12" for="dio" ><?php echo $rec2['name'];?></label>
                          
                      </div>   
                      </div--> 
					  
					  	
					  <br /><br /><br />
					  
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
						 <input id="reply" type="submit" class="btn btn-success btn-md" style="width:100%" value="Validate Job "/>
                        <!--button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button-->
						<button class="btn btn-success btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='val_view_awaiting_jobs.php' "> Cancel</button>
					 </div>
                   </div>
					
                 
				  
				  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The Job was successfully set to Completed!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-warning" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-warning">Validate Another Job</button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The system could not Validate the Job!</p>
                            <p>Please try again or contact Help Desk</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-warning" data-dismiss="modal">Cancel!</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				  
				 
					  
					  <div id= "err" > </div>
					 
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

    
    <script>
$(document).ready(function (e) {
	
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
  $.ajax({
   url: "php/update_completed_jobs.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    $("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    
     if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
                }
				if(data.match(/duplicate/gi)){
					// alert("Member is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
				
				if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    //$("#err").html(e).fadeIn();
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					 window.location.replace("val_view_awaiting_jobs.php");
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("val_view_awaiting_jobs.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
						 window.location.replace("val_view_awaiting_jobs.php");
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				$("#dup_cancel").click(function(){
					 window.location.replace("val_view_awaiting_jobs.php");	
			});
	
 })
 )
 
 
 
 
});
    </script>

  <?php include 'timeout.php'; ?>	
    
  </body>
</html>
