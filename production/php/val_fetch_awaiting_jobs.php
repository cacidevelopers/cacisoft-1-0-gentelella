<?php  session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}

include 'db_con.php';
$currentWeekNumber = date('W');

$json = array();
$sql = $pdo->prepare("SELECT * FROM jobs WHERE assigned='No'");
		$sql->execute();
		$data=$sql->fetchAll();
		
foreach ($data as $row) {
	$this_wk=date('W',strtotime($row["exp_date"]));
	//if($currentWeekNumber==$this_wk){
	//$user_id=$row['sender'];	
	$sender=$row['sender'];
	$valeter=$row['valeter'];

	/*$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$user_id]);
				$assigned_by=$stmt->fetchColumn();*/
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM clients WHERE user_id = ? ");
				$stmt->execute([$sender]);
				$rec=$stmt->fetch();
				if ($stmt->rowCount() > 0){
							$sender=$rec["name"];
				}
				
				$stmt1 = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt1->execute([$sender]);
				$rec=$stmt1->fetch();
				if ($stmt1->rowCount() > 0){
							$sender=$rec["name"];
				}
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$valeter]);
				$valeter=$stmt->fetchColumn();
				
	
    $bus = array(
		 
		 
	 //'Delete' => '<a href="php/adm_delete_awaiting_jobs.php?id='. $row['job_no'] . '"class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Delete </a>',
		'View' => '<a href="val_view_awaiting_job_profile.php?id='. $row['job_no'] . '"class="btn btn-success btn-xs"><i class="fa fa-edit"></i> View </a>',
		'Job No' => $row['job_no'],
        'Sent Date' => $row['date'],
        'Sent Time' => $row['time'],
		'Sender' => $sender,
		'Vehicle/Chasis No' => $row['veh_no'],
		'Make' => $row['make'],
		'Clean Type' => $row['clean_type'],
		'Expected Date' => $row['exp_date'],
		'Expected Time' => $row['exp_time'],
		'Location' => $row['location'],
		'Special Request' => $row['special_req']
    );
    array_push($json, $bus);
	//}	
}

$jsonstring = json_encode($json);
echo $jsonstring;

die();
?>

