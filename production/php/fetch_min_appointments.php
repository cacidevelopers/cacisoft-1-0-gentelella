<?php @session_start();
include 'db_con.php';
$json = array();
$all_m_aged=ARRAY();
	$places=ARRAY();
	

    $data = $pdo->query("SELECT * FROM min_appointments ")->fetchAll();
		
	foreach ($data as $row) {

    	$app_no=$row['app_no'];
    
		
		$sql = $pdo->prepare("SELECT CONCAT(title,' ',first_name, ' ',middle_names,' ',last_name) AS name FROM ministers WHERE app_no=?");
		$sql->execute([$app_no]);
		$name=$sql->fetchColumn();
		
		$ter_code=$row['ter_code'];
		$are_code=$row['are_code'];
		$cir_code=$row['cir_code'];
				
		if($ter_code==0 AND $are_code==0 AND $cir_code==0){
			$at='National';
		}

		if($ter_code <>0 AND $are_code==0 AND $cir_code==0){
			$sql = $pdo->prepare("SELECT ter_name FROM territories WHERE ter_code=?");
			$sql->execute([$row["ter_code"]]);
			$at=$sql->fetchColumn();
			
		}
		if($ter_code <>0 AND $are_code <>0 AND $cir_code==0){
			$sql = $pdo->prepare("SELECT are_name FROM areas WHERE are_code=?");
			$sql->execute([$row["are_code"]]);
			$at=$sql->fetchColumn();
			
		}
		if($ter_code <>0 AND $are_code <>0 AND $cir_code <>0){
			$sql = $pdo->prepare("SELECT cir_name FROM circuits WHERE cir_code=?");
			$sql->execute([$row["cir_code"]]);
			$at=$sql->fetchColumn();
		}
		  
	}

	
    $bus = array(
		 
       'Name' => $name,
		'Status' => $row['status'],
		'At' => $at,
		'From' => $row['from_date'],
		'To' => $row['to_date']
    );
    array_push($json, $bus);


$jsonstring = json_encode($json);
echo $jsonstring;

die();
?>

