<?php
session_start();
if(!isset($_SESSION['auth_level']) and !isset($_SESSION['reg_no']))
{
	session_destroy();
	header("Location: ../index.php");
	
}
		include 'db_con.php'; 
		include 'validate.php';
	
	  $count = $find = $message= "";
     $old_user_id=$reg_no=$status=$con_code=$ter_code=$cir_code=$are_code=$fam_code=$from_date=$to_date=''; 
		 $level=4;
		
		
		 $old_app_no=$_SESSION["old_app_no"];
		 $old_user_id= $_SESSION["old_user_id"];
		 $new_user_id=$_SESSION["new_user_id"];
		 $status= $_SESSION["status"];
		 $fam_code=$_SESSION["new_fam_code"];
		 $aux_code=$_SESSION["new_aux_code"];
		 $from_date=$_SESSION["new_from_date"];
		 $ter_code= $_SESSION["ter_code"];
		 $are_code=$_SESSION["are_code"];
		 $cir_code=$_SESSION["cir_code"];
         $hq_code='0';
		 $to_date='To Date'; 
		 
		
		 
		//Saving appointmens details
		$date = date_create($from_date);
		date_sub($date, date_interval_create_from_date_string('1 days'));
		$end_date= date_format($date, 'Y-m-d');
		//$reg_no=$_SESSION["member_reg_no"];
		//echo $to_date;
		
		 $log_date=date('d M Y');	
		  $log_date = date("Y-m-d", strtotime($log_date));
		  $time=date("H:i:s");
         
          $sql = " UPDATE `mem_appointments` SET `to_date`= '$end_date' WHERE `app_no`= '$old_app_no'";
          // Prepare statement
          $stmt = $pdo->prepare($sql);
          // execute the query
          $stmt->execute();
          if( $stmt->rowCount()>0)  {
            $sql_insert ="INSERT INTO `mem_appointments`(`user_id`, `status`, `hq_code`, `ter_code`, `are_code`, `cir_code`, `fam_code`, `aux_code`, `level`, `from_date`, `to_date`) VALUES	(?,?,?,?,?,?,?,?,?,?,?)";
				
            $stmt= $pdo->prepare($sql_insert);
            $stmt->execute([$new_user_id,$status,$hq_code,$ter_code,$are_code,$cir_code,$fam_code,$aux_code,$auth_level,$from_date,$to_date]);
            if( $stmt->rowCount()>0)  {
				//audit logs 
               
					//audit logs 
					$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
						$stmt= $pdo->prepare($sql);
						$stmt->execute([$hq_code,$ter_code,$are_code,$cir_code,$log_date,$time,$_SESSION['user_id'],$auth_level, 'Appointed '.$new_user_id.' as '.$status]);
					
					echo "success";
                  
                }else{
                    echo "error, new appointment could not save";  
                    exit();
                }
        }else{
            echo "error, end date for old appointee could not update";
            exit();
        }
    
        $pdo=null;
		
?>