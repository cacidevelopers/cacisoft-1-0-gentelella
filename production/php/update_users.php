<?php
@session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}
//$code=$_SESSION["cicode"];
//$level=3;

// DataTables PHP library

include( "../js/editor/php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST

Editor::inst( $db, 'users','no' )
	->fields(
		Field::inst( 'contact_no' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'first_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'last_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'emerg_contact' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'ec_mob1' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'ec_mob2' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'auth_level' )->validator( 'Validate::notEmpty'),
		Field::inst( 'dept_code' )->validator( 'Validate::notEmpty' )
		
		)
		
	
	//->where( $key = "code", $value = $cir_code, $op = "=" )
	//->where( $key = "level", $value = $level, $op = "=" )
	//->where($key = "soc_code", $value = " ", $op = "=" )
	
	->process( $_POST )
	->json();


