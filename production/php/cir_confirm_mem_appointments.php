<?php
@session_start();
if(!isset($_SESSION['auth_level']) and !isset($_SESSION['user_id']))
{
	session_destroy();
	header("Location: ../index.php");
	
}
include 'db_con.php'; 
include 'validate.php';
		
	//  $count = $find = $message= "";
   // $old_user_id= $clergy_no=$status=$hq_code=$ter_code=$are_code=$cir_code=$fam_code=$from_date=$to_date=''; 
		  //retriving data
		  //$qid = empty($_POST['qid']) ? '' : $_POST['qid'];
				$mem_title='';
				$mem_fname='';
				$mem_lname='';
				
		  
		 $new_user_id = empty($_POST['user_id']) ? '' : validate($_POST['user_id']);	
		 $auth_level=4;
		 $fam_code = empty($_POST['fam_code']) ? '' : validate($_POST['fam_code']);
		 $aux_code = empty($_POST['aux_code']) ? '' : validate($_POST['aux_code']);
		
         $from_date =empty($_POST['from_date']) ? '' :  validate($_POST['from_date']);
		 $status = addslashes(ucWords(strtolower(empty($_POST['status']) ? '' : validate($_POST['status']))));
		 $hq_code='0';
		 $to_date='To Date';  
		 $ter_code=$_SESSION["ter_code"];
		 $are_code=$_SESSION["are_code"];
		 $cir_code=$_SESSION["cir_code"];
		// $fam_code=$_SESSION["fam_code"];
		 //$aux_code=$_SESSION["aux_code"];
		 //$user_id=$_SESSION["user_id"];
		 $old_user_id='';
		 
		  $date=date('d M Y');	
		  $date = date("Y-m-d", strtotime($date));
		  $time=date("H:i:s");
		  
		//Saving Member details
		 $search = "SELECT * FROM `mem_appointments` WHERE `status` = ? AND `hq_code`= ? AND `ter_code` = ? AND `are_code`= ? AND `cir_code`= ? AND `to_date`= ? AND `fam_code`=?  AND `aux_code` = ?";

		 $stmt= $pdo->prepare($search);
		 $stmt->execute([$status,$hq_code,$ter_code,$are_code,$cir_code,$to_date,$fam_code,$aux_code]);
		 $row = $stmt->fetch();
           if ($stmt->rowCount() > 0) {
              				
				$old_user_id=$row["user_id"];
				$_SESSION["old_app_no"]=$row["app_no"];
				$_SESSION["old_user_id"]= $old_user_id;
				$_SESSION["new_user_id"]=$new_user_id;
				$_SESSION["status"]=$status;
				$_SESSION["new_fam_code"]=$fam_code;
				$_SESSION["new_aux_code"]=$aux_code;
				$_SESSION["new_from_date"]=$from_date;
			
			  
			 
				$sql = "SELECT * FROM members WHERE user_id = ? ";
				$stmt=$pdo->prepare($sql);
				$stmt->execute([$old_user_id]);
				while($row = $stmt->fetch())
				{	
					$mem_title=$row["title"];
					$mem_fname=$row["first_name"];
					$mem_lname=$row["last_name"];
				}
			
				echo "<p>The Position of: <strong>".$status." </strong>is currently held by: <strong>".$mem_title." ".$mem_fname." ".$mem_lname."</strong></p>. Do you want to Proceed?";
				
				exit;
				 
          } else{
			    
				 $sql_insert ="INSERT INTO `mem_appointments`(`user_id`, `status`, `hq_code`, `ter_code`, `are_code`, `cir_code`, `fam_code`, `aux_code`, `level`, `from_date`, `to_date`) VALUES	(?,?,?,?,?,?,?,?,?,?,?)";
				
				$stmt= $pdo->prepare($sql_insert);
				$stmt->execute([$new_user_id,$status,$hq_code,$ter_code,$are_code,$cir_code,$fam_code,$aux_code,$auth_level,$from_date,$to_date]);
						
				if( $stmt->rowCount()>0)  {
					//audit logs 
					$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
						$stmt= $pdo->prepare($sql);
						$stmt->execute([$hq_code,$ter_code,$are_code,$cir_code,$date,$time,$_SESSION['user_id'],$auth_level, 'Appointed '.$new_user_id.' as '.$status]);
					
					echo "success";
				  }
				else 
			{
				echo "error, new appointment could not save";  
			}
		  }
		$pdo=null;
		
?>