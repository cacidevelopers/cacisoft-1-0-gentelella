<?php  session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}

include 'db_con.php';
$currentWeekNumber = date('W');
$json = array();


$data = $pdo->query("SELECT * FROM jobs WHERE status='queued'")->fetchAll();
		
foreach ($data as $row) {
	$this_wk=date('W',strtotime($row["exp_date"]));
	if($currentWeekNumber==$this_wk){
	$user_id=$row['assigned_by'];
	$sender=$row['sender'];
	$valeter=$row['valeter'];

	$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$user_id]);
				$assigned_by=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM clients WHERE user_id = ? ");
				$stmt->execute([$sender]);
				$sender=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$valeter]);
				$valeter=$stmt->fetchColumn();
				
	
    $bus = array(
		 
		 'Delete' => '<a href="#user_del_job.php?id='. $row['job_no'] . '"class="btn btn-success btn-xs"><i class="fa fa-ban"></i> Delete </a>',
		 'Assign' => '<a href="usr_view_job_profile.php?id='. $row['job_no'] . '"class="btn btn-success btn-xs"><i class="fa fa-edit"></i> Assign </a>',
		'Job No' => $row['job_no'],
		'Sent Date' => $row['date'],
        'Sent Time' => $row['time'],
		'Sender' => $sender,
		'Vehicle/Chasis No' => $row['veh_no'],
		'Make' => $row['make'],
		'Clean Type' => $row['clean_type'],
		'Expected Date' => $row['exp_date'],
		'Expected Time' => $row['exp_time'],
		'Location' => $row['location'],
		'Special Request' => $row['special_req'],
		'Assigned By' => $assigned_by,
		'Valeter' => $valeter,
		'Department' =>  $row['department']
    );
    array_push($json, $bus);
}
}
$jsonstring = json_encode($json);
echo $jsonstring;

die();
?>

