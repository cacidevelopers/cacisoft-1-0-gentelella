<?php @session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}		
		
		include 'db_con.php'; 
		include 'eagle_eye.php';
		include 'send_mail.php'; 
		
		//use PHPMailer\PHPMailer\PHPMailer;
		//use PHPMailer\PHPMailer\Exception;
		//require_once('PHPMailer/vendor/autoload.php');
		include 'validate.php'; 
		include 'generate_random_number.php';
		


	
	  $account_status="active";
	  $message_Id='';
	$subject='Login Details';
	  $picture_loaded =false;
	  $picture_id = $count = $find = $message= "";
	  
		
	 $user_id  = $id =$auth_level = $last_name = $first_name = $contact_no = $user_email = $email = $emerg_contact = $ec_mob1 =  $ec_mob2 = $date = $time = $picture_id=$dept_code=$pass=$password=$reg_time=$reg_date=$busy="";
	 
		  //retrieving data
		  $user_id=$_SESSION['user_id'];
		  $group_code=$_SESSION['grp_code'];
		  $auth_level = empty($_POST['auth_level']) ? '' : validate($_POST['auth_level']);
          $last_name =  ucWords(strtolower(empty($_POST['last_name']) ? '' : validate($_POST['last_name'])));
		  $first_name =  ucWords(strtolower(empty($_POST['first_name']) ? '' : validate($_POST['first_name'])));
		  $contact_no = empty($_POST['contact_no']) ? '' : validate($_POST['contact_no']);
		  $email = strtolower(empty($_POST['email']) ? '' : validate($_POST['email']));		  
		  $emerg_contact = ucWords(strtolower(empty($_POST['emerg_contact']) ? '' : validate($_POST['emerg_contact'])));
		  $ec_mob1 = empty($_POST['ec_mob1']) ? '' : validate($_POST['ec_mob1']);
		  $ec_mob2 = empty($_POST['ec_mob2']) ? '' : validate($_POST['ec_mob2']);
		 // $dept_code = empty($_POST['dept_code']) ? '' : validate($_POST['dept_code']);
		  $pass=  generateRandomNumber(6);
		  $id=  generateRandomNumber(6);
		  $password= password_hash($pass, PASSWORD_DEFAULT);	
		  $date=date('d M Y');	
		  $reg_date = date("Y-m-d", strtotime($date));
		  $reg_time=date("H:i:s"); 
		  $busy='no';
		  
		 // echo $pass;
		  $counter=0;
		  $counter=substr_count($contact_no,'_');
		  if($counter>0){
			   echo "mobile";
			  exit;
		  }
		  $counter=substr_count($ec_mob1,'_');
		  if($counter>0){
			   echo "mobile";
			  exit;
		  }
		  $counter=substr_count($ec_mob2,'_');
		  if($counter>0){
			   echo "mobile";
			  exit;
		  }
				  
//pic upload start		  
$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
$path = '../pics/'; // upload directory


if(isset($_FILES['picture']))
{
 $img = $_FILES['picture']['name'];
 $tmp = $_FILES['picture']['tmp_name'];

 // get uploaded file's extension
// $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
 $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
 
 // can upload same picture using rand function
 $picture_id = rand(1000,1000000)."-".date('Ymdhms').'.'.$ext;
 if(in_array($ext, $valid_extensions)) 
 {     
  $path = $path.strtolower($picture_id); 
    $picture_id = strtolower($picture_id);
  if(move_uploaded_file($tmp,$path)) 
  {
   echo "<$img src='$path' />";
   $picture_loaded=true;
  }
 } 
 else 
 {
  echo 'invalid_file';
 }
}
//upload end

		//email messages
		$body='Welcome to SmartCleanze '.$last_name.'! Please access your account with User ID: "'.$user_id.'" and Provisional Password: "'.$password;

		$altbody='Welcome to SmartCleanze '.$last_name.'! Please access your account with User ID: "'.$user_id.'" and Provisional Password: "'.$password;
		
		$name=$first_name.' '.$last_name;
		$ee_body=$name." with User ID ".$user_id." registered on SmartCleanze.";
										
		//Check if user exists
		
		$stmt ="SELECT * FROM users WHERE email = '$email' ";
				$res = $pdo->query($stmt);
		 if ($res->fetchColumn() > 0) {
              echo "duplicate";
			  exit();
          } 
		  
			// $_SESSION["temp_id"]=$picture_id; 
			 
		
		 
		  $sql = "INSERT INTO `users`(`user_id`, `email`, `contact_no`, `last_name`, `first_name`, `picture_id`, `emerg_contact`, `ec_mob1`, `ec_mob2`, `auth_level`, `group_code`, `busy`, `password`, `date`, `time`) VALUES	(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([$id,$email,$contact_no,$last_name,$first_name,$picture_id,$emerg_contact,$ec_mob1,$ec_mob2,$auth_level,$group_code,$busy, $password,$reg_date,$reg_time]);
			
			if( $stmt->rowCount()>0)  {
				
							//audit logs 
							 $sql = "INSERT INTO `audit_logs`(`date`, `time`, `user_id`, `contact_no`, `auth_level`, `action`) VALUES	(?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([$reg_date,$reg_time,$user_id,$contact_no,$auth_level, $user_id.' Created User '.$id]); 
									
					//user_id login details to new user
					sendMail($email,$first_name,$user_id,$password,$subject,$body,$altbody);					
										
					// notice of registration
					eagle_wings($name,$ee_body);
								
						 echo "completed";
										exit();		 
						} 
						else{
			  echo "error";
			  exit();
		  }
		
	$pdo=null;	
?>	