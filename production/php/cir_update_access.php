<?php session_start();
if(!isset($_SESSION['auth_level']) and !isset($_SESSION['user_id']))
{
	session_destroy();
	header("Location: ../index.php");
	
}
include 'db_con.php'; 
include 'validate.php'; 
	
	$count_mem=$count_min=0;
	$user=$_SESSION['user_id'];
	$user_id =empty($_POST['user_id']) ? '' : validate($_POST['user_id']);
	//$old_access = $_SESSION['auth_level'];
	$new_access = empty($_POST['auth_level']) ? '' :  validate($_POST['auth_level']);
	$hq_code= '0';
	$ter_code= $_SESSION['ter_code'];
	$are_code= $_SESSION['are_code'];
	$cir_code= $_SESSION['cir_code'];
	$auth_level= 4;
	$date=date('d M Y');	
	$date = date("Y-m-d", strtotime($date));
	$time=date("H:i:s");
		  
	  
	if( isset($_POST['auth_level'])) 
	{
	if( $_POST['auth_level']<>'') 
	{
		
    //Detecting duplicates
	$stmt ="SELECT * FROM members WHERE auth_level = ? AND user_id= ? ";
	$stmt = $pdo->prepare($stmt);
	$stmt->execute([$new_access,$user_id]);
	
	$stmt1 ="SELECT * FROM ministers WHERE auth_level = ? AND user_id= ? ";
	$stmt1 = $pdo->prepare($stmt1);
	$stmt1->execute([$new_access,$user_id]);
	
		  if ($stmt->rowCount() > 0 or $stmt1->rowCount() > 0) {
              echo "duplicate";
			  exit;
          }else
		  
	{

		$sql_update ="UPDATE `members` SET `auth_level`= ? WHERE `user_id`= ? ";
		$stmt2 = $pdo->prepare($sql_update);
		$stmt2->execute([$new_access,$user_id]);
		echo $stmt2->rowCount();
		
		if( $stmt2->rowCount()>0){
			echo "success";
			//audit logs 
			$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
			$stmt= $pdo->prepare($sql);
			$stmt->execute([$hq_code,$ter_code,$are_code,$cir_code,$date,$time,$user_id,$auth_level, $user.' changed Access Level of '.$user_id.' to '.$auth_level]);
			
			echo "success";
			exit;
		}
				
		$sql_update ="UPDATE `ministers` SET `auth_level`= ? WHERE `user_id`= ? ";
		$stmt3 = $pdo->prepare($sql_update);
		$stmt3->execute([$new_access,$user_id]);
		echo $stmt3->rowCount();
		
		if( $stmt3->rowCount()>0){
			//echo "success";
			//audit logs 	
			$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
			$stmt= $pdo->prepare($sql);
			$stmt->execute([$hq_code,$ter_code,$are_code,$cir_code,$date,$time,$user_id,$auth_level, $user.' changed Access Level of '.$user_id.' to '.$auth_level]);
			
			echo "success";
			exit;
		}else{
			echo "error";
		}
			
			
	}
	
	 $pdo=null;
 }
}

		
?>