<?php @session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}
include 'db_con.php';
include 'validate.php';

$from_date='';
$to_date='';

$from_date = empty($_POST['from_date']) ? '' : validate($_POST['from_date']); //echo $from.'<br />';  
$to_date = empty($_POST['to_date']) ? '' : validate($_POST['to_date']);//echo $to.'<br />';
if($from_date==''){
	$from_date='1900-01-01';
}
if($to_date==''){
	$to_date= '3030-12-31';
} 
unset($_SESSION['from_date']);
unset($_SESSION['to_date']);
$_SESSION['from_date']=$from_date;
$_SESSION['to_date']=$to_date;

$json = array();
	
$stmt = $pdo->query("SELECT * FROM users WHERE auth_level='Valeter'")->fetchAll();
foreach ($stmt as $row) 
{
				//echo $row['user_id'] . "\n";
	$valeter_id=$row['user_id'];
	$group_code=$row['group_code'];
	$valeter_name=$row['first_name'].' '.$row['last_name'];
	$jobs = $pdo->query("SELECT COUNT(job_no) FROM  jobs WHERE valeter='$valeter_id' AND costed='Yes' AND date>='$from_date' AND date<='$to_date'")->fetchColumn();
	
	$stmt = $pdo->prepare("SELECT group_name FROM groups WHERE  group_code='$group_code'");
					$stmt->execute([$valeter_id]);
					$group_name=$stmt->fetchColumn();	

	 $bus = array(

		'Details' => '<a href="del_fetch_valeter_job_details.php?id='. $valeter_id . '"class="btn btn-success btn-xs"><i class="fa fa-eye"></i> Details </a>',
		'Valeter ID' => $valeter_id,
		'Valeter Name' => $valeter_name,
        'Jobs Completed' => $jobs,
		'Group Name' => $group_name
    );
    array_push($json, $bus);
}

$jsonstring = json_encode($json);
echo $jsonstring;

//}


?>

