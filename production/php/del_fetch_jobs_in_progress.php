<?php @session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}
include 'db_con.php';
include 'validate.php';

$from_date='';
$to_date='';

$from_date = empty($_POST['from_date']) ? '' : validate($_POST['from_date']); //echo $from.'<br />';  
$to_date = empty($_POST['to_date']) ? '' : validate($_POST['to_date']);//echo $to.'<br />';
if($from_date==''){
	$from_date='1900-01-01';
}
if($to_date==''){
	$to_date= '3030-12-31';
} 
$json = array();
$deal_code=$_SESSION['deal_code'];
$grp_code=$_SESSION['grp_code'];

//$sql = $pdo->query("SELECT * FROM jobs ")->fetchAll();
$sql = $pdo->prepare("SELECT * FROM jobs WHERE assigned='yes' AND completed='no' AND date>='$from_date' AND date<='$to_date' ");
		$sql->execute();
		$data=$sql->fetchAll();
		
foreach ($data as $row) {
	$invoiced_by=$row['costed_by'];
	$sender=$row['sender'];
	$valeter=$row['valeter'];
	$val_by=$row['val_by'];
	
					
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM accounts WHERE user_id = ? ");
				$stmt->execute([$sender]);
				$sender=$stmt->fetchColumn();
				
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$valeter]);
				$valeter=$stmt->fetchColumn();
				
				/*$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$invoiced_by]);
				$invoiced_by=$stmt->fetchColumn();
				
				$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE user_id = ? ");
				$stmt->execute([$val_by]);
				$val_by=$stmt->fetchColumn();*/

	 $bus = array(

		// 'Close' => '<a href="adm_close_validated_jobs.php?id='. $row['job_no'] . '"class="btn btn-primary btn-xs"><i class="fa fa-close"></i> Close </a>',
		'Job No' => $row['job_no'],
		'Sent Date' => $row['date'],
        'Sent Time' => $row['time'],
		'Sender' => $sender,
		'Vehicle/Chasis No' => $row['veh_no'],
		'Make' => $row['make'],
		'Clean Type' => $row['clean_type'],
		'Expected Date' => $row['exp_date'],
		'Expected Time' => $row['exp_time'],
		'Location' => $row['location'],
		'Special Request' => $row['special_req'],
		'Valeter' => $valeter,
		'Start Date' => $row['start_date'],
		'Start Time' => $row['start_time']
    );
    array_push($json, $bus);
	
}

$jsonstring = json_encode($json);
echo $jsonstring;

//}


?>

