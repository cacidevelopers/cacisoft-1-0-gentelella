<?php
session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"admin")
{
	header("Location: " . 'index.php');

}


?>

<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'head.php'; ?>
 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <?php include 'navbar_admin.php'; ?>

            <div class="clearfix"></div>

 <!-- ==========menu profile quick info ===== -->
           <?php include 'profile.php'; ?>
			 <br />
 <!--========== /menu profile quick info ===-->
           

  <!--==========sidebar menu  =========-->
     <?php include 'menu_admins.php'; ?>
         
 <!--==============/sidebar menu======-->
  </div>
 </div>
 <!-- ==========top navigation ======-->
        <?php include 'top_nav.php'; ?>
 <!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>VALIDATE COMPLETED JOBS</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
<?php 
@session_start();
//include 'php/db_con.php';
include 'php/validate.php';
  
  
if(isset($_GET['id'])) 
{
  $job_no = empty($_GET['id']) ? '' :  validate($_GET['id']);
  
}
  $_SESSION['job_no']='';
  $_SESSION['job_no']=$job_no;
  $req_by='';
	
  $data = $pdo->query("SELECT * FROM jobs WHERE job_no='$job_no'");
  $row = $data->fetch();
	

						$stmt = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM clients WHERE email = ? ");
				$stmt->execute([$row["sender"]]);
				$rec=$stmt->fetch();
				if ($stmt->rowCount() > 0){
							$req_by=$rec["name"];
				}
				
				$stmt1 = $pdo->prepare("SELECT CONCAT(first_name,' ', last_name) AS name FROM users WHERE email = ? ");
				$stmt1->execute([$row["sender"]]);
				$rec=$stmt1->fetch();
				if ($stmt1->rowCount() > 0){
							$req_by=$rec["name"];
				}
						
						/*$dept=$row['department'];
						$datum2 = $pdo->query("SELECT name FROM departments WHERE code='$dept'");
					    $rec2 = $datum2->fetch();
						$dep_name=$rec2["name"];*/
?>

				<style>
						tag { font-weight: bold; color:blue;}
					</style>
					
					<style>
						errortag { font-weight: bold; color:red;}
					</style>
				 <table  class='table table-striped table-bordered dt-responsive nowrap' cellspacing='0' width='100%'>
					  
						<tr>  
						   <td><tag>Job No: </td><td><?php echo ' '.$job_no;?></tag></td></tr>
						   <tr><td><tag>Date Posted: </td><td><?php echo $row['date'];?></tag></td></tr>
						  <tr> <td><tag>Time Posted:</td><td><?php echo $row['time'];?></tag></td></tr>
						  <tr> <td><tag>Posted By:</td><td><?php echo $req_by;?></tag></td></tr>
						  <tr> <td><tag>Vehicle Make:</td><td><?php echo $row['make'];?></tag></td></tr>
						  <tr> <td><tag>Vehicle No:</td><td><?php echo $row['veh_no'];?></tag></td></tr>
						  <tr> <td><tag>Type of Clean:</td><td><?php echo $row['clean_type'];?></tag></td></tr>
						  <tr> <td><tag>Expected Date:</td><td><?php echo $row['exp_date'];?></tag></td></tr>
						  <tr> <td><tag>Expected Time:</td><td><?php echo $row['exp_time'];?></tag></td></tr>
						  <tr> <td><tag>Started Date:</td><td><?php echo $row['start_date'];?></tag></td></tr>
						  <tr> <td><tag>Started Time:</td><td><?php echo $row['start_time'];?></tag></td></tr>
						  <tr> <td><tag>Completed Date:</td><td><?php echo $row['comp_date'];?></tag></td></tr>
						  <tr> <td><tag>Completed Time:</td><td><?php echo $row['comp_time'];?></tag></td></tr>
						  <tr> <td><tag>Location:</td><td><?php echo $row['location'];?></tag></td></tr>
						  <tr> <td><tag>Special Request:</td><td><?php echo $row['special_req'];?></tag></td></tr>
						</tr>
					  </table>
					<br /><br />
					  
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
						 <input id="reply" type="submit" class="btn btn-success btn-md" style="width:100%" value="Validate Job "/>
                        <!--button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button-->
						<button class="btn btn-success btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='adm_view_completed_jobs.php' "> Cancel</button>
					 </div>
                   </div>
					
                 
				  
				  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The Job was successfully Validated!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-warning" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-warning">Validate Another Job</button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The system could not Validate the Job!</p>
                            <p>Please try again or contact Help Desk</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-warning" data-dismiss="modal">Cancel!</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				  
				 
					  
					  <div id= "err" > </div>
					 
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

    
    <script>
$(document).ready(function (e) {
	
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
  $.ajax({
   url: "php/update_validated_jobs.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    $("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    
     if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
                }
				if(data.match(/duplicate/gi)){
					// alert("Member is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
				
				if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    //$("#err").html(e).fadeIn();
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					 window.location.replace("adm_view_completed_jobs.php");
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("adm_view_completed_jobs.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
						 window.location.replace("adm_view_completed_jobs.php");
				});
				
				//Duplicate  
				$("#dup_add").click(function(){
					location.reload();
				});
				$("#dup_cancel").click(function(){
					 window.location.replace("adm_view_completed_jobs.php");	
			});
	
 })
 )
 
 
 
 
});
    </script>

  <?php include 'timeout.php'; ?>	
    
  </body>
</html>
