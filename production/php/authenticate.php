<?php @session_start();

	include 'db_con.php'; 
	//include 'online_status.php'; 
	include 'validate.php'; 

$customer_code=$contact_no=$type=$user=$client='';
	
 
$user_id = empty($_POST['user_id']) ? '' : validate($_POST['user_id']);
$password =empty($_POST['password']) ? '' : validate($_POST['password']);
 $date=date('d M Y');	
 $date = date("Y-m-d", strtotime($date));
 $time=date("H:i:s");	 
	 
	
				//if (online_status()){	
				
				$stmt = $pdo->prepare("SELECT password FROM ministers WHERE user_id = ? ");
				$stmt->execute([$user_id]);
				$min_id=$stmt->fetchColumn();
				
				$sql = $pdo->prepare("SELECT password FROM members WHERE user_id = ? ");
				$sql->execute([$user_id]);
				$mem_id=$sql->fetchColumn();
								
				if(password_verify($password,$min_id)) {
				
					$stmt = $pdo->query("SELECT * FROM ministers WHERE user_id='$user_id'");
					$row = $stmt->fetch();
																
										$_SESSION["user_id"]=$row["user_id"];
										$_SESSION["auth_level"]=$row["auth_level"];
										$_SESSION["picture_id"]=$row["picture_id"];
										$_SESSION["first_name"]=$row["first_name"];
										$_SESSION["middle_names"]=$row["middle_names"];
										$_SESSION["last_name"]=$row["last_name"];
										$_SESSION["title"]=$row["title"];
										$_SESSION["ter_code"]=$row["ter_code"];
										$_SESSION["are_code"]=$row["are_code"];
										$_SESSION["cir_code"]=$row["cir_code"];
									//	$_SESSION["loc_code"]=$row["loc_code"];
										
									
									$auth_level=$row["auth_level"];
									
										$sql = $pdo->prepare("SELECT ter_name FROM territories WHERE ter_code = ? ");
										$sql->execute([$_SESSION["ter_code"]]);
										$_SESSION["ter_name"]=$sql->fetchColumn();
										
										$sql = $pdo->prepare("SELECT are_name FROM areas WHERE are_code = ? ");
										$sql->execute([$_SESSION["are_code"]]);
										$_SESSION["are_name"]=$sql->fetchColumn();
										
										$sql = $pdo->prepare("SELECT cir_name FROM circuits WHERE cir_code = ? ");
										$sql->execute([$_SESSION["cir_code"]]);
										$_SESSION["cir_name"]=$sql->fetchColumn();
										
										
										/*$sql = $pdo->prepare("SELECT name FROM locals WHERE loc_code = ? ");
										$sql->execute([$_SESSION["loc_code"]]);
										$_SESSION["local_name"]=$sql->fetchColumn();*/
									
										
								/*echo $email.'<br />';
								echo $auth_level.'<br />';
								echo $contact_no.'<br />';*/
									if($_SESSION["auth_level"]=='exe'){
										
											$_SESSION["zone"]='
											<table cellpadding="10" >
														<tr><td>Executive Portal</td></tr> 
														
												</table>'; 
																				
											}
											elseif($_SESSION["auth_level"]=='hrm'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ:   </td>'.'<td style="text-align: left;"> Human Resource Department</td></tr>'.
													' <tr><td> TERRITORY: </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA:  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT:  </td>'.'<td style="text-align: left;"> '.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='ict'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ  : </td>'.'<td style="text-align: left;"> IT Department</td></tr>'.
													' <tr><td> TERRITORY  : </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  : </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='adm'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ  :  </td>'.'<td style="text-align: left;"> Administration Department</td></tr>'.
													' <tr><td> TERRITORY  : </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='acc'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ  :  </td>'.'<td style="text-align: left;"> Accounts Department</td></tr>'.
													' <tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='sup'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ  :  </td>'.'<td style="text-align: left;"> Systems Administrator</td></tr>'.
													' <tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='ter'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td> TERRITORY  :  </td>'.'<td>'.$_SESSION["ter_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='are'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>
													  <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='cir'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>
													  <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>
													  <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											else{
												$_SESSION["zone"]='
												<table>
													<tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
									
								
									//echo $email.'<br />';
									//echo $auth_level.'<br />';
									//echo $contact_no.'<br />';
								
								
										$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([0,$_SESSION["ter_code"],$_SESSION["are_code"],$_SESSION["cir_code"],$date,$time,$user_id,$auth_level, 'Logged In']);
																	
					echo $_SESSION["auth_level"];	
						//exit();
		
					}
				
					elseif(password_verify($password,$mem_id)) {
					
					
									$stmt = $pdo->query("SELECT * FROM members WHERE user_id='$user_id'");
									$row = $stmt->fetch();
																
										$_SESSION["user_id"]=$row["user_id"];
										$_SESSION["auth_level"]=$row["auth_level"];
										$_SESSION["picture_id"]=$row["picture_id"];
										$_SESSION["first_name"]=$row["first_name"];
										$_SESSION["middle_names"]=$row["middle_names"];
										$_SESSION["last_name"]=$row["last_name"];
										$_SESSION["title"]=$row["title"];	
										$_SESSION["ter_code"]=$row["ter_code"];
										$_SESSION["are_code"]=$row["are_code"];
										$_SESSION["cir_code"]=$row["cir_code"];
										//$_SESSION["loc_code"]=$row["loc_code"];										
									
										$auth_level=$row["auth_level"];
										
										$sql = $pdo->prepare("SELECT ter_name FROM territories WHERE ter_code = ? ");
										$sql->execute([$_SESSION["ter_code"]]);
										$_SESSION["ter_name"]=$sql->fetchColumn();
										
										$sql = $pdo->prepare("SELECT are_name FROM areas WHERE are_code = ? ");
										$sql->execute([$_SESSION["are_code"]]);
										$_SESSION["are_name"]=$sql->fetchColumn();
										
										$sql = $pdo->prepare("SELECT cir_name FROM circuits WHERE cir_code = ? ");
										$sql->execute([$_SESSION["cir_code"]]);
										$_SESSION["cir_name"]=$sql->fetchColumn();
										
										/*$sql = $pdo->prepare("SELECT name FROM locals WHERE loc_code = ? ");
										$sql->execute([$_SESSION["loc_code"]]);
										$_SESSION["local_name"]=$sql->fetchColumn();*/
										
												if($_SESSION["auth_level"]=='exe'){
										
											$_SESSION["zone"]='
											<table cellpadding="10" >
														<tr><td>Executive Portal</td></tr> 
														
												</table>'; 
																				
											}
											elseif($_SESSION["auth_level"]=='hrm'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ:   </td>'.'<td style="text-align: left;"> Human Resource Department</td></tr>'.
													' <tr><td> TERRITORY: </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA:  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT:  </td>'.'<td style="text-align: left;"> '.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='adm'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ  : </td>'.'<td style="text-align: left;"> Administration Department</td></tr>'.
													' <tr><td> TERRITORY  : </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  : </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='ict'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ  :  </td>'.'<td style="text-align: left;"> IT Department</td></tr>'.
													' <tr><td> TERRITORY  : </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='acc'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ  :  </td>'.'<td style="text-align: left;"> Accounts Department</td></tr>'.
													' <tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='sup'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td>HQ  :  </td>'.'<td style="text-align: left;"> Systems Administrator</td></tr>'.
													' <tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='ter'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td> TERRITORY  :  </td>'.'<td>'.$_SESSION["ter_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='are'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>
													  <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>
												</table>';
											}
											elseif($_SESSION["auth_level"]=='cir'){
										
												$_SESSION["zone"]='
												<table>
													  <tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>
													  <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>
													  <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
											else{
												$_SESSION["zone"]='
												<table>
													<tr><td> TERRITORY  :  </td>'.'<td style="text-align: left;">'.$_SESSION["ter_name"].'</td></tr>'.
													' <tr><td> AREA  :  </td>'.'<td style="text-align: left;">'.$_SESSION["are_name"].'</td></tr>'.
													' <tr><td> CIRCUIT  :  </td>'.'<td style="text-align: left;">'.$_SESSION["cir_name"].'</td></tr>
												</table>';
											}
									
								
									//echo $email.'<br />';
									//echo $auth_level.'<br />';
									//echo $contact_no.'<br />';
								
										$sql = "INSERT INTO `audit_logs`(`hq_code`, `ter_code`, `are_code`, `cir_code`, `date`, `time`, `user_id`, `auth_level`,`action`) VALUES	(?,?,?,?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([0,$_SESSION["ter_code"],$_SESSION["are_code"],$_SESSION["cir_code"],$date,$time,$user_id,$auth_level, 'Logged In']);
										
					echo $_SESSION["auth_level"];	
						//exit();		
							//echo $_SESSION["zone"];
						}
										
					
					else  {
							echo 'denied';
										
					}
				//}
				
			/*	else {
					echo "offline";
				}*/
			
	$pdo=null;		
?>
	
