<?php @session_start();
if(!isset($_SESSION['user_id']) )
{
	session_destroy();
	header("Location: ../index.php");
	
}

		include 'db_con.php'; 
		include 'validate.php';
	
		  //retriving data
		$job_no =$_SESSION['job_no'];
		$user_id =$_SESSION['user_id'];
		$auth_level ='';
		$cost = empty($_POST['cost']) ? '' : validate($_POST['cost']);
		$fee = empty($_POST['fee']) ? '' : validate($_POST['fee']);
		$mat_deductions = empty($_POST['mat_deductions']) ? '' : validate($_POST['mat_deductions']);
		$ins_deductions = empty($_POST['ins_deductions']) ? '' : validate($_POST['ins_deductions']);
		$clo_deductions = empty($_POST['clo_deductions']) ? '' : validate($_POST['clo_deductions']);
		$ins_ex_deductions = empty($_POST['ins_ex_deductions']) ? '' : validate($_POST['ins_ex_deductions']);
		$oth_deductions = empty($_POST['oth_deductions']) ? '' : validate($_POST['oth_deductions']);
		
		$date=date('d M Y');	
		$date = date("Y-m-d", strtotime($date));
		$time=date("H:i:s");	   
	
		
					
		 $sql = "UPDATE `jobs` SET `costed`=?,`cost_date`=?,`cost_time`=?,`costed_by`=?,`cost`=?,`fee`=?, `mat_deductions`=?, `ins_deductions`=?, `ins_ex_deductions`=?,`clo_deductions`=?,`oth_deductions`=? WHERE `job_no`=?";
		
		// Prepare statement
		$stmt = $pdo->prepare($sql);
		// execute the query
		$stmt->execute(['Yes',$date,$time,$user_id,$cost,$fee,$mat_deductions,$ins_deductions,$ins_ex_deductions,$clo_deductions,$oth_deductions,$job_no]);


		if ($stmt->rowCount() > 0 ) {
	
				
 //audit logs 
				$sql = "INSERT INTO `audit_logs`( `date`, `time`, `user_id`, `contact_no`, `auth_level`, `action`) VALUES	(?,?,?,?,?,?)";
										$stmt= $pdo->prepare($sql);
										$stmt->execute([$date,$time,$user_id,'',$auth_level, 'Costed Job No. '.$job_no]);
						 
				echo "success";
			}	
		
            
			else{
				
				echo "error";
				//print_r($pdo->errorInfo());
			}
//}
		$pdo=null;	
	//}
		
?>