<?php
session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"adm")
{
	header("Location: " . 'index.php');	
}

include 'php/load_territories.php';

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <?php include 'adm_navbar.php'; ?>

            <div class="clearfix"></div>

 <!-- ==========menu profile quick info ===== -->
           <?php include 'profile.php'; ?>
			 <br />
 <!--========== /menu profile quick info ===-->
           

  <!--==========sidebar menu  =========-->
     <?php include 'adm_menu.php'; ?>
         
 <!--==============/sidebar menu======-->
  </div>
 </div>
 <!-- ==========top navigation ======-->
        <?php include 'top_nav.php'; ?>
 <!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>Add New Area</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
					<div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ter_code">Territory<span style="color:red;">*</span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select class="form-control has-feedback-left has-feedback-left" required="required" name="ter_code" id="ter_code" autofocus>
                            <option value="">Select Territory/Missionary Area</option>
							<?php echo $territories; ?> 
                           </select>
						   <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
					 </div>
					<div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="are_code" >Area Code <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="are_code" required="required" class="form-control has-feedback-left col-md-7 col-xs-12" name="are_code"  data-inputmask="'mask' : '99'" placeholder="Enter two digit Area code "  type="text" >
						  <span class="fa fa-building form-control-feedback left" aria-hidden="true" ></span>
                        
                        </div>
                      </div>
					  									 
					  <div class="item form-group">
                        <label class="control-label col-md- col-sm-3 col-xs-12" for="are_name" >Area Name <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="are_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="are_name" placeholder="30 letters max"  type="text"  required="required" maxlength="30"  >
						  <span class="fa fa-home form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>                 					  
					  			  
					  					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="town_name">Name of Town</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="town_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="town_name" placeholder="Territorial Head Town name" type="text" maxlength="30">
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>                    

					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="area_name">Name of Area</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="area_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="area_name" placeholder="Territorial Head Area name" type="text"  maxlength="30">
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					   	
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street_name">Street Name</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="street_name" class="form-control has-feedback-left col-md-7 col-xs-12" name="street_name" placeholder="30 letters max" type="text"  maxlength="30">
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hse_no">House Address</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="hse_no" class="form-control has-feedback-left col-md-7 col-xs-12" name="hse_no" placeholder="15 letters max" type="text"  maxlength="15">
						  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="postal_address">Postal Address</label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="postal_address" class="form-control has-feedback-left col-md-7 col-xs-12" name="postal_address" placeholder="60 letters max" type="text" maxlength="60">
						  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>					  
					 <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Number I</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile" 
						  placeholder="Use Format 233XXXXXXXXX" type="tel" />						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Number II</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="mobile2" class="form-control has-feedback-left col-md-7 col-xs-12" name="mobile2" placeholder="Use Format: 233xxxxxxxxx" type="tel" />				 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address 
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="email" class="form-control has-feedback-left col-md-7 col-xs-12" name="email" placeholder="30 letters max" type="email"  maxlength="30">
						   <span class="fa fa-at form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_founded">Date Founded
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="date_founded" class="form-control has-feedback-left col-md-7 col-xs-12" name="date_founded" placeholder="MM/DD/YYYY" type="date">
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>                        
                        </div>
                      </div>
					   
                    
                     
                  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Area Details Successfully Added.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-warning" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-warning">Add New Area</button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Area details was not saved, Please contact Help Desk</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-warning" data-dismiss="modal">Cancel!</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "duplicate" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >DUPLICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Area details have been captured already!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="dup_cancel" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-warning">Add New Area</button>
                       
                        </div>

                      </div>
                    </div>
                  </div>
				   
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
						 <input id="add_area" type="submit" style="width:100%" class="btn btn-primary btn-md" value="Add"/>
						 <button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
					  <button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_adm.php' "> Cancel</button>
						
						
					 
					    </div>
                      </div>
					  
					  <div id= "err" > </div>
					 
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>
    <script>
$(document).ready(function (e) {
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
  
  $.ajax({
   url: "php/save_area.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    $("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    
     if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
                }
				if(data.match(/duplicate/gi)){
					// alert("Member is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
				
				if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    $("#err").html(e).fadeIn();
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				$("#dup_cancel").click(function(){
					 window.location.replace("index_adm.php");	
											
				});
				
	
 })
 )
});
    </script>
	 <?php include 'timeout.php'; ?>
	 <!-- jquery.inputmask -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
    </script>
    <!-- /jquery.inputmask -->  
    
  </body>
</html>
