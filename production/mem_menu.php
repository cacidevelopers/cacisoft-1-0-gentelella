    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <br/><br/><br/><br/>
                <ul class="nav side-menu">
                 
				 <li><a class="ajax-link" href="index_mem.php"><i class="fa fa-tachometer"></i><span> Dashboard</span></a>
                 </li>
				 <li><a><i class="fa fa-institution"></i>My Church @ a Glance<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>International<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                           <li><a href="#mem_view_nat_summaries.php">CACI at a Glance</a>
                            </li>
                          </ul>
                        </li>
						
                        <li><a>Territories<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="mem_view_ter_summaries.php">View Territorial Summaries</a>
                            </li>
                          </ul>
                        </li>
						
						 <li><a>Areas<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <!--li><a href="mem_view_circuits.php">View Circuits</a-->
                            <li><a href="mem_view_are_summaries.php">View Area Summaries</a>
                            </li>							
                          </ul>
                        </li>
						
						 <li><a>Circuits<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
							<li><a href="#mem_view_my_cir_summaries.php">My Circuit</a></li>
							<li><a href="mem_view_cir_summaries.php">View All Circuits</a>
                            </li>			
                          </ul>
                        </li> 
						
						<!--li><a>Locals<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
							<li><a href="#mem_view_my_society_summaries.php">My Local</a></li>
							<li><a href="#mem_view_soc_summaries.php">View All Locals</a>
                            </li>			
                          </ul>
                        </li--> 
						
						 <li><a>Family Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
							<li><a href="#mem_view_bc_summaries.php">View Family Groups</a>
							</li>
							<!--li><a href="mem_view_bc_zone.php">Leaders Zone</a>
                            </li-->			
                          </ul>
                        </li> 
						
						<li><a>Auxilliary Groups<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
							<li><a href="#mem_view_org_summaries.php">View Auxilliary Groups</a></li>
									
                          </ul>
                        </li>
												
                    </ul>
                  </li>
				  
				   <li><a class="ajax-link" href="mem_view_mem_profile.php"><i class="fa fa-user"></i><span>My Profile</span></a>
                 </li>
				 
				   <li><a><i class="fa fa-book"></i>Sermons<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <!--li><a href="#mem_auth_sermon_upload.php">Add New Sermon</a></li-->
                     <li><a href="#mem_view_sermons.php">View Sermons</a></li>
					  <!--li><a href="#delete_sermons.php">Delete My Sermons</a></li-->
                    </ul>
                  </li>
				  
				 				  
				  <li><a class="ajax-link" href="#mem_fin_reports.php"><i class="fa fa-money"></i><span>My Contributions</span></a>
                 </li>
				
				   <li><a><i class="fa fa-flag"></i>My Requests<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Transfers<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
							<li><a href="#mem_new_transfers.php">New Requests</a></li>
							  <li><a href="#mem_edit_my_transfers.php">Edit Requests</a></li>
							  <li><a href="#mem_view_my_transfer_requests.php">View Requests</a></li>
							  <li><a href="#mem_view_my_approved_transfer_request.php">Approved Requests</a></li>
							  <li><a href="#mem_view_my_transfer_history.php">View Transfer History</a></li>
                          </ul>
                        </li>
						<li><a>Burials<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <!--li><a href="mem_unprocessed_transfer_requests.php">View Pending Requests</a></li-->
							  <li><a href="#mem_new_burial_requests.php">New Requests</a></li>
							  <li><a href="#mem_edit_my_burial_requests.php">Edit Requests</a></li>
							  <li><a href="#mem_view_my_burial_request_history.php">View Requests History</a></li>
							  
                          </ul>
                        </li>
                        <li><a>Other Requests<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
								<li><a href="#mem_new_requests.php">New Requests</a></li>
								<li><a href="#mem_edit_my_requests.php">Edit Requests</a></li>
								<li><a href="#mem_view_pending_requests.php">Pending Requests</a></li>
								<li><a href="#mem_view_processed_requests.php">Processed Requests</a></li>
								
                          </ul>
                        </li>
                    </ul>
                  </li> 
				
				  <!--li><a><i class="fa fa-bell"></i>Meetings<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
					
					 <li><a href="#soc_view_meetings.php">View Meetings</a></li>
					
                    </ul>
                  </li-->
				  
				<!--li><a><i class="fa fa-calendar"></i>Calendar<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#conn/con_almanac.php">Almanac</a></li>
                      <li><a href="#conn/con_lectionary.php">Lectionary</a></li>
                      <li><a href="#soc/soc_programs.php">Society Programs</a></li>
					  <li><a href="#soc/org_programs.php">Organizational Programs</a></li>
                      </ul>
                  </li-->
                  
				 <!--li><a><i class="fa fa-bar-chart"></i>Reports<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
							 <li><a href="#mem_view_reports.php">View Reports</a></li>
                    </ul>
                  </li-->
				  
				   <li><a><i class="fa fa-briefcase"></i> Appointments<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a>Ministerial Appointments<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
						   <li><a href="#mem_view_min_appointments.php">View Appointments</a>
                            </li>
                          </ul>
                        </li>
                        <li><a>Lay Appointments<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li><a href="#mem_view_appointments.php">View Appointments</a>
                            </li>
                          </ul>
                        </li>
                    </ul>
                  </li> 
                  
                </ul>
              </div>
            </div>