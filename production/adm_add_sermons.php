<?php
@session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"adm")
{
	header("Location: " . 'index.php');
	
}


?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'adm_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'adm_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
 <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>ADD SERMONS</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">


				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="am">
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="file">Select File<span style="color:red;">*</span>
                        </label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="file" class="form-control col-md-7 col-xs-12 has-feedback-left" name="file" type="file" accept="application/msword,  application/vnd.ms-powerpoint, application/vnd.ms-excel, application/pdf" required="required" >
						<!--application/pdf-->
						 <span class="fa fa-folder form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Sermon Title<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="title" class="form-control has-feedback-left col-md-7 col-xs-12" name="title" placeholder="Title of the Sermon" type="text" required="required" >
						  <span class="fa fa-star form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div> 
					  
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="author">Author Name
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="author" class="form-control has-feedback-left col-md-7 col-xs-12" name="author" placeholder="Sermon written by" type="text" >
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
                    
                   <!--MODALS START-->   
                  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The Sermon was Successfully uploaded.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-primary" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-primary">Upload Another Sermon</button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SERMON UPLOAD ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>The Sermon was not saved, please try again or contact Help Desk</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-primary" data-dismiss="modal">Cancel!</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "duplicate" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >DUPLICATE SERMON UPLOAD</h4>
                        </div>
                        <div class="modal-body" id="confirm">
                          <p>This Sermon has already been uploaded!</p> 
                          
                        </div>
                        <div class="modal-footer">
						  
                          <button type="button" id="dup_cancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-primary" data-dismiss="modal">Upload New Sermon</button>
                       
                        </div>

                      </div>
                    </div>
                  </div> 
				  
				  <div class="modal fade bs-example-modal-sm" id= "invalid_file" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >INVALID FILE</h4>
                        </div>
                        <div class="modal-body" id="confirm">
                          <p>Please upload one of the following file types: .pdf, .docx, .doc, .xls, .pptx</p> 
                          
                        </div>
                        <div class="modal-footer">
						  
                          <button type="button" id="invalid_cancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "invalid_add" class="btn btn-primary" data-dismiss="modal">Upload New Sermon</button>
                       
                        </div>

                      </div>
                    </div>
                  </div>
	   <!--MODALS END-->
				   
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
					 <input id="upload" type="submit" style="width:100%" class="btn btn-primary btn-md" value="Upload Sermon"/>
					  <button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_adm.php' "> Cancel</button>
						
					    </div>
                      </div>
					  
					  <div id= "err" > </div>
					 
                    </form>
							
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
     
$(document).ready(function (e) {
 $("#am").on('submit',(function(e) {
  e.preventDefault();
  
  $.ajax({
   url: "php/save_sermons.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   /*beforeSend : function()
   {
    $("#preview").fadeOut();
    $("#err").fadeOut();
   },*/
   success: function(data)
      {
    
				
				if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
				
				
                }
				else if(data.match(/duplicate/gi)){
					// alert("Member is already registered!");
					 $("#duplicate").modal({backdrop: true});
				}
				
				else if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}
				else if(data.match(/invalid_file/gi)){
					// alert("PHP error");
					$("#invalid_file").modal({backdrop: true});
				}
   
      },
     
    error: function(e) 
      {
		//  alert ("ajax error");
    //$("#err").html(e).fadeIn();
	$("#sys_error").modal({backdrop: true});
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//Invalid File
				$("#invalid_cancel").click(function(){
					 window.location.replace("index_adm.php");	
				});
				$("#invalid_add").click(function(){
					location.reload();
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					location.reload();
				});
				$("#dup_cancel").click(function(){
					location.reload();	
			});
 })
 )
});
    </script>
 <?php include 'timeout.php'; ?>	
    
  </body>
</html>
