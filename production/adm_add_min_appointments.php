<?php @session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"adm")
{
	header("Location: " . 'index.php');
	
}
include 'php/fill_fam_groups.php';
include 'php/adm_fill_min_statuses.php';
include 'php/fill_aux_groups.php';
include 'php/fill_territories.php';

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'adm_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'adm_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>ADD NEW APPOINTMENT</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
          <form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss">
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="min_id">Minister's Reg. No.<span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="min_id" class="form-control has-feedback-left col-md-7 col-xs-12" name="min_id" placeholder="Minister's ID" type="text" required="required" maxlength="9" autofocus>
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div>
					   <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">
                          Appointed As<span style="color:red;">*</span>
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  required class="form-control has-feedback-left" id="status" name="status">
						    <option value="">Select</option>
							<?php echo $statuses; ?>  
                          </select>
						  <span class="fa fa-briefcase form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					 
					  <div class="item form-group" >
                      <center>  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="at">
                         At:
                        </label></center>
                      </div>
					
					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hq_code">National <span style="color:red;">*</span>
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="hq_code" class="form-control has-feedback-left col-md-7 col-xs-12" name="hq_code" placeholder="National" type="text" value='CACI' maxlength="30" readonly="readonly">
						  <span class="fa fa-user form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div>
					  
					    <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ter_code">
                          Territory
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select   class="form-control has-feedback-left" id="ter_code" name="ter_code">
						    <option value="">If Applicable</option>
							<?php echo $territories; ?>  
						</select>
						  <span class="fa fa-institution form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>

					  <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="are_code">Area</label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select   class="form-control has-feedback-left" id="are_code" name="are_code">
							 <option value="">If Applicable</option>
						</select>
						  <span class="fa fa-building form-control-feedback left" aria-hidden="true" required></span>
           </div> 
          </div>
					  
					 <div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cir_code">Circuit</label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" id="cir_code" name="cir_code">
						    <option value="">If Applicable</option>
							
						</select>
						  <span class="fa fa-home form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div>
					  
					   <!--div class="item form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="org_code">
                         Organization Name
                        </label>
						<div class="col-md-4 col-sm-4 col-xs-12">
                          <select  class="form-control has-feedback-left" id="org_code" name="org_code">
						    <option value="">If Applicable</option>
							
						</select>
						  <span class="fa fa-bars form-control-feedback left" aria-hidden="true" required></span>
                        </div> 
                      </div-->
					  
					  
					  
					  <!--div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="where_baptised">Please specify*
                        </label>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="employer" class="form-control has-feedback-left col-md-7 col-xs-12" name="employer" placeholder="30 letters max" type="text" maxlength="30" >
						   <span class="fa fa-edit form-control-feedback left" aria-hidden="true" required></span>
                        
                        </div>
                      </div-->
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="from_date">Effective Date<span style="color:red;">*</span></label>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="from_date" class="form-control has-feedback-left col-md-7 col-xs-12" name="from_date" placeholder="MM/DD/YYYY" type="date" required='required'>
						  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true" required></span>
                        </div>
                      </div> 
                    
                  <!--MODALS START-->   
                  <div class="modal fade bs-example-modal-sm" id= "success" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SUCCESS!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Appointment Details Successfully Saved.</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="success_fin" class="btn btn-primary" data-dismiss="modal">Finish</button>
                          <button type="button" id= "success_add" class="btn btn-primary">Add New Appointment</button>
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "sys_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >SYSTEM ERROR!</h4>
                        </div>
                        <div class="modal-body">
                             <p>The appointment details failed to save.</p>
                            <p>Please try again or contact support</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="sys_cancel" class="btn btn-primary" data-dismiss="modal">Cancel!</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  
				   <div class="modal fade bs-example-modal-sm" id= "position" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" >CONFIRMATION REQUIRED</h4>
                        </div>
                        <div class="modal-body" id="confirm">
                           
                          
                        </div>
                        <div class="modal-footer">
						  <button type="button" id="edit" class="btn btn-primary" data-dismiss="modal">Edit</button>
                          <button type="button" id="dup_cancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          <button type="button" id= "dup_add" class="btn btn-primary" data-dismiss="modal">Save New Appointment</button>
                       
                        </div>

                      </div>
                    </div>
                  </div>
				   <!--MODALS END-->
				   
				   <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-3">
					 <input id="save_appointment" type="submit" style="width:100%" class="btn btn-primary btn-md" value="Save Appointment"/>
					  <button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button>
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='index_connexional.php' "> Cancel</button>
						
					    </div>
                      </div>
					  
					  <div id= "err" > </div>
					 
                    </form>
				  
				  <!----FORM END--->
				  
				  
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
 $(document).ready(function(){  
      $('#status').change(function(){  
           var status = $(this).val();  
		   console.log(status);
       });  
	  
	  $('#ter_code').change(function(){  
           var ter_code = $(this).val();  
		   console.log(ter_code);
           $.ajax({  
                url:"php/load_areas.php",  
                method:"POST",  
                data:{ter_code:ter_code}, 
				 dataType:"text",
                success:function(data){  
				// $('#show_product').html("");  
				console.log(data);
                     $('#are_code').html(data);  
                }  
	   });  
      });

      $('#are_code').change(function(){  
           var are_code = $(this).val();  
		   console.log(are_code);
           $.ajax({  
                url:"php/load_circuits.php",  
                method:"POST",  
                data:{are_code:are_code}, 
				 dataType:"text",
                success:function(data){  
				// $('#show_product').html("");  
				console.log(data);
                     $('#cir_code').html(data);  
                }  
	   });  
      });
	  
 });

 </script>     
    <script>
$(document).ready(function (e) {
 $("#ss").on('submit',(function(e) {
  e.preventDefault();
    $("#fam_code").removeAttr('disabled');
	 $("#aux_code").removeAttr('disabled');
  $.ajax({
   url: "php/adm_confirm_min_appointments.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function()
   {
    $("#preview").fadeOut();
    $("#err").fadeOut();
   },
   success: function(data)
      {
    
     if(data.match(/success/gi)){
				// alert("success");
				$("#success").modal({backdrop: true});
                }
				if(data.match(/Position/gi)){
					 $("#confirm").html(data);
					 $("#position").modal({backdrop: true});
				}
				
				if(data.match(/error/gi)){
					// alert("PHP error");
					$("#sys_error").modal({backdrop: true});
				}

   
      },
     
    error: function(e) 
      {
		  alert ("ajax error");
    $("#err").html(e).fadeIn();
      } 
      
    });
	
				//action buttons
				//success transaction
				$("#success_add").click(function(){
					location.reload();
				});
				
				$("#success_fin").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//System Error
				$("#sys_cancel").click(function(){
					 window.location.replace("index_adm.php");	
				});
				
				//Duplicate
				$("#dup_add").click(function(){
					save_appointments_data();
				});
				$("#dup_cancel").click(function(){
					 window.location.replace("index_adm.php");	
											
				});
        
				function save_appointments_data(){
					 $.ajax({
								   url: "php/adm_save_min_appointments.php",
								   type: "POST",
								   data:  new FormData(this),
								   contentType: false,
										 cache: false,
								   processData:false,
								   
								   success: function(data)
									  {
									
									 if(data.match(/success/gi)){
												// alert("success");
												$("#success").modal({backdrop: true});
												}
												if(data.match(/error/gi)){
													// alert("PHP error");
													$("#sys_error").modal({backdrop: true});
												}

								   
									  },
									 
									error: function(e) 
									  {
										  alert ("ajax error");
									$("#err").html(e).fadeIn();
									  } 
									  
									});
													
				}
				
	
 })
 )

});
    </script>
	<?php include 'timeout.php'; ?>	
    
  </body>
</html>
