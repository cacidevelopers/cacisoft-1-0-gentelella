<?php
session_start();
/*
if((isset($_SESSION['user_id']) && strpos($_SESSION['user_id'], 'c') !== false) OR (isset($_SESSION['user_id']) && strpos($_SESSION['user_id'], 'C') !== false)  OR (isset($_SESSION['user_id']) && $_SESSION['local_preacher']== 'local_preacher'))*/
if((isset($_SESSION['user_id']) && strpos($_SESSION['user_id'], 'c') !== false) OR (isset($_SESSION['user_id']) && strpos($_SESSION['user_id'], 'C') !== false)  )
{
	//$user_id=$_SESSION['mem_id'];
	//if (strpos($user_id, 'mcg') !== false) {
   // echo 'true';
  // $access='granted';
	header("Location: " . 'adm_add_sermons.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'adm_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'adm_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->

 <!--========== page content =======-->
 <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="">
              <div class="page-title">
              <div class="title_right">
                <h4>UPLOAD SERMONS</h4><br/>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

				  <!---FORM HERE--->
				   
				<form class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id="ss" >
					  
				    				  
				   <div class="modal fade bs-example-modal-sm" id= "auth_error" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
                          </button>
                          <h4 class="modal-title" >AUTHENTICATION ERROR!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Sorry, you are not authorized to upload Sermons!</p>
                          
                        </div>
                        <div class="modal-footer">
                          <button type="button" id="auth_cancel" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                          
                        </div>

                      </div>
                    </div>
                  </div>
				  				 
				  <br><br><br>
				   
				  
                    </form>
				  
				  <!----FORM END--->
                  </div>
                </div>
              </div>
            </div>
          </div>
		</div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
	
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
  
$(document).ready(function (e) {
	$("#auth_error").modal({backdrop: true});
	
	$("#auth_cancel").click(function(){
		 window.location.replace("index_adm.php");
	});
});
    </script>  
  <?php include 'timeout.php'; ?>  
    
  </body>
</html>
