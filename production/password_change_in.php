<?php
session_start();
if(!isset($_SESSION['auth_level']) and !isset($_SESSION['user_id']))
{
	header("Location: " . 'index.php');
	
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
 
    <meta http-equiv="Content-Type" content="text/html; charset=ANSI">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CACI-ERP</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
	<!-- Start of tecman Zendesk Widget script -->
	
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},
window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",
d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e)
{n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");
n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,
this.zendeskHost="tecmanlimited.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>
<!-- End of tecmanlimited Zendesk Widget script -->
	

  </head>

  
<body style="background:#b3d7ff;">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
 
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
		  
		 	 <img src="images/cacilogo.png" alt="..."  width="30%" height="25%" ><br />	
			 
           <form class="form-horizontal form-label-left" method="post"   id="li">
               <h1><font color='#4B5F71'>Change Password</font></h1>
			   
			     <div class="alert alert-success" id="results" role="alert">
						
			</div>
			 <div class="alert alert-danger" id="error" role="alert">
						
			</div>
			
              <div>
                <input id="old_pass" class="form-control has-feedback-left col-md-7 col-xs-12" name="old_pass" 
						  placeholder="Enter Old Password" type="password" required="required" autofocus>						 
                         
              </div> 
			  <div>
                <input id="password" class="form-control has-feedback-left col-md-7 col-xs-12" name="password" 
						  placeholder="Enter New Password" type="password" required="required" autofocus>						 
                         
              </div> 
			  <div>
                <input id="password1" class="form-control has-feedback-left col-md-7 col-xs-12" name="password1" 
						  placeholder="Confirm New Password" type="password" required="required">						 
                         
              </div>
             
              <div class="form-group ">
                 <div  class="btn-group btn-group-justified">
				 <input id="pass" type="submit" class="btn btn-dark btn-md " style="width:100%" value="Save Password!"/>
              </div>
			  </div> 
			  
			  <div class="form-group ">
                 <div  class="btn-group btn-group-justified">
				 <input id="cancel" type="submit" class="btn btn-dark btn-md " onclick="window.location.href='index.php' " style="width:100%" value="Cancel"/>
              </div>
			  </div>
			  <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

							
                <div>
                 
                </div>
              </div>
            </form>
          </section>
        </div>        
       </div>
      </div>
  <?php include 'javascripts.php'; ?>

    
 <script type="text/javascript" >
$(document).ready(function (e) {
	
	$('#results').hide();
	$('#error').hide();
 $("#li").on('submit',(function(e) {
  e.preventDefault();
 var old_pass = $("#old_pass").val();
 var password = $("#password").val();
 var password1 = $("#password1").val();
 //alert(mobile);
 console.log(old_pass);
 console.log(password);
 console.log(password1);
 /*if(mobile==''){
	$('#error').html("");
	$('#error').html("<p><sytle= 'font-family:courier; color:white;'>ERROR! <br />Please Enter Your Registered Mobile Number.</font></p>");	
	$('#error').show(); 
 }else{*/
 //alert(password);
  $.ajax({
  
   type: "POST",
   data:  {
	   password:password, password1:password1, old_pass:old_pass	    
		},
	url: "php/save_password_change_in.php",
   dataType: 'html',
         cache: false,
  
   success: function(data)
      {
						console.log(data);
						if(data.match(/success/gi)){
							//setTimeout(function(){	window.location="index_members.php";},2000);	
								//window.location="verify_token.php";	
								$('#error').hide();
								$('#results').html("");
								$('#results').html("<p><sytle= 'font-family:courier;'>Password was successfully changed.</p><p> Redirecting to Login Page...</font></p>");												
								$('#results').show(); 
								$('#old_pass').hide();
								$('#cancel').hide();
								$('#pass').hide();
								$('#cancel').hide();
								$('#password').hide();
								$('#password1').hide();
								setTimeout(function(){	window.location="index.php";},5000);	
						}
							
							else if(data.match(/mismatch/gi)){
							$('#error').html("");
							$('#error').html("<p><sytle= 'font-family:courier; color:white;'>Passwords do not match, Please try again.</font></p>");
							$('#error').show();
							}
							
							else if(data.match(/wrong/gi)){
							$('#error').html("");
							$('#error').html("<p><sytle= 'font-family:courier; color:white;'>Wrong Old Password! Please try again.</font></p>");
							$('#error').show();
							}
							
							 else{
							$('#results').hide();
							$('#error').html("");
							$('#error').html("<p><sytle= 'font-family:courier; color:white;'>SYSTEM ERROR! Please contact Help Desk.</font></p>");
							$('#error').show();
							
							 }
							 
											
				},
							
		error: function(e) {
						$('#error').html("");
							
							$('#error').html("<p><sytle= 'font-family:courier; color:white;'>NETWORK ERROR! <br />Please check your internet connection.</font></p>");	
													
							$('#error').show();
						
				}			
					
			
		});
 }

		
	 
 
 )
)
 //}
 });

</script>
 <script>
$('body').on("click mousemove keyup", _.debounce(function(){
  window.location.replace("php/logout.php");}, 180000)) // 15mins inactivity
</script>
  </body>
</html>