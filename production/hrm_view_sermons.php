<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"hrm")
{
	header("Location: " . 'index.php');
	
}

?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'header.php'; ?>
 

 <!--==========/header  =========-->

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
				 <?php include 'hrm_navbar.php'; ?>

				<div class="clearfix"></div>

				<!-- ==========menu profile quick info ===== -->
					<?php include 'profile.php';?>
					<br />
				<!--========== /menu profile quick info ===-->
			   

				<!--==========sidebar menu  =========-->
					<?php include 'hrm_menu.php'; ?>
			 
				<!--==============/sidebar menu======-->
		   </div>
		</div>
		<!-- ==========top navigation ======-->
			<?php include 'top_nav.php'; ?>
		<!--========= /top navigation ======-->


 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>SERMONS</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
				  
				   <!--div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Transfer No.</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input id="transfer_no" class="form-control has-feedback-left col-md-7 col-xs-12" name="transfer_no" 
						  placeholder="Enter Transfer No." type="number" min="1">
						 
                          <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  <br />
					    <div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
						 <input id="register_mem" type="submit" class="btn btn-primary btn-md" style="width:100%" value="Reply "/>
                       
						</div>
                      </div-->
                   
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
									<tr >
										<th >Action</th>
										<th >Title</th>
										<th >Author</th>	
										<th >Uploaded On</th>
										<th >Uploaded By</th>
									</tr>
							</thead >
							
						<tbody >
					  </tbody> 
					  <tfoot>
									<tr >
										<th >Action</th>
										<th >Title</th>
										<th >Author</th>	
										<th >Uploaded On</th>
										<th >Uploaded By</th>
									</tr>
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
		
    <!-- jQuery -->
	
 <?php include 'javascripts.php'; ?>

 <script type="text/javascript">
 $('#results').DataTable( {
    ajax: {
        url: 'php/fetch_sermons.php',
        dataSrc: '',
		 "deferRender": true
    },
    columns: [
            { data: "Action" },
			{ data: "Title" },
			 { data: "Author" },
			 { data: "Uploaded On" }, 
			 { data: "Uploaded By" }
        ],
		//	 order: [[ 2, "asc" ]]	
} );
</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>
