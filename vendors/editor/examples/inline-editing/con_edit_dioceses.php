<?php session_start();
if(!isset($_SESSION['auth_level'])|| $_SESSION['auth_level']<>"connexional")
{
	header("Location: " . 'index.php');
	
}
?>
<!DOCTYPE html>
<html lang="en">
<!--==========header  =========-->
<?php include 'main_header.php'; ?>
 <!--==========/header  =========-->
 
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <?php include 'con_navbar.php'; ?>

            <div class="clearfix"></div>

 <?php include 'profile.php';?>
					<br />
 <!--========== /menu profile quick info ===-->
           

  <!--==========sidebar menu  =========-->
     <?php include 'con_side_menu.php'; ?>
         
 <!--==============/sidebar menu======-->
  </div>
 </div>
 <!-- ==========top navigation ======-->
        <?php include 'top_navigation.php'; ?>
 <!--========= /top navigation ======-->

 <!--========== page content =======-->
        <div class="right_col" role="main">
          <div class="">
        
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ARCHIVED MEETING MINUTES</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
				  
				  
                    <div style="overflow-x: auto;">
                    <table id="results" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					
                      <thead>
									<tr >
										 <th></th>
										<th>Diocese</th>
										<th>Town Name</th>
										<th>Area Name</th>
										<th>Street Name</th>
										<th>Hse No</th>
										<th>Postal Address</th>
										<th>Email</th>
										<th>Tel One</th>
										<th>Tel Two</th>
										<th>Date Founded</th>
									</tr>
							</thead >		
							
						<tbody >
					  </tbody> 
					  <tfoot>
									<tr >
											<th></th>
											<th>Diocese</th>
											<th>Town Name</th>
											<th>Area Name</th>
											<th>Street Name</th>
											<th>Hse No</th>
											<th>Postal Address</th>
											<th>Email</th>
											<th>Tel One</th>
											<th>Tel Two</th>
											<th>Date Founded</th>
									</tr> 
							</tfoot >
                      <tbody>
                       
                      </tbody>
                    </table>
					
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
<!--======== /page content ==========-->

        <!-- footer content -->
       <?php include 'footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>
		
    <!-- jQuery -->
	
 <?php include 'script_links.php'; ?>

 <script type="text/javascript" language="javascript" class="init">
	


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "../vendors/editor/examples/php/update_dioceses.php",
		table: "#results",
		fields: [ {
				label:" Diocese:",
				name: "diocese_name"
			}, {
				label: "Town :",
				name: "town_name"
			}, {
				label: "Area:",
				name: "area_name"
			}, {
				label: "Street:",
				name: "street_name"
			},  {
				label: "Hse No:",
				name: "hse_no"
			}, {
				label: "Postal Address:",
				name: "postal_address"
			}, {
				label: "Email:",
				name: "email"
			}, {
				label: "Tel One:",
				name: "tel_one"
			}, {
				label: "Tel Two:",
				name: "tel_two"
			},{
				label: "Date Created:",
				name: "date_founded"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#results').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this );
	} );

	$('#results').DataTable( {
		dom: "Bfrtip",
		ajax: "../vendors/editor/examples/php/update_dioceses.php",
		order: [[ 1, 'asc' ]],
		columns: [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
			{ data: "diocese_name" },
			{ data: "town_name" },
			{ data: "area_name" },
			{ data: "street_name" },
			{ data: "hse_no" },
			{ data: "postal_address" },
			{ data: "email" },
			{ data: "tel_one" },
			{ data: "tel_two" },
			{ data: "date_founded" }
			//{ data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
		],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
		buttons: [
			//{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor },
			//{ extend: "remove", editor: editor }
		]
	} );
} );

</script>
 <?php include 'timeout.php'; ?>
  </body>
</html>
