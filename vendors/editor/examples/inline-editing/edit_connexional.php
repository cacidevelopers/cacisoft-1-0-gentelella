<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
	<title>Editor example - Simple inline editing</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.1/css/select.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../../css/editor.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../resources/syntax/shCore.css">
	<link rel="stylesheet" type="text/css" href="../resources/demo.css">
	<style type="text/css" class="init">
	
	</style>
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="../../js/dataTables.editor.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="../resources/syntax/shCore.js">
	</script>
	<script type="text/javascript" language="javascript" src="../resources/demo.js">
	</script>
	<script type="text/javascript" language="javascript" src="../resources/editor-demo.js">
	</script>
	
</head>
<body class="dt-example">
	<div class="container">
	<br><br><br><br>
					<div class="form-group">
                         <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-3">
						 <!--input id="reply" type="submit" class="btn btn-primary btn-md" style="width:100%" value="Save Minutes "/-->
                        <!--button class="btn btn-primary btn-md" style="width:100%" type="reset" id="reset"> Reset</button-->
						<button class="btn btn-primary btn-md" style="width:100%" type="button" id="cancel" onclick="window.location.href='../../../../production/index_connexional.php' "> Back to Home Page</button>
					 </div>
                      </div>
	<br><br><br><br>
	  <div style="overflow-x: auto;">
		<table id="results" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th></th>
            <th>Connexional</th>
            <th>Town Name</th>
            <th>Area Name</th>
            <th>Street Name</th>
            <th>Hse No</th>
            <th>Postal Address</th>
            <th>Email</th>
            <th>Tel One</th>
            <th>Tel Two</th>
        </tr>
    </thead>
</table>	

	</div>
	</div>
<script type="text/javascript" language="javascript" class="init">
	


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "../php/update_connexional.php",
		table: "#results",
		fields: [ {
				label: "Connexional:",
				name: "con_name"
			}, {
				label: "Town :",
				name: "town_name"
			}, {
				label: "Area:",
				name: "area_name"
			}, {
				label: "Street:",
				name: "street_name"
			},  {
				label: "Hse No:",
				name: "hse_no"
			}, {
				label: "Postal Address:",
				name: "postal_add"
			}, {
				label: "Email:",
				name: "email"
			}, {
				label: "Tel One:",
				name: "tel_one"
			}, {
				label: "Tel Two:",
				name: "tel_two"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#results').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this );
	} );

	$('#results').DataTable( {
		dom: "Bfrtip",
		ajax: "../php/update_connexional.php",
		order: [[ 1, 'asc' ]],
		columns: [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
			{ data: "con_name" },
			{ data: "town_name" },
			{ data: "area_name" },
			{ data: "street_name" },
			{ data: "hse_no" },
			{ data: "postal_add" },
			{ data: "email" },
			{ data: "tel_one" },
			{ data: "tel_two" }
			//{ data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
		],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
		buttons: [
			//{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor },
			//{ extend: "remove", editor: editor }
		]
	} );
} );



	</script>	
</body>
</html>