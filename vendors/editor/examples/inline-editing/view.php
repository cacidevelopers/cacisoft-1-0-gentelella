<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
	<title>Editor example - Simple inline editing</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.1/css/select.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../../css/editor.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../resources/syntax/shCore.css">
	<link rel="stylesheet" type="text/css" href="../resources/demo.css">
	<style type="text/css" class="init">
	
	</style>
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="../../js/dataTables.editor.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="../resources/syntax/shCore.js">
	</script>
	<script type="text/javascript" language="javascript" src="../resources/demo.js">
	</script>
	<script type="text/javascript" language="javascript" src="../resources/editor-demo.js">
	</script>
	<script type="text/javascript" language="javascript" class="init">
	


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "../php/members.php",
		table: "#example",
		fields: [ {
				label: "Reg No:",
				name: "reg_no"
			}, {
				label: "Title:",
				name: "title"
			}, {
				label: "First Name:",
				name: "first_name"
			}, {
				label: "Middle Names:",
				name: "middle_names"
			},  {
				label: "Last Name:",
				name: "last_name"
			}, {
				label: "Qualifications:",
				name: "qualifications"
			}, {
				label: "Marital Status:",
				name: "marital_status"
			}, {
				label: "Gender:",
				name: "gender"
			}, {
				label:"DoB",
				name: "dob",
				type: "datetime"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this );
	} );

	$('#example').DataTable( {
		dom: "Bfrtip",
		ajax: "../php/members.php",
		order: [[ 1, 'asc' ]],
		columns: [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
			{ data: "reg_no" },
			{ data: "title" },
			{ data: "first_name" },
			{ data: "middle_names" },
			{ data: "last_name" },
			{ data: "qualifications" },
			{ data: "marital_status" },
			{ data: "gender" },
			{ data: "dob" }
			//{ data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
		],
		select: {
			style:    'os',
			selector: 'td:first-child'
		},
		buttons: [
			//{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor },
			//{ extend: "remove", editor: editor }
		]
	} );
} );



	</script>
</head>
<body class="dt-example">
	<div class="container">
	<table id="example" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th></th>
            <th>Reg No</th>
            <th>Title</th>
            <th>First Name</th>
            <th>Middle Names</th>
            <th>Last name</th>
            <th>Qualifications</th>
            <th>Marital Status</th>
            <th>Gender</th>
            <th width="18%">DoB</th>
        </tr>
    </thead>
</table>	
	</div>
	
</body>
</html>