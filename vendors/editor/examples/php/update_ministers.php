<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST

Editor::inst( $db, 'ministers','user_id' )
	->fields(
		
		Field::inst( 'title' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'first_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'middle_names' ),
		Field::inst( 'last_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'qualification' ),
		Field::inst( 'oqualifications' ),
		Field::inst( 'grade' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'notch' )->validator( 'Validate::numeric' ),
		Field::inst( 'current_station' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'previous_stations'),
		Field::inst( 'marital_status' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'spouse' ),
		Field::inst( 'spouse_no' )->validator( 'Validate::numeric' ),
		Field::inst( 'children' ),
		Field::inst( 'mobile' )->validator( 'Validate::numeric' ),
		Field::inst( 'mobile2' )->validator( 'Validate::numeric' ),
		Field::inst( 'whatsapp' )->validator( 'Validate::numeric' ),
		Field::inst( 'email' ),
		Field::inst( 'sec_lang' ),
		Field::inst( 'postal_add' ),
		Field::inst( 'emergency_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'emergency_no' )->validator( 'Validate::numeric' ),
		Field::inst( 'relationship' )->validator( 'Validate::notEmpty' )
		/*Field::inst( 'age' )
			->validator( 'Validate::numeric' )
			->setFormatter( 'Format::ifEmpty', null ),*/
		
			/*->getFormatter( 'Format::date_sql_to_format', Format::DATE_ISO_8601 )
			->setFormatter( 'Format::date_format_to_sql', Format::DATE_ISO_8601 )*/
	)
	->process( $_POST )
	->json();
	
	