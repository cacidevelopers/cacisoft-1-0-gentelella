<?php
@session_start();
/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );
//$cir_code=$_SESSION["cir_code"];
// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST

Editor::inst( $db, 'visitors','visitor_id' )
	->fields(
		Field::inst( 'reg_date' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'title' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'first_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'last_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'mobile' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'whatsapp' ),
		Field::inst( 'nationality' ),
		Field::inst( 'purpose' )
		
		/*Field::inst( 'age' )
			->validator( 'Validate::numeric' )
			->setFormatter( 'Format::ifEmpty', null ),*/
		
			/*->getFormatter( 'Format::date_sql_to_format', Format::DATE_ISO_8601 )
			->setFormatter( 'Format::date_format_to_sql', Format::DATE_ISO_8601 )*/
	)
	//->where( $key = "cir_code", $value = $cir_code, $op = "=" )
	->process( $_POST )
	->json();
