<?php
@session_start();
/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

$cir_code=$_SESSION["cir_code"];

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST

Editor::inst( $db, 'fam_groups','fam_code' )
	->fields(
		Field::inst( 'fam_code' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'fam_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'meeting_days' )->validator( 'Validate::notEmpty' )
		
	)
	->where( $key = "cir_code", $value = $cir_code, $op = "=" )
	->process( $_POST )
	->json();
