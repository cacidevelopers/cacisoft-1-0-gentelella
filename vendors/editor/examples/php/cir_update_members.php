<?php
@session_start();
/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );
$cir_code=$_SESSION["cir_code"];
// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST

Editor::inst( $db, 'members','user_id' )
	->fields(
		Field::inst( 'title' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'first_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'middle_names' ),
		Field::inst( 'last_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'maiden_name' ),
		Field::inst( 'dob' )->validator('Validate::dateFormat', array("format"=> Format::DATE_ISO_8601,"message" => "Please enter a date in the format yyyy-mm-dd")),
		Field::inst( 'gender' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'home_region' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'home_town' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'mother_tongue' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'sec_lang' ),	
		Field::inst( 'mobile' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'mobile2' ),
		Field::inst( 'whatsapp' ),
		Field::inst( 'email' ),
		Field::inst( 'postal_add' ),
		Field::inst( 'digi_add' ),
		Field::inst( 'marital_status' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'type_of_marriage' ),
		Field::inst( 'town_of_res' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'area_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'street_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'hse_no' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'qualification' ),
		Field::inst( 'profession' ),
		Field::inst( 'occupation' ),
		Field::inst( 'employer' ),
		Field::inst( 'emergency_name' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'emergency_no' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'emergency_email' ),
		Field::inst( 'date_joined' )->validator('Validate::dateFormat', array("format"=> Format::DATE_ISO_8601,"message" => "Please enter a date in the format yyyy-mm-dd")),
		Field::inst( 'baptised' )->validator( 'Validate::notEmpty' ),
		Field::inst( 'where_baptised' ),
		Field::inst( 'date_baptised' ),
		Field::inst( 'aux_code1' ),
		Field::inst( 'date_joined_aux1' ),
		Field::inst( 'aux_code2' ),
		Field::inst( 'date_joined_aux2' ),
		Field::inst( 'aux_code3' ),		
		Field::inst( 'date_joined_aux3' ),
		Field::inst( 'account_status' ),
		Field::inst( 'fam_code' )
		
		/*Field::inst( 'age' )
			->validator( 'Validate::numeric' )
			->setFormatter( 'Format::ifEmpty', null ),*/
		
			/*->getFormatter( 'Format::date_sql_to_format', Format::DATE_ISO_8601 )
			->setFormatter( 'Format::date_format_to_sql', Format::DATE_ISO_8601 )*/
	)
	->where( $key = "cir_code", $value = $cir_code, $op = "=" )
	->process( $_POST )
	->json();
